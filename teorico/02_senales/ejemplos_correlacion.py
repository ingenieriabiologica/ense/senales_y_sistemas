#!/usr/bin/env python3
import math
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from scipy import signal
import time

N = 2000
t = np.linspace(-10, 10, N)  # create a time signal
dt = t[2] - t[1]
s = 1
ti = 0

t2i = -2
i = 5
t2 = t2i + 0.5 * i
x1 = np.zeros(N)
idx2 = math.floor(t2 / dt + N / 2)
w1 = math.floor(1 / dt)
x1[range(idx2 - w1, idx2 + w1)] = 1

# %%
t2i = -2
i = 5
x2a = np.zeros(N)
t2 = t2i + 0.5 * i
idx2 = math.floor(t2 / dt + N / 2)
w2 = math.floor(1 / dt)
x2a[range(idx2 - w2, idx2 + w2)] = 1

c = np.correlate(x1, x2a)[0]
print("anchos: ", w1, w2)
print("correlation coef: ", c)

plt.plot(t, x1, 'k-')
plt.plot(t, x2a*1.02, 'r-')
plt.ylim([0, 1.1])

plt.title('Ejemplo 1 - correlación: '+str(c))
x2_nombre = 'x2_' + str(i)
plt.legend(['x1', x2_nombre])
#plt.show()

# %%
t2i = -2
i = 0
x2a = np.zeros(N)
dt2 = 0.1
t2 = t2i + dt2 * i
idx2 = math.floor(t2 / dt2 + N / 2)
w2 = math.floor(1 / dt2)
x2a[range(idx2 - w2, idx2 + w2)] = 1

c = np.correlate(x1, x2a)[0]
print("anchos: ", w1, w2)
print("correlation coef: ", c)
plt.figure()
plt.plot(t, x1, 'k-')
plt.plot(t, x2a, 'r-')
plt.ylim([0, 1.1])
plt.title('Ejemplo 2 - correlación: '+str(c))
x2_nombre = 'x2' + str(i)
plt.legend(['x1', x2_nombre])
#plt.show()



# %%
t2i = -2
i = 10
x2i = np.zeros(N)


t2 = t2i + dt2 * i
idx2 = math.floor(t2 / dt + N / 2)
x2i[range(idx2 - w2, idx2 + w2)] = 1
c = np.correlate(x1, x2i)
print("correlation coef: ", c)

plt.figure()
plt.plot(t, x1, 'k-')
plt.plot(t, x2i, 'r-')
plt.ylim([0, 1.1])
plt.title('Ejemplo 3 - correlación: '+str(c))

#plt.show()

# %%
t2i = -2
i = 21
x2i = np.zeros(N)

t2 = t2i + 0.1 * i;
idx2 = math.floor(t2 / dt + N / 2)
x2i[range(idx2 - w2, idx2 + w2)] = 1
plt.figure()
plt.plot(t, x1, 'k-')
plt.plot(t, x2i, 'r-')
plt.ylim([0, 1.1])
c = np.correlate(x1, x2i)
print("correlation coef: ", c)
plt.title('Ejemplo 4 - correlación: '+str(c))
#plt.show()


# %%
t2i = -2
i = 21
x2i = np.zeros(N)

t2 = t2i + 0.1 * i;
idx2 = math.floor(t2 / dt + N / 2)
x2i[range(idx2 - w2, idx2 + w2)] = 2
plt.figure()
plt.plot(t, x1, 'k-')
plt.plot(t, x2i, 'r-')
plt.ylim([0, 2.1])
c = np.correlate(x1, x2i)
print("correlation coef: ", c)
plt.title('gaussian pulse')
plt.title('Ejemplo 5 - correlación: '+str(c))
plt.show()


