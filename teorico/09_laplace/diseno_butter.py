#!/usr/bin/env python3
# %%
import math
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

# DISEÑO DE UN FILTRO DE BUTTERWORTH EN TIEMPO CONTINUO

T = 1.0
FLAG = 3    # variable que controla cual ejemplo se corre

# -*- coding: utf-8 -*-
# %% Señales y Sistemas

# ejemplo 2 de las notas (Clase 34 Señales y Sistemas)

if FLAG == 1:
    delta_1 = 0.8
    delta_2 = 0.15

    fp = 10  # frecuencia de la banda pasante en Hz
    fs = 30  # frecuencia de la banda supresora en Hz

    wp = 2 * math.pi * fp
    ws = 2 * math.pi * fs

# %% Ejemplos para usar en Procesamiento Digital de Señales (clase 13)

# ejemplo 7.2
if FLAG == 2:
    delta_1 = 0.89125
    delta_2 = 0.17783
    # frecuencias angulares discretas
    wd_p = 0.2 * math.pi
    wd_s = 0.3 * math.pi
    # conversión a frecuencias angulares continuas
    wp = wd_p / T
    ws = wd_s / T

# ejemplo bilineal
if FLAG == 3:
    #  tolerancias
    delta_1 = 0.75
    delta_2 = 0.3
    # frecuencias angulares discretas
    wd_p = 0.2 * math.pi
    wd_s = 0.4 * math.pi
    # conversión a frecuencias angulares en T.C.
    wp = 2.0 / T * math.tan(wd_p / 2)
    ws = 2.0 / T * math.tan(wd_s / 2)

# %%
wa = math.pi/T # frecuencia (en T.C.) máxima

if FLAG >= 1:
    print("Especificaciones en T.D.")
    print("wd_p: ", np.round(wd_p, 3))
    print("wd_s: ", np.round(wd_s, 3))

print("Especificaciones en T.C.")
print("Frecuencias angulares")
print("wp: ", np.round(wp, 3))
print("ws: ", np.round(ws, 3))
print("wa: ", np.round(wa, 3))
print("Tolerancias")
d1 = 1 / delta_1**2 - 1
d2 = 1 / delta_2**2 - 1
print("delta_1: ", np.round(delta_1, 3))
print("delta_2: ", np.round(delta_2, 3))
print("Tolerancias convertidas (para facilitar cuentas)")
print("d1: ", np.round(d1, 3))
print("d2: ", np.round(d2, 3))

# %% Calculo el N mínimo
N = math.log(d1/d2) / math.log(wp/ws) / 2

# Como N debe ser entero:
if FLAG == 1:
    No = math.ceil(N)

if FLAG == 2:
    No = 6  # invariancia al impulso
    No = math.ceil(N)

if FLAG == 3:
    No = 2  # bilineal

# A partir de N entero, calculo la frecuencia de corte
wc  = wp / d1 ** (1 / 2 / No)
wc2 = ws / d2 ** (1 / 2 / No)

# calculo la frecuencia de corte en T.D.
wdc = 2 * math.atan(wc * T / 2.0)

# %%
# verifico valores
Gbs = math.sqrt(1 / (1 + (ws / wc) ** (2 * No)))
Gbp = math.sqrt(1 / (1 + (wp / wc) ** (2 * No)))
Ga  = math.sqrt(1 / (1 + (wa / wc) ** (2 * No)))


print("Parámetros de diseño")
print("N: ", np.round(N, 2), " No: ", No)
print("wc: ", np.round(wc, 3))

print("\n")
print("Ganancias obtenidas")
print("Ganancia Bp: ", Gbp)
print("Ganancia Bs: ", Gbs)
print("Ganancia alias: ", np.round(Ga, 4))

# %% cálculo de polos

x = np.zeros(2 * No)
y = np.zeros(2 * No)
for k in range(0, 2 * No):
    a = math.pi / 2 + (2 * k * math.pi + math.pi) / 2 / No
    x[k] = wc * math.cos(a)
    y[k] = wc * math.sin(a)

plt.figure()
circle = plt.Circle((0, 0), wc, color='k', fill=False)
ax = plt.gca()
ax.add_patch(circle)
plt.plot(x, y, '.', markersize=20)
plt.plot(x[0:No], y[0:No], 'r.', markersize=20)
plt.axis('equal')
plt.title('Polos calculados')
# plt.ylim([-1, 1])
# plt.xlim([-1, 1])

plt.figure()
circle = plt.Circle((0, 0), wc, color='k', fill=False)
ax = plt.gca()
ax.add_patch(circle)
plt.plot(x, y, '.', markersize=20)
plt.plot(x[0], y[0], 'rx', markersize=20)
plt.plot(x[No - 1], y[No - 1], 'gx', markersize=20)
plt.axis('equal')
# plt.ylim([-1, 1])
# plt.xlim([-1, 1])
plt.title('Polos (primero marcado)')
if 0:
    plt.show()

# %%
# Cálculo de coeficientes del polinomino del denominador
# p es el vector con los coeficientes
p = 1
s = np.zeros(No, dtype=complex)
for k in range(0, No):
    s[k] = complex(x[k], y[k])
    p = np.convolve(p, [1, -s[k]], 'full')

# redondeo los coeficientes y le quito la parte imaginaria que es cero
p_r = np.real(np.round(p, 4))
# guardo los coeficientes
np.save('coefs_butter.npy', p_r)

# %%
# # Bode
# dibujo el bode
S = signal.lti([1], p)
w, mag, phase = signal.bode(S)

plt.figure()
plt.semilogx(w, mag)  # Bode magnitude plot
plt.title('Bode - Modulus (polynomal coefs and signal.lti)')
plt.figure()
plt.semilogx(w, phase)  # Bode phase plot
plt.title('Bode - Phase (polynomal coefs and signal.lti)')

# %% Retardo de grupo
gd = -np.diff(phase) / (w[2] - w[1])
w_gd = w[0:len(w) - 1]
plt.figure()
plt.semilogx(w_gd, gd)
plt.title('Group delay')

# %%
# Verificación
# verificamos los términos de a pares conjugados
par0t = np.convolve([1, -s[0]], [1, -s[No - 1]])
par0 = np.round(np.real(par0t), 4)


# Valido para N=2
pol_analitico = [1, -2 * wc * math.cos((1.0 / 2 + 1.0 / 4) * math.pi), wc ** 2]

np.set_printoptions(formatter={'float': lambda x: "{0:0.2f}".format(x)})
print(u"\nPolinomios desarrollados")
print(u"Coeficientes (numéricos) H(s): ", p_r)
print(u"Coeficientes (numéricos) del primer par de polos conjugados de H(s): ", par0)
print(u"Coeficientes (analíticos) del primer par de polos conjugados de H(s):", np.round(pol_analitico, 2))

S2 = signal.lti([1], pol_analitico)
w, mag, phase = signal.bode(S2)

plt.figure()
plt.semilogx(w, mag)  # Bode magnitude plot
plt.title('Bode Modulus - from coefficients (analytical)')
plt.figure()
plt.semilogx(w, phase)  # Bode phase plot
plt.title('Bode Phase - from coefficients (analytical)')
plt.show()

# %%
