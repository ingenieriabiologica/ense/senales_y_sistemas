#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

# creo un sistema y hago el bode
# Bilineal (FLAG=3)
b = [1, 2, 1]
a = [6.4, -7.04, 2.52]

# b = [1]
#a=[1, 1.45, 0.49]
#a=[1, 0.95, 0.44]
# a = [1, 95.48, 4558.58]
s1 = signal.dlti(b, a)
w, mag, phase = signal.dbode(s1)

plt.figure()
plt.semilogx(w, mag)    # Bode magnitude plot
plt.title('Magnitud')
plt.figure()
plt.semilogx(w, phase)  # Bode phase plot
plt.title('Fase')
plt.show()

wc=0.7
No=2
wp=0.2*math.pi
ws=0.4*math.pi

Gp=math.sqrt(1/(1+(wp/wc)**(2*No)))
Gs=math.sqrt(1/(1+(ws/wc)**(2*No)))

print("Ganancia Bp: ", Gp)
print("Ganancia Bs: ", np.round(Gs, 2))
