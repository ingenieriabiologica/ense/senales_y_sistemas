#!/usr/bin/env python3
import math
import os.path

import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
import cmath

T = 1.0
verbose = 0

# Ejercicio 7.2

d1 = 0.89125
d2 = 0.17783

wp = 0.2 * math.pi
ws = 0.3 * math.pi

# resultados provenientes del diseno_butter.py (ejemplo 7.2)
# a = [1.00, 0.98, 0.48]
a = [1, 2.7170, 3.6910, 3.1788, 1.8252, 0.6644, 0.1209]   # T=1
# a = [1.00, 2716.97, 3690972.08, 3178835083.96, 1825172750947.06, 664372749640871.88, 120917636491214608.00] # T=1e-3
# a = [1.00, 271.70, 36909.72, 3178835.08, 182517275.09, 6643727496.41, 120917636491.21]    # T=1e-2
fname = 'coefs_butter.npy'
if os.path.exists(fname):
    a = np.load(fname)

print("Coeficients (numéricos) de H(s): ", a)

b = 1

No = len(a)-1

S = signal.lti(b, a)
w, mag, phase = signal.bode(S)

plt.figure()
plt.semilogx(w, mag)  # Bode magnitude plot
plt.title('C.T. Bode Plot (Magnitude)')
plt.figure()
plt.semilogx(w, phase)  # Bode phase plot
plt.title('C.T. Bode Plot (Phase)')

# da los polos rotados 90 grados
res, poles_t, k = signal.residue(b, a)

poles = np.round(poles_t, 2)

plt.figure()
plt.plot(poles.real, poles.imag, 'r.')
plt.axis('equal')
plt.ylim([-1, 1])
plt.xlim([-1, 1])
plt.title('C.T. Poles')
if verbose:
    plt.show()

az = 1
bz = 1
z = np.zeros(No, dtype=complex)

for k in range(0, No):
    z[k] = cmath.exp(poles_t[k]*T)
    az = np.convolve(az, [1, -z[k]], 'full')

az_r = np.round(np.real(az), 2)
print("Coeficientes (numéricos) de H(z): ", az_r)

r_roc = np.max(np.abs(z))

plt.figure()
plt.plot(z.real, z.imag, 'rx')
circle_fourier = plt.Circle((0, 0), 1, color='b', fill=False)
circle_roc = plt.Circle((0, 0), r_roc, color='r', linestyle='--', fill=False)
plt.gcf().gca().add_artist(circle_fourier)
plt.gcf().gca().add_artist(circle_roc)
plt.axis('square')
plt.ylim([-2, 2])
plt.xlim([-2, 2])
plt.title('Z poles')


# solo para que se vean lindos
z_r = np.real(z)

b, a = signal.invresz(res, z, [0])
Sd = signal.dlti(b, a)
w, mag, phase = Sd.dbode()
mag_n = mag/np.max(mag)

# cálculo de ganancias
idx_wp = np.argmin(np.abs(w-wp))
idx_ws = np.argmin(np.abs(w-ws))
Gp = mag_n[idx_wp]
Gs = mag_n[idx_ws]

print("\nGanancias obtenidas")
print("Ganancia Bp: ", Gp)
print("Ganancia Bs: ", Gs)

plt.figure()
plt.semilogx(w, mag_n)  # Bode magnitude plot
plt.semilogx(wp, Gp, '*r')
plt.semilogx(ws, Gs, '*r')
plt.title('$|H(e^{jw})|$')
plt.figure()
plt.semilogx(w, phase)  # Bode phase plot
plt.title('$\Theta(e^{jw})$')
plt.show()


