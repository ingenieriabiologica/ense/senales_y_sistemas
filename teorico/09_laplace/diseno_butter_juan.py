#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

# creo un sistema y hago el bode
# Bilineal (FLAG=3)
b = [1, 2, 1]
a = [6.4, -7.04, 2.52]
wc=0.69
No=2
wp=0.2*math.pi
ws=0.4*math.pi

# b = [1]
#a=[1, 1.45, 0.49]
#a=[1, 0.95, 0.44]
# a = [1, 95.48, 4558.58]
w, H = signal.freqz(b, a)
h_max = np.max(abs(H))
print("h_max: ", h_max)
mag_n = abs(H)/h_maxq
phase = np.angle(H)

# cálculo de ganancias
idx_wp = np.argmin(np.abs(w-wp))
idx_ws = np.argmin(np.abs(w-ws))
Gp = mag_n[idx_wp]
Gs = mag_n[idx_ws]

print("\nGanancias obtenidas")
print("Ganancia Bp: ", Gp)
print("Ganancia Bs: ", Gs)

plt.figure()
plt.semilogx(w, mag_n)  # Bode magnitude plot
plt.semilogx(wp, Gp, '*r')
plt.semilogx(ws, Gs, '*r')
plt.title('$|H(e^{jw})|$')
plt.figure()
plt.semilogx(w, phase)  # Bode phase plot
plt.title('$\Theta(e^{jw})$')
plt.show()

