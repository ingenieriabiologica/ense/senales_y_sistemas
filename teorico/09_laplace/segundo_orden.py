#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from scipy import signal, io

e = 0.2
fn = 100  # Hz
wn = 2 * math.pi * fn
N = 2 ** 16
wmax = 100 * wn
wmin = 0.01 * wn
dw = (wmax - wmin) / N
w = np.arange(wmin, wmax, dw)

a = [1, 2 * e * wn, wn ** 2]
# b=[2*e*wn,wn**2]
b = [wn ** 2]

S = signal.lti(b, a)
wu, mag, phase = signal.bode(S, w)

# Análisis en frecuencia

# frecuencias de interés
p = wn  # natural
pl = 0.1 * wn
pr = 10 * wn
if e<0.7:
    wm = math.sqrt(1 - 2 * e ** 2) * wn  # máximo
we = math.sqrt(1 - e ** 2) * wn  # efectiva

# Módulo: aproximación por una recta
B = (w > p) * (-40 * np.log(w) / math.log(10) + 40 * np.log(p) / math.log(10))
# Fase: aproximación por una recta
Phi = -math.pi / 2 * ((w > pl) * (w < pr)) * (np.log(w / p) / math.log(10) + 1) - (w > pr) * math.pi
Phi = Phi * 180 / math.pi
H = np.sqrt(1.0 / ((2 * e * w / wn) ** 2 + (1 - (w / wn) ** 2) ** 2))
modH = np.abs(H)

# indices de las frecuencias de interés
idx_w = np.argmin(np.abs(w - p))
idx_pr = np.argmin(np.abs(w - pr))
idx_pl = np.argmin(np.abs(w - pl))
if e<0.7:
    idx_wm = np.argmin(np.abs(w - wm))

plt.figure()
plt.semilogx(w, modH, '-r')  # Bode magnitude plot
plt.semilogx(w[idx_w], modH[idx_w], 'ok')
plt.semilogx(w[idx_pl], modH[idx_pl], 'ok')
plt.semilogx(w[idx_pr], modH[idx_pr], 'ok')
if e<0.7:
    plt.semilogx(w[idx_wm], modH[idx_wm], 'og')
plt.legend(['bode','w_c','0.1w_c','10w_c','w_m'])
plt.title(u'Módulo')

plt.figure()
plt.semilogx(w, B, '-b')
plt.semilogx(w, mag, '-r')  # Bode magnitude plot
plt.semilogx(w[idx_w], mag[idx_w], 'ok')
plt.semilogx(w[idx_pl], mag[idx_pl], 'ok')
plt.semilogx(w[idx_pr], mag[idx_pr], 'ok')
if e<0.7:
    plt.semilogx(w[idx_wm], mag[idx_wm], 'og')
plt.legend([u'Asintótico', 'Real'])
plt.title(u'Módulo (Logaritmico vs Bode)')

plt.figure()
plt.semilogx(w, Phi, '-b')
plt.semilogx(w, phase, '-r')  # Bode phase plot
plt.semilogx(w[idx_w], phase[idx_w], 'ok')
plt.semilogx(w[idx_pl], phase[idx_pl], 'ok')
plt.semilogx(w[idx_pr], phase[idx_pr], 'ok')
plt.title(u'Fase')
plt.legend([u'Asintótico', 'Real'])
if 0:
    plt.show()

print("wn: ", w[idx_w])
if e<0.7:
    print("H(wm): ", math.pow(10, mag[idx_wm] / 20))
    print("H(wm): ", modH[idx_wm])

# Análisis en el tiempo
tf = 20.0 / wn
n_t = 100  # cantidad de muestras a tomar en el eje del tiempo
dt = 1.0 / n_t / wn
t = np.arange(0, tf, dt)

# sobretiro máximo (e<1)
tp = np.pi/we
Mp = 1+np.exp(-e * np.pi/np.sqrt(1-e*e))
s = 1 - np.exp(-e * wn * t) * np.sin(we * t + math.acos(e))
ref = np.ones(len(t))
plt.figure()
plt.plot(t, s, '-g')
plt.plot(t, ref, '-b')
plt.plot(tp, Mp, '*r')
plt.title(u'Respuesta al escalón ($\epsilon<1$)')
plt.ylim([0, 1.6])
plt.xlim([0, 0.04])
plt.show()
