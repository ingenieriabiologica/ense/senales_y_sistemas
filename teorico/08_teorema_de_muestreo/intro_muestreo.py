#!/usr/bin/env python3

# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sg

# TODO: me suena que este ejemplo lo empezó Lucía y lo dejó por la mitad.
# Habría que hacer lo siguiente (en al menos 3 programas diferentes):
# 1.- Ilustrar la idea básica del muestreo en el tiempo y el error de la interpolación lineal
#           (para la primera clase de muestreo)
# 2.a- Ilustrar la interpolación de orden 0 (ver la interpolación en el tiempo, el error y la resp. frecuencia)
# 2.b- Ilustrar lo mismo con el interpolador de orden 1
# 2.c- Comparar ambas respuestas en frecuencia
# 3.- Realizar la interpolación de orden cero como una expansión seguida de una antitransformación



# Diseño del filtro pasa bajos (Butterworth de orden 6 y Fc=np.pi/L)
def filtro(L, fs_x):
    # Diseño de Pasabajos (Butterworth orden 6)
    order = 12
    Fc = np.pi/L # wc= np.pi/L 
    Fs = L*fs_x
    b, a = sg.butter(int(order / 2), Fc, 'lowpass', fs=Fs)
    return b, a

def calcular_butter(frecuencia_corte, orden, fs):
    # Especificaciones del filtro
    
    
    # Calcular la frecuencia de corte normalizada
    
    frecuencia_corte_normalizada = frecuencia_corte / (0.5 * fs)
    
    # Diseñar el filtro Butterworth
    b, a = sg.butter(orden, frecuencia_corte_normalizada, btype='low', analog=False, output='ba')
    
    return b, a

###############################################################################
#.........................Ejemplo de muestreo..................................
###############################################################################

# Genero una señal sinusoidal 'continua', con gran densidad de puntos
#..............................................................................
f = 1.0    # Hz, frecuencia de la señal
fsc = 200  # Hz, frecuencia de muestreo densa
ti = -1    # Tiempo de inicio
tf = 10    # Tiempo de fin
tc = np.arange(ti,tf,1/fsc) # Intervalo muestreado, simétrico por conveniencia
xc = np.sin(2*np.pi*f*tc)   # Señal sinusoidal 'continua'


# Genero la señal 'muestreada'
#..............................................................................
fs = 2.5    # Hz, frecuencia de muestreo (ie. >= 2*f)
Ts = 1/fs   # Distancia entre muestras
t = np.arange(ti, tf, Ts)  # Intervalo muestreado, simétrico por conveniencia
x = np.sin(2*np.pi*f*t)    # Muestras de la señal sinusoidal


# Gráfico de la señal y sus muestras
#..............................................................................
fig,ax = plt.subplots()
ax.plot(t,x,'-o')
ax.plot(tc,xc,'-')
ax.set_xlabel('tiempo',fontsize=14)
ax.set_ylabel('amplitud',fontsize=14)
tit = 'muestreo a $f_s$ = ' + str(fs)
plt.title(tit)

# Este sirve para ilustrar el ejemplo del coseno mal muestreado
if 0:
    xa=np.sin(2*np.pi*(fs-f)*t)
    plt.figure()
    plt.plot(t,x,'-b')
    plt.plot(t,xa,'-r')
    tit = 'reconstrucción de copia con $f_o$ = ' + str(round(fs-f, 3))
    plt.title(tit)
    plt.show()

# %% ####################   Interpolación lineal   ############################

interval = []  # piecewise domains
apprx = []     # line on domains

# build up points *evenly* inside of intervals
tp = np.hstack([np.linspace(t[i], t[i+1], 40, False) for i in range(len(t)-1)])
# construct arguments for piecewise2
for i in range(len(t)-1):
   interval.append( np.logical_and(t[i] <= tp,tp < t[i+1]))
   apprx.append( (x[i+1]-x[i])/(t[i+1]-t[i])*(tp[interval[-1]]-t[i]) + x[i])
x_hat = np.piecewise(tp, interval, apprx)  # piecewise linear approximation

xp = np.sin(2*np.pi*f*tp)
sqe = (x_hat - xp)**2

fig, ax3 = plt.subplots()
ax3.plot(tp, xp, 'r-')
ax3.plot(tp, x_hat, 'b-')
ax3.plot(t, x,'o')
plt.title("Interpolación lineal")

# Error cometido con la interpolación lineal
#..............................................................................

ax1 = plt.figure().add_subplot(111)
ax1.fill_between(tp, x_hat, xp, facecolor='b', alpha=0.8)
ax1.plot(tp, xp, 'k-')
ax1.set_xlabel('tiempo', fontsize=18)
ax1.set_ylabel('Area', fontsize=18)
# ax2 = ax1.twinx()
ax1.set_title('Error con interpolante lineal')

ax2 = plt.figure().add_subplot(111)
ax2.plot(tp, sqe,'r')
ax2.axis(xmin=-1,ymax= sqe.max() )
ax2.set_ylabel('Amplitud', color='k',fontsize=18)
ax2.set_title('Error con interpolante lineal')

# %% ########################   Interpolación   ###############################

L=20

# Expansor de la tasa de muestreo
#..............................................................................
x_new = np.zeros(L*len(x))
for i in np.arange(0, len(x)):
    x_new[int(L*i)] = x[i]

# Filtrado
#..............................................................................
fc = (fs)/(2*L)
grado_filtro = 10
b, a = calcular_butter(fc, grado_filtro, fs)

#ind = w>(np.pi/L)
#K = L/np.max(np.abs(h[ind]))

# K = L*10
x_n = L* sg.filtfilt(b, a, x_new)
t_n = np.arange(ti,len(x_n)*Ts/L + ti,Ts/L)

w, h = sg.freqz(b, a)

plt.figure()
plt.plot(w, np.abs(h))
plt.title('Filtro Hl')

# Gráficos
#..............................................................................
# Gráfico del filtro
# TODO: no queda claro cual filtro es
if 0:
    w, h = sg.freqz(b, a)
    plt.figure()
    plt.plot(w, 20*np.log10(K*abs(h)))
#    plt.axvline(x=(f), color='r')
#    plt.axvline(x=(np.pi/L), color='g')
    plt.grid()
    plt.xlabel('frecuencia (Hz)',fontsize=14)
    plt.ylabel('amplitud (dB)',fontsize=14)
    plt.title('Filtro pasa bajos')

# TODO
#Gráfico de las transformadas de 
if 0:
    w, h = sg.freqz(b, a)
    plt.figure()
#    plt.plot(np.fft.fftshift(np.fft.fftfreq(len(x),1/fs)),np.fft.fftshift(np.fft.fft(x)),'k')
    plt.plot(np.fft.fftshift(np.fft.fftfreq(len(x_new),1/(fs*L)))/np.pi,np.fft.fftshift(np.fft.fft(x_new)),'b')
    plt.plot(np.fft.fftshift(np.fft.fftfreq(len(x_n),1/(fs*L)))/np.pi,np.fft.fftshift(np.fft.fft(x_n)),'g')
    plt.axvline(x=f(np.pi), color='r')
    plt.title('filtrado')
    plt.grid()
  

# Gráfico de expansión
fig,ax = plt.subplots()
ax.plot(t_n,x_new,'-o')
ax.plot(tc,xc,'-')
ax.plot(t,x,'*')
ax.set_xlabel('tiempo',fontsize=14)
ax.set_ylabel('amplitud',fontsize=14)
plt.title('Expansión')

# Gráfico de la señal y sus muestras
fig,ax = plt.subplots()
ax.plot(t_n,x_n,'-o')
ax.plot(tc,xc,'-')
ax.set_xlabel('tiempo',fontsize=14)
ax.set_ylabel('amplitud',fontsize=14)
plt.title('Interpolación')

plt.figure()
plt.plot(t_n,x_n, '.-')
# plt.plot(x_new, '.-')
# plt.plot(tc, xc)
plt.plot(t, x, 'o-')
plt.title('Senal continua, muestreada, y con incremento de frecuencia')
plt.legend(['x_{i}[n]',  'x[n] (muestreada original)'])

## Error cometido con la interpolación 
##..............................................................................
#sqe2 = (x_n - np.sin(2*np.pi*f*t_n))**2
#
#ax1 = plt.figure().add_subplot(111)
#ax1.fill_between(t_n, x_n, np.sin(2*np.pi*f*t_n), facecolor='b', alpha=0.8)
#ax1.plot(t_n, np.sin(2*np.pi*f*t_n), 'k-')
#ax1.set_xlabel('tiempo', fontsize=18)
#ax1.set_ylabel('Area', fontsize=18)
## ax2 = ax1.twinx()
#ax1.set_title('Error con interpolación')
#
#ax2 = plt.figure().add_subplot(111)
#ax2.plot(t_n, sqe2,'r')
#ax2.axis(xmin=-1,ymax= sqe.max() )
#ax2.set_ylabel('Amplitud', color='k',fontsize=18)
#ax2.set_title('Error con interpolación')
#%%

# respuesta en frecuencia del retenedor de orden cero
wi = -1.9*np.pi*fs
wf = 1.9*np.pi*fs
w = np.arange(wi,wf,0.01)
Ho = np.exp(-1j*w*Ts/2)*np.sin(w*Ts/2)/w
fig, ax4 = plt.subplots()
ax4.plot(w, abs(Ho))
ax4.set_title("$H_0(j\Omega)$")
fig, ax5 = plt.subplots()
Hr = 1/(abs(Ho)+0.0000001)  # FIXME: no se porque invertimos el H_0
ax5.plot(w, Hr)


# retenedor de orden 1
fig, ax6 = plt.subplots()
H1 = np.sin(w*Ts/2)/w/2/Ts
H1 = H1**2
ax6.plot(w, np.abs(H1))
fig, ax7 = plt.subplots()
ax6.set_title("$H_1(j\Omega)$")
Hr = 1/(abs(H1)+0.0000001)    # FIXME: no se porque invertimos el H_0
ax7.plot(w, Hr)


#comparación
Ho_n = np.abs(Ho)
Ho_n = Ho_n/max(Ho_n)
H1_n = np.abs(H1)
H1_n = H1_n/max(H1_n)
fig, ax8 = plt.subplots()
ax8.plot(w, Ho_n, '-r', label='Ho')
ax8.plot(w, H1_n, '-b', label='H1')
plt.title('Comparación de reconstrucción')
plt.legend(loc=2, fontsize="small")
fig, ax9 = plt.subplots()
ax9.plot(w, 1/Ho_n, '-r')
ax9.plot(w, 1/H1_n, '-b')

plt.show()
