# -*- coding: utf-8 -*-
"""
Created on Mon Jun 20 16:04:36 2022

@author: Manu
"""
# Con base en este programa, y en intro_muestreo.py, queda como tarea construir los 3 programas que se detallan en este último. 


import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sg

#%% Creo una señal seno "continua"

f = 2      # Frecuencia de la señal seno

fsc = 10000 * f     # Frecuencia de muestreo "continua"
hsc = 1/fsc       # Paso asociado a la fs "continua"
tf = 1              # Tiempo final del eje de tiempo

t = np.arange(0, tf, hsc)

xc = np.sin(2*np.pi*f*t)

# xc = np.cos(2*np.pi*f*t)   # Ejecutar con coseno para ver casos bordes, para un caso que x está lejos de 0  

#%% Voy a muestrear la señal continua

M = 10  # Cantidad de muestras por ciclo

fs = M * f     # Frecuencia de muestreo (elegir numero divisible entre fsc)
hs = 1/fs

T = hs/hsc
n = np.linspace(0, tf-hs, int(len(t)/T))    

xs = np.sin(2*np.pi*f*n)
# xs = np.cos(2*np.pi*f*n)
    
plt.figure()
plt.plot(n, xs, 'o')
plt.plot(t, xc)
plt.legend(['Muestras', 'Señal continua'])
plt.title('Señal continua y muestreada')

#%% Reconstruccion "ideal". Mostrar que cuantas más muestras tome, mejora esta aproximación. Puede ser subiendo tf o M


xr = np.zeros(len(t))    # Señal reconstruida


for i in range(len(n)):     # Es una sumatoria en n
    xr = xr + xs[i] * (np.sin((np.pi*(t-i*hs))/hs)/(np.pi*(t-i*hs))) * hs

plt.figure()
plt.plot(t, xr)
plt.plot(t, xc)
plt.legend(['Reconstruida', 'Continua'])
plt.title('Reconstrucción con "pasabajos ideal"')


'''Todos los n influyen en todos los n, pero yo fijo n, hago un enventanado. Entonces siempre me podria haber considerado
mas enes de los que me considere (para que sea ideal debería haber tomado infinitos), y la reconstrucción hubiera sida distinta. 

Reconstruir con polinomios tiene la ventaja de que controlo cuántas muestras para adelante necesito.

Finito en frecuencia, infinito en el tiempo. Infinito en frecuencia, finito en el tiempo.'''

#%% Reconstrucción de orden 0

def h0 (T, pos):
    aux = np.zeros(len(t))
    aux[int(pos):int(pos+T*fsc)] = 1
    return aux


xr0 = np.zeros(len(t))    # Señal reconstruida


for i in range(len(n)):     # Es una sumatoria en n
    ho = h0(hs, i*int(hs*fsc))
    xr0 = xr0 + xs[i] * ho
    
plt.figure()
plt.plot(t, xr0)
plt.plot(t, xc)
plt.plot(n, xs, '.')
plt.legend(['Reconstruída', 'Continua', 'Muestreada'])
plt.title('Reconstrucción con retenedor de orden 0')


#%% Reconstruccion de orden 1. Falta arreglar el caso borde.


def h1(T, pos):
    aux = np.zeros(len(t))
    
    aux[int(pos-T):int(pos)] = t[:int(T)] / (T/fsc)       # multiplico por fsc para pasarlo a indice de eje "continuo".
    aux[int(pos):int(pos+T)] = 1 - t[:int(T)] / (T/fsc)   # divido entre T para conseguir la pendiente en la recta que quiero
    return aux                                             

xr1 = np.zeros(len(t))    # Señal reconstruida

for i in range(1, len(n)):     # Es una sumatoria en n. En cero se rompe y no aporta. quizas pueda tratarlo particularmente, matando la parte de la h que se va al negativo

    h_uno = h1(hs*fsc, i*int(hs*fsc))
    xr1 = xr1 + xs[i] * h_uno
    
plt.figure()
plt.plot(n, xs, '.')
plt.plot(t, xc)
plt.plot(t, xr1)
plt.legend(['Muestreada', 'Continua', 'Reconstruída'])
plt.title('Reconstrucción con interpolación lineal')

#%% Reconstruccion de orden 2- chequear condición de borde, ancho y altura de la parabola                                             

def h2(T, pos):
    ind = pos/(hs*fsc)  # ind, indica cuántas veces hs (o Ts) me he movido, es decir dónde está el centro de la parábola
    
    aux = np.zeros(len(t))  
    
    aux[int(pos-1.5*int(T*fsc)):int(pos+1.5*int(T*fsc))] = (-1/(2.25*T**2)) * (t[int(pos-1.5*int(T*fsc)):int(pos+1.5*int(T*fsc))] - T * (ind - 1.5)) * (t[int(pos-1.5*int(T*fsc)):int(pos+1.5*int(T*fsc))] - T * (ind + 1.5))
    
    return aux

# dos = h2(hs, 2*hs*fsc)    # Para probar valores puntuales

# plt.figure()
# plt.plot(t, dos)

# #%%


xr2 = np.zeros(len(t))    # Señal reconstruida

for i in range(2, len(n)):     # Es una sumatoria en n. En cero se rompe y no aporta. quizas pueda tratarlo particularmente, matando la parte de la h que se va al negativo

    h_dos = h2(hs, i*int(hs*fsc))
    xr2 = xr2 + xs[i] * h_dos
    
# plt.figure()
# plt.plot(t, xr2/2)
# plt.plot(t, xc)
    
plt.figure()
plt.plot(n, xs, '.')
plt.plot(t, xc)
plt.plot(t, xr2/2)
plt.legend(['Muestreada', 'Continua', 'Reconstruida'])
plt.title('Reconstrucción con interpolación cuadrática')








