%%
notas={'do','re'};
nota=4;
octava=4;
f = 440.0 * exp(((octava-4)+ (nota-10)/12.0) *log(2));
%f = 440;
sr = 44100;
s = 1;
t = linspace(0,s,sr * s);
y = sin(2*pi*f*t);

figure(1)
plot(t, y);
title('Nota en el tiempo x(t)')
%%
Ts=1/sr;
b = 5;	%cantidad de bits
sample_max = 2^(b-1)-1;
y_q= floor(y*sample_max);

norm(y_q/sample_max-y)/norm(y)
norm(y_q/sample_max-y)/length(y)
%%
e=y*sample_max-y_q;

figure(2)
plot(t(1:500), y(1:500)*sample_max,'-b');
hold on
plot(t(1:500), y_q(1:500),'-r');
hold off

figure(21)
plot(t(1:500), e(1:500),'-r');
title('error de cuantizacion')
%%
a1=audioplayer(y,sr);
a1.play
%%
a2=audioplayer(y_q/sample_max,sr);
a2.play
%%
figure(4)
Y=fftshift(abs(fft(y)));
plot(Y)
%%
figure(5)
Y_q=fftshift(abs(fft(y_q)));
plot(Y_q)
