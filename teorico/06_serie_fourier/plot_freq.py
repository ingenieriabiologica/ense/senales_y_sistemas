# -*- coding: utf-8 -*-
"""
Created on Fri May  3 10:12:21 2019

@author: lucia
"""

import numpy as np
import matplotlib.pyplot as plt


def ss_plot_frec(w, H, f1, f2, d):
    M = max(np.abs(H))
    m = min(np.abs(H))
    umbral = 0.5 * (M - m) * np.ones(len(w)) + m

    plt.figure(f1)
    plt.plot(w, np.abs(H), '-g')
    if d:
        plt.plot(w, np.abs(H), 'or')
    plt.plot(w, umbral, '--k')
    plt.title(u'Módulo de H')

    plt.figure(f2)
    plt.plot(w, np.angle(H), '-g')
    plt.grid(True)
    plt.title(u'Fase de H')
