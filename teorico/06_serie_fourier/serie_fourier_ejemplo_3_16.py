# -*- coding: utf-8 -*-
"""
Editor de Spyder

Este es un archivo temporal
"""

import numpy as np
import matplotlib.pyplot as plt

t = np.arange(0, 2, 0.01);

# %%
plt.figure()
plt.suptitle('Bases')

K = 3
phi = []
color = 'rgb'
for i in range(0, K):
    k = i + 1
    phi.append(np.cos(2 * k * np.pi * t))

    plt.subplot(K, 1, k)
    plt.plot(t, phi[i], color[i])
    plt.xlabel('t')
    plt.ylabel('$\Phi' + str(k) + '$')

# %%
a0 = 1
a = [1 / 4, 1 / 2, 1 / 3]

x0 = a0
x = x0
for k in range(0, K):
    x = x + a[k] * phi[k]

plt.figure()
plt.title('señal original')
plt.plot(t, x)
plt.xlabel('t')
plt.ylabel('x')

# %%
plt.figure()

b0 = 1
y = b0
theta0 = 0
b = []
theta = []
phi_y = []
t_max_phi_y = []
y_max_phi_y = []

for i in range(0, K):
    k = i + 1
    b.append(1 / np.sqrt(1 + 4 * np.pi ** 2 * k ** 2));
    theta.append(-np.arctan(2 * np.pi * k))
    phi_y.append(b[i] * np.cos(2 * np.pi * k * t + theta[i]))
    t_max_phi_y.append((2 * np.pi - theta[i]) / 2 / np.pi / k)
    y_max_phi_y.append(b[i])
    y = y + phi_y[i];
    # phi_y(k,:)=cos(2*pi*k*t+theta(k));
    plt.subplot(K, 1, k)
    plt.plot(t, phi[i], color[i])
    plt.plot(t, phi_y[i], color[i] + ':')
    plt.plot(t_max_phi_y[i], y_max_phi_y[i], 'ok')
    plt.title('$\Phi_' + str(k) + '$')

# %%
plt.figure()
plt.stem(np.arange(0, K + 1), np.concatenate(([b0], b)), 'rs-', markerfmt='ro')
plt.axis([-0.1, 3.1, 0, 1.1])
plt.title('Módulo del sistema')

# %%
plt.figure()
plt.stem(np.arange(0, K + 1), np.concatenate(([theta0], theta)), 'rs-', markerfmt='ro')
plt.axis([-0.1, 3.1, -2, 2])
plt.title('Fase del sistema')

# %%
plt.figure()
plt.plot(t, x, '-r', label='entrada')
plt.plot(t, y, '-b', label='salida')
plt.title('Salida del sistema')
plt.legend()

plt.show()

# %%
print("Fase para cada coeficiente")
print(str(np.round(theta, 2)))
print("Retardo para cada coeficiente")
print(str(np.round(t_max_phi_y, 2)))