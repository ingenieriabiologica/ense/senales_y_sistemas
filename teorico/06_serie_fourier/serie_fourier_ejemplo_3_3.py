#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np

N=5
t = np.arange(-N,N+1,1)
y = np.zeros(2*N+1) 
y[N+1] = 1
y[N-1] = 1
markerline, stemlines, baseline = plt.stem(t, y, '-.')
plt.setp(markerline, 'markerfacecolor', 'r')
plt.setp(baseline, 'color', 'k', 'linewidth', 2)

plt.show()