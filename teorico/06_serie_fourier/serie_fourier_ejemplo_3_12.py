#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import matplotlib.pyplot as plt

N = 100 # usar N par
N1 = 10.0
N_m = math.floor(N/2)
k = np.arange(-N_m, N_m)
f = N1 / N
wo = 2.0 * math.pi / N
ak = 1 / N * np.sin(wo * (N1 + 1 / 2.0) * k) / np.sin(wo / 2.0 * k)
ak[N_m] = (2.0 * N1 + 1) / N

# %%
x = np.arange(-N_m, N_m, 0.1)
xp = np.arange(2, N / 2, 0.1)
seno_numerador = np.sin(wo * (N1 + 1 / 2.0) * xp)
seno_denominador = np.sin(wo / 2.0 * xp)
ax = 1 / N * np.sin(wo * (N1 + 1 / 2.0) * x) / np.sin(wo / 2.0 * x)
ax[10 * N_m] = (2 * N1 + 1) / N
envelope = 1 / N / seno_denominador
# %%
# Análisis del envolvente
plt.figure()
plt.plot(xp, seno_denominador, '--b')
plt.plot(xp, seno_numerador, '-r')
plt.title('Seno denominador')
plt.show()

# revisar esta fase
ak_f = -2 * math.pi * f * k

plt.figure()
my_dpi = 300
plt.plot(k, ak, '.r', markersize=10)
# plt.plot(k, ak, '-g')
plt.plot(x, ax, '-g')
plt.plot(xp, envelope, '--b')
plt.plot(-xp, envelope, '--b')
plt.plot(xp, -envelope, '--b')
plt.plot(-xp, -envelope, '--b')
plt.ylim([-0.1, max(ak) * 1.05])
plt.xlim([-N_m, N_m])
plt.gca().set_position([0.1, 0.1, 0.8, 0.9])
fname = "output/ejemplo_serie_fourier_3_12/espectro_"+str(round(f,3))+".png"
plt.savefig(fname, dpi=my_dpi)

# %%
# Reconstrucción

n = np.arange(-N / 2, N / 2)
x_parcial = n * 0
Nr = round(0.95 * N)  # cantidad de muestras usadas en la reconstrucción (en porcentaje)
rango = range(N - Nr, Nr)
Er = 0
Et = (2.0 * N1 + 1) / N
for i in rango:
    # if i!=N/2:
    x_parcial = x_parcial + ak[i] * np.exp(1j * wo * k[i] * n)
    Er = Er + abs(ak[i]) ** 2

err = abs(Et - Er) / Et * 100
print('err=' + str(round(err, 2)))


# destapar luego
if 1:
    plt.figure()
    plt.plot(n, np.real(x_parcial), '*r')
    tit = 'Reconstrucción parcial N=' + str(Nr)
    tit = tit + ' E=' + str(round(Et, 3)) + ' Er=' + str(round(Er, 3)) + ' err=' + str(round(err, 2))
    plt.title(tit)

    plt.figure()
    plt.plot(n, np.real(x_parcial), '*r')
    plt.plot(n, np.imag(x_parcial), '*b')
    plt.plot(n, x_parcial, '-g')

plt.show()
