#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import os.path

import numpy as np
import matplotlib.pyplot as plt

###############################################################################
#                               Ejemplo 3.5
###############################################################################
# Serie de Fourier de pulso rectangular, se juega con la variación del ancho del
# pulso y ver su efecto en frecuencia.
# se guarda la salida como imagen para poder comparar entre corridas

# valores accesorios para el cálculo numérico
eps = np.finfo(float).eps
# print("eps: ", eps)

# .................Se calculan los valores de los coeficientes ak...............
# El período es T
N = 30
k = np.arange(-N, N)
f = 1.0 / 4  # T_1/T
ak = 1 / math.pi / (k + eps)* np.sin(2 * math.pi * f * k)  # v alores de ak
ak[N] = 2 * f

# ...........................Construyo valores de senc..........................
x = np.arange(-N, N, 0.1)
xp = np.arange(1, N, 0.1)
ax = 1 / math.pi / (x + eps) * np.sin(2 * math.pi * f * x)
envelope = 1 / math.pi / xp
ax[N * 10] = 2 * f

# ..................................Grafico.....................................
my_dpi = 300
# valores de ak
plt.plot(k, ak, '.r', markersize=10)
# gráfica de senc
plt.plot(x, ax, '-g')
# envolventes
plt.plot(xp, envelope, '-b')
plt.plot(-xp, envelope, '-b')
plt.plot(xp, -envelope, '-b')
plt.plot(-xp, -envelope, '-b')
# formato de la imagen
plt.ylim([-0.13, 0.52])
plt.xlim([-N, N])
plt.gca().set_position([0.1, 0.1, 0.8, 0.9])

# código accesorio para guardar
base_dir = "output"
if os.path.exists(base_dir):
    output_dir = os.path.join(base_dir, "ejemplo_serie_fourier_35")
    if not (os.path.exists(output_dir)):
        print("Creando directorio: ", output_dir)
        os.mkdir(output_dir)
    fname = os.path.join(output_dir, "espectro_" + str(round(f, 3)) + ".png")
    plt.savefig(fname, dpi=my_dpi)
else:
    raise Exception("El código deber correr desde la raíz del repositorio")

plt.show()
