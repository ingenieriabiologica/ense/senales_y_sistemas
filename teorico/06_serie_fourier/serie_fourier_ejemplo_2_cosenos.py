#!/usr/bin/env python3

import pygame, math, time
import numpy as np
import matplotlib.pyplot as plt

fs=44100
bits=16
pygame.mixer.init(frequency=fs,size=-bits,channels=1)

k=2**(bits-1)-1

A1=0.95*k
A2=0.05*k

f1 = 400
f2 = 5000
t = np.arange(0,5,1.0/fs)

x1d = A1*np.sin(2*math.pi*f1*t)
x2d = A2*np.sin(2*math.pi*f2*t)

x1i = x1d.astype(np.int16)
x2i = x2d.astype(np.int16)

x3i = x1i + x2i

s1 = pygame.sndarray.make_sound(x1i)
s2 = pygame.sndarray.make_sound(x2i)
s3 = pygame.sndarray.make_sound(x3i)

print("length: "+str(s1.get_length()))

r=np.arange(0, 1000)
s3.play()
plt.plot(t[r], x3i[r])
plt.show()

#time.sleep(10)