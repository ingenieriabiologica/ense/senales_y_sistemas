#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import matplotlib.pyplot as plt

###############################################################################
#                               Ejemplo 3.6
###############################################################################
# .................Se calculan los valores de los coeficientes ak...............
# El periodo es T
N = 30
k = np.arange(-N, N)
f = 1.0 / 4  # T_1/T
ak = 1 / math.pi / k * np.sin(2 * math.pi * f * k)  # Valores de módulo de ak
ak[N] = 0

# ..........................Valores de senc (módulo)............................
x = np.arange(-N, N, 0.1)
xp = np.arange(1, N, 0.1)
ax = 1 / math.pi / x * np.sin(2 * math.pi * f * x)
ax[10 * N] = 0
# Envolvente de los módulos de ak
envelope = 1 / math.pi / xp

# .................................Fase de ak...................................
ak_f = -2 * math.pi * f * k

# ..................................Grafico.....................................
my_dpi = 300
# Modulo de ak
plt.figure()
plt.plot(k, np.abs(ak), '.r', markersize=10)
# Grafica de senc
plt.plot(x, np.abs(ax), '-g')
# Envolvente
plt.plot(xp, envelope, '-b')
plt.plot(-xp, envelope, '-b')
# Ajustes de imagen
plt.title('Módulo de ak')
# plt.ylim([0.49,0.52])
plt.xlim([-N, N])
# plt.show()
plt.gca().set_position([0.1, 0.1, 0.8, 0.9])
fname = "test_" + str(round(f, 3)) + ".png"
plt.savefig(fname, dpi=my_dpi)

# Fase de ak
plt.figure()
plt.plot(k, ak_f, 'or')
plt.plot(k, ak_f, '-g')
# plt.figure()
# plt.plot(x,dx_r,'-g')
# plt.plot(k,dk_r,'*r')
# Ajustes de imagen
plt.title('Fase de ak')
plt.show()
