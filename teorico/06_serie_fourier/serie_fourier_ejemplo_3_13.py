#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 20 13:04:32 2020

@author: lucia
"""
import math
import numpy as np
import matplotlib.pyplot as plt

###############################################################################
#                               Ejemplo 3.13
###############################################################################
#.................Se calculan los valores de los coeficientes ak...............
N=100
N1=20.0
N_m=round(N/2)
k=np.arange(-N/2,N/2)
f=N1/N
wo=2.0*math.pi/N

ak=1/N*np.sin(wo*(N1+1/2.0)*k)/np.sin(wo/2.0*k)
ak[N_m]=(2.0*N1+1)/N + 1/N

#%% 
x=np.arange(-N/2,N/2,0.1)
xp=np.arange(1,N/2,0.1)
ax=1/N*np.sin(wo*(N1+1/2.0)*x)/np.sin(wo/2.0*x)
ax[10*N_m]=(2*N1+1)/N +1/N
#%% 
envelope=1/math.pi/xp


ak_f=-2*math.pi*f*k

my_dpi=300
plt.plot(k,ak,'.r',markersize=10)
plt.plot(k,ak,'-g')
#plt.plot(x,ax,'-g')
#plt.plot(xp,envelope,'-b')
#plt.plot(-xp,envelope,'-b')
#plt.plot(xp,-envelope,'-b')
#plt.plot(-xp,-envelope,'-b')
plt.ylim([-0.1,0.3])
plt.xlim([-N/2,N/2])
#plt.show()
plt.gca().set_position([0.1, 0.1, 0.8, 0.9])
fname="test_"+str(round(N1,3))+".png"
plt.savefig(fname,dpi=my_dpi)

plt.figure()
plt.plot(k,ak_f)
#plt.figure()
#plt.plot(x,dx_r,'-g')
#plt.plot(k,dk_r,'*r')

#%%

n=np.arange(-N/2,N/2)
x_parcial=n*0
Nr = N-1
rango=range(N-Nr,Nr)
Er=0
Et=(2.0*N1+1)/N
for i in rango:
	#if i!=N/2:
	x_parcial=x_parcial+ak[i]*np.exp(1j*wo*k[i]*n)
	Er=Er+abs(ak[i])**2

plt.figure()
plt.plot(n,np.real(x_parcial),'*r')
tit='reconstrucción parcial N='+str(Nr)
tit=tit+' E='+str(round(Et,2))+' Er='+str(round(Er,2))
plt.title(tit)
err=abs(Et-Er)/Et*100
print('err='+str(round(err,2)))
#destapar luego
plt.figure()
plt.plot(n,np.imag(x_parcial),'*r')
plt.plot(n,x_parcial,'-g')
plt.show()