#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 26 16:27:35 2021

@author: lucia
"""
import matplotlib.pyplot as plt
import numpy as np

# Ejemplo 3.2

a = [0, 0, 1 / 3, 1 / 2, 1 / 4, 1, 1 / 4, 1 / 2, 1 / 3, 0, 0]
k = [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5]

plt.figure()
plt.title('Coeficientes de x(t)')
plt.stem(k, a,'r', basefmt=' ')
plt.xlabel('k')
plt.ylabel('$a_k$')
ax1 = plt.gca()
ax1.spines['bottom'].set_position('zero')
ax1.spines['left'].set_position('zero')
ax1.spines['top'].set_visible(False)
ax1.spines['right'].set_visible(False)

t = np.arange(-5, 5, 0.01)
x0 = np.ones(len(t))
x1 = np.cos(2 * np.pi * t) / 2
x2 = np.cos(4 * np.pi * t)
x3 = 2 * np.cos(6 * np.pi * t) / 3

x = x0 + x1 + x2 + x3

plt.figure()

plt.subplot(4, 1, 1)
plt.plot(t, x0)
plt.xlabel('t')
plt.ylabel('$x_0$')

plt.subplot(4, 1, 2)
plt.plot(t, x1)
plt.xlabel('t')
plt.ylabel('$x_1$')

plt.subplot(4, 1, 3)
plt.plot(t, x2)
plt.xlabel('t')
plt.ylabel('$x_2$')

plt.subplot(4, 1, 4)
plt.plot(t, x3)
plt.xlabel('t')
plt.ylabel('$x_3$')

plt.figure()

plt.subplot(3, 1, 1)
plt.plot(t, x0 + x1)
plt.xlabel('t')
plt.ylabel('$x_0+x_1$')
plt.ylim([-1, 4])

plt.subplot(3, 1, 2)
plt.plot(t, x0 + x1 + x2)
plt.xlabel('t')
plt.ylabel('$x_0+x_1+x_2$')
plt.ylim([-1, 4])

plt.subplot(3, 1, 3)
plt.plot(t, x)
plt.xlabel('t')
plt.ylabel('x')
plt.ylim([-1, 4])

plt.show()
