#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 24 14:04:44 2019

@author: lucia
"""

import numpy as np
import matplotlib.pyplot as plt
from plot_freq import ss_plot_frec

# Ejemplo 3.17

# Entrada:              x[n] = cos((2*np.pi/N)*n)
# Respuesta al impulso: h[n]=u[n]*(a**n)

N = 100
f0 = 1.0 / N
wo = 2 * np.pi * f0
wk = np.arange(-N / 2, (N / 2) + 1, 1) * wo
a = 0.2

# %% Transformada de la respuesta al impulso de un SLIT en tiempo discreto
# H(e**(jw))
# H = np.sqrt((1-a*np.cos(wk))**2 + (a**2)*np.sin(wk)**2)**(-1)
H = ((1 - a * np.cos(wk)) + a * np.sin(wk) * 1j) ** (-1)
ss_plot_frec(wk, H, 0, 1, 1)

Hm = np.sqrt(1 + a) ** (-1)
Hhi = np.abs(1 + a) ** (-1)

# %% Respuestas al impulso y al escalón
L = 10
n = np.arange(0, L, 1)
u = np.ones(L)
h = a ** n
s = (1 - a ** (n + 1)) / (1 - a)

plt.figure()
plt.plot(n, u, '-b')
plt.plot(n, h, '-g')
plt.plot(n, h, 'or')
plt.axis([0, L - 1, 0, 1.05])
plt.title(u'h[n]')

plt.figure()
plt.plot(n, 2 * u, '-b')
plt.plot(n, s, '-g')
plt.plot(n, s, 'or')
plt.axis([0, L - 1, 0, 2.05])
plt.title(u's[n]')
plt.show()

n0 = np.ceil(np.log(0.1) / np.log(a))
s0 = (1 - a ** (n0 + 1)) / (1 - a)
