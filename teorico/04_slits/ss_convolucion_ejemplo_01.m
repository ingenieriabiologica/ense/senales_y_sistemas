%%
clear all
close all
home

fs = 10000;                             % Number of samples per second
    t = -0.5:1/fs:0.5;                          % Time vector
    x2 = rectpuls(t,20e-3);                 % Generating rectangular pulse 
    figure(1)
	 plot(t,x2), axis([-0.1 0.1 -0.2 1.2])
	 title('original')
	 
	 t2 = -1:1/fs:1;
	 y=conv(x2,x2);

	 figure(2)
	 plot(t2,y)
	 axis([-0.1 0.1 -1 1.1*max(y)]) 
	  title('convolucion')