# -*- coding: utf-8 -*-
"""
Created on Fri Apr 26 08:56:44 2019

@author: lucia
"""

from scipy import signal
import numpy as np
import matplotlib.pyplot as plt

fs = 1000    # Number of samples per second
t = np.arange(-0.5,0.5+1/fs,1/fs)    # Time vector
x2=[]     # Generating rectangular pulse 
for i in range(0,len(t)):
    if ((t[i]>=(-0.05/2))and(t[i]<(0.05/2))):
        x2.append(1)
    else:
        x2.append(0)

plt.figure(1)
plt.stem(t,x2)
plt.title('original')
plt.xlim([-0.1,0.1])
plt.ylim([-0.2,1.2])

#%%
t2 = np.arange(-1,1+1/fs,1/fs)    # Time vector 2
y = np.convolve(x2,x2,'full') 

plt.figure(2)
plt.stem(t2,y)
plt.title('convolución')
plt.xlim([-0.1,0.1])
plt.ylim([-1,1.1*max(y)])

#%%
plt.figure(3)
plt.stem(t2,y, ':g', markerfmt='go', label='convolucion')
plt.stem(t,x2,':b', markerfmt='bo', label='original')
plt.xlim([-0.1,0.1])
plt.ylim([-1,1.1*max(y)])
plt.grid(axis='y')
plt.legend()
plt.show()