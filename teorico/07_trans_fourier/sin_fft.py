# %%
import matplotlib.pyplot as plt
import numpy as np
import csv
from numpy.fft import fft, fftshift

fs = 44100  # Frecuencia de muestreo del mensaje

A1 = 1  # Amplitud del mensaje

f1 = 400 #frecuencia del mensaje
t1 = 1.0/f1

k = 1000       # Cantidad de ciclos que quiero que dure la señal de mensaje. 
'''--> Probar luego con k = 1 para mostrar que se genera ruido al usar solo un ciclo porque a la fft "le cuesta más" establecer precisamente la frecuencia de la señal.
--> Poner un k no entero, 10.1 por ejemplo, para mostrar el ruido que se produce al empezar y terminar en puntos distintos la señal. Esto sucede por las aproximaciones que toma en los bordes el algoritmo para poder calcular la transformada.'''

tf = k*t1     # Duración de la señal del mensaje

t = np.arange(0, tf, 1.0/fs)    

x1 = A1*np.sin(2*np.pi*f1*t)    # El mensaje es un seno de f = 400 Hz, muestreado a fs = 44100 Hz

sp = (1/len(x1)) * np.fft.fft(x1)              # fft del mensaje. MOSTRAR QUE ES NECESARIO MULTIPLICAR POR 1/largo_señal_tiempo --> sale de la formula de la transformada

freq = np.fft.fftfreq(len(t), d = 1/fs)   # Construcción del eje de frecuencia


plt.figure(1)
plt.stem(freq, abs(sp))
plt.title('fft habiendo usado ' + str(k) + ' ciclos de un seno de ' + str(f1) + 'Hz')

'''Hay dos formas de construir el eje de frecuencias, la primera es la que se muestra arriba, usando la función fftfreq
La siguiente es haciendo un arreglo "a mano", pero hay que hacerle un "shift" a la fft, porque la fft no recorre el eje de frec de forma lineal, entonces si no se hace esto, se van a mostrar los valores de la fft "desorganizados" en el eje. '''

# %%
freq = np.linspace(-0.5, 0.5, len(sp))*fs   # Esto arroja un vector [-fs/2:fs/2] lo cual tiene sentido considerando el teorema de muestreo

'''Ploteamos la fft con el nuevo eje de frecuencia para chequear que arroja un resultado que está mal'''

plt.figure(2)
plt.stem(freq, abs(sp))
plt.title('fft con nuevo eje frec, antes de hacer shift')

'''Hacemos el shift y graficamos nuevamente'''

sp = np.abs(fftshift(sp))

plt.figure(3)
plt.stem(freq, abs(sp))
plt.title('fft con nuevo eje frec')

# %% Para esta parte poner un k no entero, por ejemplo 1.1, 3.1, 10.1. Quiero mostrar los efectos del enventanado. Voy a usar Hamming
# Probar también con un k grande, tipo 1000.1. Se ve que a pesar de que la señal en el tiempo cambia mucho, en frecuencia no, incluso puede decirse que mejora.
ancho = len(t)

v_ham = np.hamming(ancho)

plt.figure(4)
plt.plot(t, v_ham)
plt.title('Ventana de Hamming')

'''Aplico la ventana'''
x1_ham = x1 * v_ham

sp_ham = (1/len(x1_ham)) * np.fft.fft(x1_ham) 

freq = np.fft.fftfreq(len(t), d = 1/fs) 

plt.figure(5)
plt.plot(t, x1_ham)
plt.title('Señal en el tiempo, después del enventanado')

plt.figure(6)
plt.stem(freq, abs(sp_ham))
plt.title('fft con enventanado Hamming')

'''Se puede ver que el enventanado dismunuye las oscilaciones, pero suma ruido en las componentes más cercanas a la frecuencia fundamental del seno'''

plt.show()
