#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  5 17:13:30 2022

@author: lucia
"""

import numpy as np
import matplotlib.pyplot as plt

# Ejercicio 5.3

N1 = 40
n = np.arange(-3*N1, 3*N1+1, 1)
x = np.zeros(len(n))
x[abs(n) <= N1] = 1

plt.figure()
plt.stem(n, x)
plt.xlabel('n')
plt.ylabel('x[n]')


#%%
w = np.arange(0, 4, 0.01)
X = np.sin(w*np.pi*(N1+0.5))/np.sin(w*np.pi*0.5)

X[0]=2*N1+1

plt.figure()
plt.plot(w,X)
plt.ylabel('$X(e^{j\omega})$')
plt.xlabel('$w/\pi$')
plt.grid()

#%%

X_fft = np.fft.fft(x)
freq = np.fft.fftfreq(len(n), d = 1)

plt.figure()
plt.plot(2*np.pi*freq, np.abs(X_fft), '.-')
plt.show()