#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from scipy import signal, io

# %%
N = 50
n = np.arange(-N, N)
a = 0.9
xn = np.zeros(len(n))
n_mid = N
n_max = 2 * N
ri = np.arange(0, n_mid)
rd = np.arange(n_mid, n_max)
xn[ri] = np.power(a, -n[ri])
xn[rd] = np.power(a, n[rd])
plt.plot(n, xn, '.r', markersize=10)
plt.plot(n, xn, '-g')

# %%
# dos formas de calcular la magnitud de la transformada
Nw = 6 * N + 1
fw = 1.0 / Nw
w = math.pi * np.arange(-3, 3, fw)

X2 = (1 - a ** 2) / (1 - 2 * a * np.cos(w) + a ** 2)

my_dpi = 300
plt.figure()
plt.plot(w, np.abs(X2), '-g')
plt.title(u'$|X(e^{jw})|$')

# %%
plt.figure()
plt.plot(w, np.angle(X2), '-b')
plt.grid(color='k', linestyle='--', linewidth=1)
plt.title(u'Fase')
plt.show()
# %%
# compare with 5.1
X = np.load('X_51.npy')
plt.figure()
plt.plot(w, np.abs(X), '-g')
plt.plot(w, np.abs(X2), '-r')
plt.title(u'Comparación ejemplos 5.1 y 5.2')
plt.legend(['5.1', '5.2'])
plt.show()
