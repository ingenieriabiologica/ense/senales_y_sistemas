import math

import numpy as np
import matplotlib.pyplot as plt

w_max = 3
w = np.arange(-w_max, w_max, 0.01)

N = len(w)  # sample count

a = 0.5

X_41 = 1./(a+1.0j*w)
X_42 = 2*a/(a**2+w**2)

plt.figure(1)
plt.plot(w, abs(X_41), '-b')
plt.plot(w, X_42, '-g')
plt.legend(['X_41','X_42'])
# axis([w(1) w(end) 0 1.1])
n1 = math.floor(N/2)
w_p1 = w[n1]
n2 = np.argmin(np.abs(w-1/a))
w_p2 = w[n2]
X_41_p1 = abs(X_41[n1])
X_41_p2 = abs(X_41[n2])
X_42_p1 = abs(X_42[n1])
X_42_p2 = abs(X_42[n2])
plt.plot(w_p1, X_41_p1, '.r', markersize=10)
plt.plot(w_p2, X_41_p2, '.r', markersize=10)
plt.plot(w_p1, X_42_p1, '.r', markersize=10)
plt.plot(w_p2, X_42_p2, '.r', markersize=10)
plt.show()
