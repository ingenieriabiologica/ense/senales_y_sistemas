import math
import numpy as np
import matplotlib.pyplot as plt

w_max = 2*math.pi
w = np.arange(-w_max, w_max, 0.01)

N = len(w)  # sample count


G = 2*(np.sin(w)/w-np.cos(w))
X = G/(1.0j*w)

plt.figure(1)
plt.plot(w, abs(G), '-b')
plt.title('G(jw)')
plt.figure(2)
plt.plot(w, abs(X), '-b')
plt.title('X(jw)')

n1 = math.floor(N/2)
w_p1 = w[n1]
w_p1 = abs(X[n1])
plt.plot(w_p1, w_p1, '.r', markersize=10)
plt.show()
