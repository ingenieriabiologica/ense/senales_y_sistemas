import numpy as np
import matplotlib.pyplot as plt

w = 3
w = np.arange(-w, w, 0.01)

W1 = 2
W2 = 1
t = np.linspace(-10, 10, len(w))

N = len(w)  # sample count
P1 = 2  # period
P2 = 1
D = 5  # width of pulse
X1 = np.abs(t) < P1
X2 = np.abs(t) < P2


plt.figure(1)
plt.plot(w, X1, '-b')
plt.plot(w, X2, '-r')
plt.legend(['X1', 'X2'])
# axis([w(1) w(end) 0 1.1])

x1 = W1 / np.pi * np.sinc(W1 * t / np.pi)
x2 = W2 / np.pi * np.sinc(W2 * t / np.pi)

plt.figure(2)
plt.plot(t, x1, '-b')
plt.plot(t, x2, '-r')
plt.legend(['x1', 'x2'])
plt.show()
