%%
clear all
close all
home
%%
N=1000;
tf=5;
ti=0;
dt=(tf-ti)/(N-1);	% Ts, per?odo de muestreo
fs=1/dt;
t=[ti:dt:tf];
%%
col='rgb';
K=3;
P=zeros(K,N);
phi=zeros(K,N);
f=[1:K]*10;
%%
for k=1:K
	phi(k,:)=cos(2*pi*f(k)*t);
	R=floor(N/K);
	P(k,R*(k-1)+1:R*k)=1;
	figure(10)
	subplot(K,1,k)
	plot(t,phi(k,:),col(k))
	title(['\phi_' num2str(k) '(t)'])
	figure(11)
	subplot(K,1,k)
	plot(t,P(k,:),col(k))
	title(['P_' num2str(k) '(t)'])
end
[M1, x1]=min(P(1,:));
[M2, x2]=max(P(3,:));
%%
figure(12)
xt=zeros(1,N);
for k=1:K
	xt=xt+phi(k,:);
end
plot(t,xt)
%%
a0=1;
a=[1/4 1/2 1/3];
figure(2)
x=a0;

for k=1:K
	x=x+P(k,:).*phi(k,:);
end
x=x-mean(x);
rango1=[min(x) max(x)];
Mr=max(abs(rango1))*1.05;
rango=[-Mr Mr];
plot(t,x)
hold on
plot(t([x1 x1]),rango,'r','linewidth',2)
plot(t([x2 x2]),rango,'g','linewidth',2)
hold off
axis([0 t(end) -Mr Mr])
M=sum(x.*dt)/tf;
%%
trafo=zeros(K,N);
figure(3)
clf
hold on
rango=[400:600];
f=fs/2*linspace(-1,1,N);
leyenda=cell(1,3);
for k=1:K
	trafo(k,:)=abs(fftshift(fft(phi(k,:))))/N;
	semilogy(f(rango),trafo(k,rango),col(k))
	leyenda{k}=['\Phi_' num2str(k) '(jw)'];
end
hold off
legend(leyenda)
title('Transformadas de Fourier')
%%
figure(4)
plot(abs(fftshift(fft(x)))/N)
title('Transformada de Fourier de la se?al completa')
%%
figure(5)
win=128;
overlap=win/2;
nfft=128;
spectrogram(x,win,overlap,nfft,fs);
[S,F,T]=spectrogram(x,win,overlap,nfft,fs);
title('Espectrograma')
figure(6)
Smod=abs(S);
mesh(Smod)
title('Espectrograma 3D')
%%
[m,n]=size(Smod);
maximos=zeros(1,n);
frec=zeros(1,n);
for i=1:n
	frec_m=Smod(:,i);
	[maximos(i),frec(i)]=max(frec_m);
end
figure(7)
plot(T,F(frec))
title('frecuencias detectadas en funci?n del tiempo')
xlabel('t')
ylabel('f')