%%
W=3;
w=[-W:0.01:W];
W1=1;
W2=2;
t=[-10:0.01:10];
figure(21)
X1=rectpuls(w,W1);
X2=rectpuls(w,W2);
plot(w,X1,'-b');
hold on
plot(w,X2,'-r');
hold off
legend('X1','X2')

axis([w(1) w(end) 0 1.1])
figure(22)
x1=W1/pi.*sinc(W1*t/pi);
x2=W2/pi.*sinc(W2*t/pi);
plot(t,x1,'-b')
hold on
plot(t,x2,'-r')
hold off
legend('x1','x2')

hold on
