#!/usr/bin/env python3

import math
import numpy as np
import matplotlib.pyplot as plt


# %%
N = 50
n = np.arange(-N, N)
a = 0.9
xn = np.zeros(len(n))
n_mid = N
n_max = 2 * N
ri = np.arange(0, n_mid)
rd = np.arange(n_mid, n_max)
xn[rd] = np.power(a, n[rd])
plt.plot(n, xn, '.r', markersize=10)
plt.plot(n, xn, '-g')

# %%
# dos formas de calcular la magnitud de la transformada
Nw = 6 * N + 1
fw = 1.0 / Nw
w = math.pi * np.arange(-3, 3, fw)

# transformada calculada como complejo
X = 1 / (1 - a * np.exp(-1j * w))
# módulo calculado análiticamente
Xmod = 1 / np.sqrt((1 - a * np.cos(w)) ** 2 + (a * np.sin(w)) ** 2)

my_dpi = 300
use_umbral = 0
# calculo el umbral para representar un período
umbral = [min(np.abs(X)), max(np.abs(X))]
w_u = np.array([math.pi, math.pi])

plt.figure()
plt.plot(w, np.abs(X), '-g')
plt.title(u'Módulo (calculado con np.abs())')
if use_umbral:
    plt.plot(-w_u, umbral, '--k')
    plt.plot(w_u, umbral, '--k')

plt.figure()
plt.plot(w, Xmod, '-r')
# habilitar en una segunda corrida
if use_umbral:
    plt.plot(-w_u, umbral, '--k')
    plt.plot(w_u, umbral, '--k')
plt.title(u'Expresión análítica del módulo')

# %%
plt.figure()
plt.plot(w, np.angle(X), '-b')
plt.grid(color='k', linestyle='--', linewidth=1)
plt.title('Fase (calculado con np.angle())')
plt.show()
# guardo el módulo de X para el ejercicio siguiente
np.save('X_51.npy', X)
