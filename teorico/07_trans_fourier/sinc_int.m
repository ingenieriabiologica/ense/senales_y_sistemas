function y=sinc_int(t,w,T1)

dw=w(2)-w(1);
y=1/2/pi*sum(2*sin(w*T1)./(w+eps).*exp(1i*w*t)*dw);