%%
%close all
%clear all
home
%%
W=100;
fprintf('Wmax: %g\n',W)
dw = 0.01;
w=[-W:dw:W];
T1=1;
t=[-10:0.01:10];

% Reconstrucción
yr=zeros(size(t));
for i=1:length(t)
	yr(i)=sinc_int(t(i),w,T1);
end

fprintf('max(y_r(t)): %g\n',max(real(yr)))
fprintf('min(y_r(t)): %g\n',min(real(yr)))
%%
% Reconstrucción en t=T1
yr_T=sinc_int(T1,w,T1);
fprintf('y_r(T1): %g\n',abs(yr_T))
%%
y=rectpuls(t,2*T1);
figure()
plot(t,y, '-b')
hold on
plot(t,real(yr), '-r')
hold off
legend('y','y_r')
%%
dt=t(2)-t(1);
e_medio=sum(abs(yr-y).*dt);
e_medio_rel=sum(abs(yr-y).*dt)/sum(abs(y).*dt);
fprintf('e_medio: %g\n',e_medio)