#!/usr/bin/env python3
import math
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from scipy import signal

N=200
t = np.linspace(0,10,N) # create a time signal
dt=t[2]-t[1]
x1 = np.sin(t) # create a simple sine wave
xn = np.sin(100*t) # add noise to the signal
x2=x1+xn

a=0.5
ak=[1, -a];
bl=1;
y2 = sp.signal.filtfilt(bl, ak, x2)

# %%
# plot the results
plt.plot(t,x1,'k-')
plt.plot(t,x2,'r-')
plt.title('Uniform noise')
plt.xlabel('time')
plt.show()
# %%

plt.plot(t,x1,'k-')
plt.plot(t,y2,'r-')
plt.title('Uniform noise (median)')
plt.show()