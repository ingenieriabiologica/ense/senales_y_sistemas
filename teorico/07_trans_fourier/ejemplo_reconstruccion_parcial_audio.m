%%
clear all
close all
home
%% leer se?al de audio
[s,fs]=audioread('voice2.wav');
L=length(s);
%L=round(L/10)
%s=s(1:L)
Ts=1/fs;
t=[1:L]*Ts;
t_max = L*Ts;
figure(10)
plot(t,s)
title('Señal original en el tiempo')
axis([0 t_max -1 1])
fprintf('Duracion: %3.2g segs.\n',t_max)
%% Reproducci?n de audio
ap=audioplayer(s,fs);
ap.play
%% Calculo de la transformada de Fourier
f=fs/2*linspace(-1,1,L);
S=fft(s);
t_mod=fftshift(abs(S));
figure(12)
plot(f,t_mod/max(t_mod))
title('Espectro de la señal original')
df=f(2)-f(1);
%% Selecci?n de un rango del espectro
f_c=500;
[m,ind]=min(abs(f-f_c));
[m2,ind2]=min(abs(f+f_c));
t_mod_int=t_mod(ind:end);
L_int=length(t_mod_int);
E_int=sum(t_mod_int.^2)*df;
E=sum(t_mod.^2)*df;
E_r=2*E_int/E*100;
fprintf('Energia descartada: %4.2f%%\n',E_r);
figure(12)
hold on
plot([f_c f_c],[0 1],'r','linewidth',2)
plot([-f_c -f_c],[0 1],'r','linewidth',2)
hold off
%% Filtro pasabajos ideal
mascara=zeros(L,1);
mascara(ind2:ind)=1;
figure(40)
plot(f,t_mod/max(t_mod), '-b')
hold on
plot(f,mascara, '-r','linewidth',2)
hold off
axis([min(f) max(f) 0 1.05])
Sr=ifftshift(fftshift(S).*mascara);
sr=real(ifft(Sr));
figure(41)
Sr_mod_shift=abs(fftshift(Sr));
plot(f,Sr_mod_shift)
figure(42)
plot(t,sr)
tit=sprintf('Reconstrucción en el tiempo (descartando: %4.2f%%)\n',E_r);
title(tit)
axis([0 t_max -1 1])
%%
apr=audioplayer(sr,fs);
apr.play
%%
f = 10;
Se=[S ; zeros(f*L,1)];
fse=f*fs;
Le=length(Se);
Se_mod=fftshift(abs(Se));
fe=fse/2*linspace(-1,1,Le);
%%
figure(5)
plot(fe,Se_mod/max(Se_mod))
se=ifft(Se);
figure(6)
plot(real(se))
%%
ap=audioplayer(se,fse);
ap.play