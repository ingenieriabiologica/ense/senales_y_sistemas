# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

'''Visualizacion:
                ver_todo:   muestra todas las graficas del codigo si esta en True
                            independiente de los valores que se le de a los otros 
                            controladores
                    
                Los otros:  si una variable esta en True y ver_todo = False se va a
                            mostrar solo las graficas referidas al nombre de dicha 
                            variable.                
'''

''' DESCRIPCION:
            1-  Grafico 3 senos de diferentes frecuencias
            2-  Grafico 3 pulsos que duran lo mismo que los senos pero se prenden en un tercio 
                de la duración
            3-  Seno_sum es la suma de los 4 senos
            4-  Seno_mezcla es la mezcla de los tres senos, es decir es el producto de pulso 1 por seno 
                mas pulso 2 por seno 2 y asi sucesivamente
            5-  Hago latransformada dela suma y de lamezcla
            6-  Hago elespectrograma de la suma de senos
            7-  Hago el espectrograma de la mezcla 
            8-  Hagos los espectrogramas de la suma y mezcla en 3D (estos pueden estar en ejes log o decimal) 
'''

ver_todo = True

ver_senos = True
ver_pulsos = True
ver_suma_senos = True
ver_mezcla_senos = True
ver_transformadas_senos = True
ver_trans_suma_mezcla = True
ver_espectrogramas = True
ver_3D_espectrograma = True

#%% 
''' Defino el vector de tiempo que voy a usar'''
dt = 0.0001
t = np.arange(0.0, 6.0, dt)

# fase de cada seno (sirve para análisis en tiempo y frecuencia, cap. 6)
# para el resto de los ejemplos debe ir en cero
theta1 = 0
theta2 = 0
theta3 = 0

''' Defino los senos de diferentes frecuencias que voy a usar'''
s1 = np.sin(2 * np.pi * 10 * t + theta1)
s2 = np.sin(2 * np.pi * 30 * t + theta2)
s3 = np.sin(2 * np.pi * 50 * t + theta3)

if ver_senos or ver_todo:
    fig1, axs1 = plt.subplots(3)
    fig1.suptitle('Sin')
    axs1[0].plot(t, s1, 'r')
    axs1[1].plot(t, s2, 'g')
    axs1[2].plot(t, s3, 'b')
    plt.xlabel('t[s]')
    if ver_senos and not ver_todo:
        plt.show()

#%%
''' Defino los pulsos prendidos en diferentes tiempos que voy a usar'''
nmax = len(t)
n1 = np.int(np.floor(len(t) / 3.0))
n2 = np.int(np.ceil(len(t) * 2.0 / 3))
p1 = np.concatenate([np.ones(n1), np.zeros(n2)])
p2 = np.concatenate([np.zeros(n1), np.ones(n1), np.zeros(nmax-2*n1)])
p3 = np.concatenate([np.zeros(n2), np.ones(n1)])

if ver_pulsos or ver_todo:
    fig2, axs2 = plt.subplots(3)
    fig2.suptitle('Pulsos')
    axs2[0].plot(t, p1, 'r')
    axs2[1].plot(t, p2, 'g')
    axs2[2].plot(t, p3, 'b')
    plt.xlabel('t[s]')
    if ver_pulsos and not ver_todo:
        plt.show()

#%%
''' Suma de los tres senos'''
suma_senos = s1+s2+s3

if ver_suma_senos or ver_todo:
    plt.figure(3)
    plt.plot(t, suma_senos)
    plt.title('Suma de los tres senos')
    plt.xlabel('t[s]')
    if ver_suma_senos and not ver_todo:
        plt.show()

''' Mezclo en un mismo eje de tiempo los tres senos
    usando los pulsos para que queden en diferentes tiempos'''
mezcla_senos = s1*p1+s2*p2+s3*p3

if ver_mezcla_senos or ver_todo:
    plt.figure(4)
    plt.plot(t, mezcla_senos, 'r')
    plt.axvline(t[int(len(t) / 3)], 0, 1)
    plt.axvline(t[int(len(t) * (2/3))], 0, 1)
    plt.title('Mezcla de senos')
    plt.xlabel('t[s]')
    if ver_mezcla_senos and not ver_todo:
        plt.show()

#%%
''' Transformada de Fourier de cada seno'''
freq_ts = np.fft.fftfreq(t.shape[-1], d = dt)

ts1 = np.fft.fft(s1)
ts2 = np.fft.fft(s2)
ts3 = np.fft.fft(s3)

if ver_transformadas_senos or ver_todo:
    plt.figure(5)
    plt.plot(freq_ts, abs(ts1), 'r', label='F1')
    plt.plot(freq_ts, abs(ts2), 'g', label='F2')
    plt.plot(freq_ts, abs(ts3), 'b', label='F3')
    plt.xlim(-0.006, 0.006)
    plt.title('Transformadas de Fourier')
    plt.legend()
    if ver_transformadas_senos and not ver_todo:
        plt.show()

#%%
''' Transformada de Fourier de suma y mezcla'''
tsuma_senos = np.fft.fft(suma_senos)
tmezcla_senos = np.fft.fft(mezcla_senos)

if ver_trans_suma_mezcla or ver_todo:
    fig3, axs3 = plt.subplots(2)
    fig3.suptitle('Transformada de la suma y de la mezcla')
    axs3[0].stem(freq_ts, abs(tmezcla_senos), 'g', label='mezcla senos')
    axs3[0].title.set_text("Transformada de la mezcla")
    
    axs3[1].stem(freq_ts, abs(tsuma_senos), 'r', label='suma senos')
    axs3[1].title.set_text('Transformada de la suma')
    
    plt.xlabel('Frecuencias [rad/s]')
    plt.legend()
    if ver_trans_suma_mezcla and not ver_todo:
        plt.show()

#%%
'''Espectrogramas'''

NFFT = 1024       # the length of the windowing segments
Fs = int(1.0/dt)

if ver_espectrogramas or ver_todo:
    plt.figure(7)
    ax1 = plt.subplot(211)
    plt.plot(t, suma_senos, 'r')
    plt.title('Suma de senos y espectrograma')
    plt.subplot(212, sharex=ax1)
    plt.specgram(suma_senos, NFFT=NFFT, Fs=Fs, noverlap=900, cmap=plt.cm.gist_heat, scale='dB')

    plt.figure(8)
    ax1 = plt.subplot(211)
    plt.plot(t, mezcla_senos, 'r')
    plt.axvline(t[int(len(t) / 3)], 0, 1)
    plt.axvline(t[int(len(t) * (2/3))], 0, 1)
    plt.title('Mezcla de senos y espectrograma')
    ax2 = plt.subplot(212, sharex=ax1)
    plt.specgram(mezcla_senos, NFFT=NFFT, Fs=Fs, noverlap=900, cmap=plt.cm.gist_heat, scale='dB')
    ax2.set_ylim([0, 100])
    if ver_espectrogramas and not ver_todo:
        plt.show()

#%%
'''Espectrograma 3D'''
# extract the spectrum

freq_bins, timestamps, spec = signal.spectrogram(mezcla_senos, NFFT)
freq_bins_suma, timestamps_suma, spec_suma = signal.spectrogram(suma_senos, NFFT)

if ver_3D_espectrograma or ver_todo:
    fig8 = plt.figure(9)
    ax8 = fig8.gca(projection='3d')
    ax8.plot_surface(freq_bins[:, None], timestamps[None, :], spec, cmap=cm.coolwarm)
    plt.title('Espectrograma de la mezcla de senos (eje decimal)')
    plt.ylabel('Frecuencia [Hz]')
    plt.xlabel('Tiempo [s]')

    fig9 = plt.figure(10)
    ax9 = fig9.gca(projection='3d')
    ax9.plot_surface(freq_bins_suma[:, None], timestamps_suma[None, :], 10.0*np.log10(spec_suma), cmap=cm.coolwarm)
    plt.title('Espectrograma de la suma de senos (eje log)')
    plt.ylabel('Frecuencia [Hz]')
    plt.xlabel('Tiempo [s]')

    if ver_3D_espectrograma and not ver_todo:
        plt.show()

# Para ver todas las gráficas
if ver_todo:
    plt.show()
