#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from scipy import signal,io

#%%
N = 30
n = np.arange(-N, N)
xn = np.zeros(len(n))
xn[N] = 1
plt.plot(n, xn, '.r', markersize=10)
plt.plot(n, xn, '-g')
plt.title(u'$\delta[n]$')

#%%
# ir variando W hasta que tienda a pi para ver como converge
Nw = 20
f = 1
W = f*math.pi
xnr = np.sin(W*n)/math.pi/n
xnr[N] = W/math.pi

my_dpi = 300
plt.figure()
plt.plot(n,xnr,'-g')
plt.plot(n,xnr,'.r',markersize=10)

plt.ylim([-0.2, 1.05])
#plt.gca().set_position([0.1, 0.1, 0.8, 0.95])
plt.title(u'$\hat{x}[n]$')
fname = "fig_ej_5_4_"+str(round(f, 3))+".png"
plt.savefig(fname, dpi=my_dpi)
plt.show()
