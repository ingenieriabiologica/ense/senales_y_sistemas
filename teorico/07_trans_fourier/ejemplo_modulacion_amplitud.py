#!/usr/bin/env python3
# coding=utf-8

import math
import numpy as np
import matplotlib.pyplot as plt
from numpy.fft import fft, fftshift

fs = 44100

A1 = 0.2  #Amplitud del mensaje
A2 = 1 #Amplitud de portadora
B1 = 0.8 #Offset de mensaje
B2 = 0 #Offset de portadora

f1 = 400 #frecuencia del mensaje
f2 = 5000 #frecuencia de portadora

t = np.arange(0, 1, 1.0/fs)

x1 = A1*np.sin(2*math.pi*f1*t)+B1 #mensaje
x2 = A2*np.sin(2*math.pi*f2*t)+B2 #portadora

x3 = x1*x2 #señal modulada


r = np.arange(0, 300)

plt.figure()
plt.subplot(2,1,1)
plt.plot(t[r],x1[r],'r')
plt.title('Mensaje')
plt.subplot(2,1,2)
plt.plot(t[r],x2[r],'b')
plt.title('Portadora')

plt.figure()
plt.plot(t[r], x3[r])
plt.plot(t[r], np.abs(x1[r]), 'g')
plt.plot(t[r], -np.abs(x1[r]), 'r')
plt.title(u'Señal modulada')

# plt.plot(t,x3)
# plt.show()
# time.sleep(10)

X1 = fft(x1)
X2 = fft(x2)
freq = np.linspace(-0.5, 0.5, len(X1))*fs
X1mod = np.abs(fftshift(X1))
X2mod = np.abs(fftshift(X2))


plt.figure()
plt.subplot(2,1,1)
plt.plot(freq, X1mod/np.max(X1mod),'r')
plt.title(u'Transformada del mensaje')
plt.subplot(2,1,2)
plt.plot(freq, X2mod/np.max(X2mod),'b')
plt.title(u'Transformada de la portadora')


X3 = fft(x3)
freq = np.linspace(-0.5, 0.5, len(X3))*fs
X3mod = np.abs(fftshift(X3))


plt.figure()
plt.plot(freq, X3mod/np.max(X3mod))
plt.title(u'Transformada de la señal modulada')
plt.show()

