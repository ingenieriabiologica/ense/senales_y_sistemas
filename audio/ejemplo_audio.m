%% leer se?al de audio
[a,fs]=audioread('voice2.wav');
N = length(a);
Ts = 1/fs;
t = [1:N]*Ts; %#ok<NBRAK>
plot(t,a)
title('voz hablada')
fprintf('Duraci�n: %d muestras %2.1f segs.\n',N, t(end))
%% reproducir la voz hablada
player=audioplayer(a,fs);
player
player.play
%% ejemplo de sonidos tonales y atonales
ruido=randn(1,150000);
figure(2)
plot(ruido(1:10000))
title('ruido blanco')
%% reproducir el ruido
player2=audioplayer(ruido,fs);
player2.play
%% parar la reproducci�n
player2.stop
%% Cambiar la frecuencia de muestreo
player_lento=audioplayer(a,fs/2);
player_lento.play
%% aumentar la frecuencia de muestreo
player_rapido=audioplayer(a,fs*2);
player_rapido.play
%% aumentar al rango no audible
player_rapido=audioplayer(a,fs*10);
player_rapido.play