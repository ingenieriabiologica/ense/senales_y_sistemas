# -*- coding: utf-8 -*-
"""
Created on Mon Jan 30 12:33:58 2023

@author: Manu
"""

import numpy as np
import matplotlib.pyplot as plt

'''Ejercicio 1: tipos de datos y listas'''

# 1
lista1 = ["a", "b", "c"]

# 2 
lista2 = [0, 1, 2]

# 3
lista1.append("d")
lista2.append(3)

# 4
lista1.append(3)
lista2.append("e")


# 5
lista1.pop()
lista2.pop()

# 6 
print(lista1[1])
print(lista2[1])

# 7
lista2a1 = lista2[:]
lista2a2 = lista2[:3]
lista2a3 = lista2[1:]
lista2a4 = lista2[:-1]

# 8
lista3 = []

# 9
lista3.append([lista1[0], lista2[0]])
lista3.append([lista1[1], lista2[1]])
lista3.append([lista1[2], lista2[2]])
lista3.append([lista1[3], lista2[3]]) 

# 10
print("La letra " + lista3[0][0] + " es la numero " + str(lista3[1][1]) + " del " + lista3[0][0] + 
      lista3[1][0] + "e" + lista3[2][0] + "e" + lista3[3][0] + lista3[0][0] + "rio")

# 11
lista4 = [float(0), float(1), float(2), float(3)]

# 12
# S = string
# I = integer
# F = float

# Casos para lista1 (string)
doce1 = lista1[0] + lista1[1] # S + S = S
# doce2 = lista1[0] + lista2[2] # Error S + I
# doce3 = lista1[1] + lista4[3] # Error S + F
# doce4 = lista1[0] * lista1[1] # Error S * S
# doce5 = lista1[0] * lista1[2] # Error S * I
# doce6 = lista1[1] * lista1[3] # Error S * F

# Casos para lista2 (int)
doce7 = lista2[0] + lista2[1] # I + I = I
doce8 = lista2[1] + lista4[3] # I + F = F
doce9 = lista2[0] * lista2[1] # I * I = I
doce10 = lista2[1] * lista4[3] # I * F = F

# Casos para lista4 (float)
doce11 = lista4[0] + lista4[1] # F + F = F
doce12 = lista4[0] + lista4[1] # F * F = F




#%% 
'''Ejercicio 2: numpy y matplotlib.pyplot'''

# 1
lista = np.array(lista2)
lista = list(lista)

# 2
lista3 = np.array(lista3)
print("lista1: " + str(lista3[:, 0]))
print("lista2: " + str(lista3[:, 1])) # Notar que los valores quedan en tipo string

# 4
print("Primer valor de los ultimos dos elementos: " + str(lista3[2:,0]))

# 5
# Da error, "list indices must be integers or slices, not tuple". 

# 6
t1 = np.arange(0, 10, 0.1)

# 7
t2 = np.linspace(0,10,100)

# 8
f = 1
sen = np.sin(2*np.pi*f*t1)

# 9
sen1 = []
for i in range(len(t1)):
    sen1.append(np.sin(2*np.pi*f*t1[i]))

# 10
cos = np.cos(2*np.pi*f*t1)
exp = np.exp(t1)
log = np.log(t1)
cte = np.ones(len(t1))*0.5

# 11
plt.figure()
plt.plot(t1, sen, ".-") # '.-' lo uso para mostrar la interpolación lineal que realiza Python
plt.plot(t1, cos, '.-')
plt.plot(t1, cte, '.-')
plt.legend(['Seno', 'Coseno', 'Constante'])

plt.figure()
plt.plot(t1, exp, '.-')
plt.plot(t1, log, '.-')
plt.legend(['Exponencial', 'Logaritmo'])

# 12
fib = [1,1]
cant = 10   # Cantidad de numeros de la serie

for i in range(cant-2):
    fib.append(fib[i]+fib[i+1])













