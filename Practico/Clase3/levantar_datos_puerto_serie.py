# -*- coding: utf-8 -*-
"""
Created on Thu Nov  9 23:05:42 2023

@author: Ing. Biologica
"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#%% Levantar y graficar datos generados en Arduino y leidos directo por comunicacion serie
# Defino el nombre del archivo que tiene los datos que voy a visualizar
nombre_archivo_ard = 'datos_Arduino.txt'

# Cargo los datos
data = np.loadtxt(nombre_archivo_ard, delimiter = ',')

# Los primeros datos suelen tener errores. Ajustar segun convenga
data = data[1:, ] #    Notar que la fs queda definida por el delay que puse en el codigo de Arduino

# Extraigo de data los vectores que me interesan
tiempo1 = data[:, 0]/1000    # Paso de ms a s
sig1 = data[:, 1]/1000       # Paso de mV a V

# Grafico los datos
plt.figure()
plt.plot(tiempo1, sig1, '.-')
plt.xlabel('Tiempo (s)')
plt.ylabel('Señal (V)')
plt.title('Datos generados en Arduino, leidos directo por com. serie.')
plt.grid()
plt.show()

#%% Levantar y graficar datos generados en Arduino y medidos por el AD2

# Defino el nombre del archivo que tiene los datos que voy a visualizar
nombre_archivo_ad2 = 'datos_AD2.csv'

# Cargo los datos. Es posible que sea necesario eliminar la informacion que esta comentada en los datos al principio. Tambien, si hay un salto de linea al final de todo, eliminarlo.
data = pd.read_csv(nombre_archivo_ad2, sep = ',') # Por ahora se está usando tipo caja negra

tiempo2 = np.array(data['Time (s)'])
sig2 = np.array(data['Channel 1 (V)'])

# Grafico los datos
plt.figure()
plt.plot(tiempo2, sig2, '.-')
plt.xlabel('Tiempo (s)')
plt.ylabel('Señal (V)')
plt.title('Datos generados en Arduino y medidos por el AD2.')
plt.grid()
plt.show()

#%% Levantar y graficar datos generados en el AD2 y leidos en Arduino

# Defino el nombre del archivo que tiene los datos que voy a visualizar
nombre_archivo_ard = 'datos_Arduino_osciloscopio.txt'

# Cargo los datos
data = np.loadtxt(nombre_archivo_ard, delimiter=',')

# Los primeros datos suelen tener errores. Ajustar segun convenga
data = data[11:, ]

# Extraigo de data los vectores que me interesan
tiempo3 = data[:, 0]/1000    # Paso de ms a s
sig3 = data[:, 1]/1000       # Paso de mV a V

# Grafico los datos
plt.figure()
plt.plot(tiempo3, sig3, '.-')
plt.xlabel('Tiempo (s)')
plt.ylabel('Señal (V)')
plt.title('Datos generados en AD2 y leidos Arduino.')
plt.grid()
plt.show()


#%% Levantar y graficar seno generado en AD2 y leido en el mismo osciloscopio del AD2

# Defino el nombre del archivo que tiene los datos que voy a visualizar
nombre_archivo_ad2 = 'seno_osciloscopio_AD2.csv'

# Cargo los datos. Es posible que sea necesario eliminar la informacion que esta comentada en los datos al principio. Tambien, si hay un salto de linea al final de todo, eliminarlo.
data = pd.read_csv(nombre_archivo_ad2, sep = ',') # Por ahora se está usando tipo caja negra

tiempo4 = np.array(data['Time (s)'])
sig4 = np.array(data['Channel 1 (V)'])

# Grafico los datos
plt.figure()
plt.plot(tiempo4, sig4, '.-')
plt.xlabel('Tiempo (s)')
plt.ylabel('Señal (V)')
plt.title('Datos generados en AD2 y medidos en el mismo AD2.')
plt.grid()
plt.show()


#%% Comparacion seno generado en AD2: osciloscopio Arduino vs osciloscopio AD2

# Primero los tengo que alinear para que sean comparables
# TODO lo hice medio a lo chancho, harcodeado, no se si vale la pena hacerlo bien, es solo demostrativo

# La sig3 pasa cerca de 1.6V en la muestra 26
sig3_alin = sig3[26:]
tiempo3_alin = tiempo3[26:]
tiempo3_alin = tiempo3_alin - np.min(tiempo3_alin)

# La sig3 pasa cerca de 1.6V en la muestra 180
sig_4_alin = sig4[180:]
tiempo4_alin = tiempo4[180:]
tiempo4_alin = tiempo4_alin - np.min(tiempo4_alin)

# Grafico los datos
plt.figure()
plt.plot(tiempo3_alin, sig3_alin, '.-')
plt.plot(tiempo4_alin, sig_4_alin, '.-')
plt.legend(['Osc. Arduino', 'Osc. AD2'])
plt.xlabel('Tiempo (s)')
plt.ylabel('Señal (V)')
plt.title('Datos generados en AD2 y medidos en el mismo AD2.')
plt.grid()
plt.show()


#%% Comparacion seno generado en Arduino: comunicacion serie directa vs osciloscopio AD2

# Primero los tengo que alinear para que sean comparables
# TODO lo hice medio a lo chancho, harcodeado, no se si vale la pena hacerlo bien, es solo demostrativo

# La sig3 pasa cerca de 1.6V en la muestra 26
sig1_alin = sig1[86:374]
tiempo1_alin = tiempo1[86:374]
tiempo1_alin = tiempo1_alin - np.min(tiempo1_alin)

# La sig3 pasa cerca de 1.6V en la muestra 180
sig_2_alin = sig2[213:]
tiempo2_alin = tiempo2[213:]
tiempo2_alin = tiempo2_alin - np.min(tiempo2_alin)

# Grafico los datos
plt.figure()
plt.plot(tiempo1_alin, sig1_alin, '.-')
plt.plot(tiempo2_alin, sig_2_alin, '.-')
plt.legend(['Com. serie directa', 'Osc. AD2'])
plt.xlabel('Tiempo (s)')
plt.ylabel('Señal (V)')
plt.title('Datos generados en Arduino: comunicados directo por com. serie vs medidos en el AD2.')
plt.grid()
plt.show()
