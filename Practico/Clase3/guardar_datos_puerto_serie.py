# -*- coding: utf-8 -*-
"""
Created on Wed Nov  8 17:51:41 2023

@author: Ing. Biologica
"""

import serial
import re

# Definir el archivo en el que voy a guardar los datos
nombre_archivo_salida = "datos_Arduino.txt"

# Creo el archivo si no existe, y lo limpio si tiene algo
with open(nombre_archivo_salida, "w") as archivo:
    archivo.truncate(0)

# Configura el puerto serie
port = serial.Serial('COM4', baudrate=9600)  # Reemplaza 'COM4' con el nombre de tu puerto serie y la velocidad de baudios que necesites

try:
    while True:
        # Lee los datos del puerto serie
        data = port.readline().decode('utf-8', errors='ignore')  # Utiliza 'errors='ignore'' para ignorar caracteres no válidos

        # Utiliza una expresión regular para buscar un número con un punto decimal,
        # seguido de una coma y otro número
        match = re.search(r'(\d+(\.\d+)?),(\d+)', data)

        # Si se encontró el patrón de número con punto decimal, coma y número
        if match:
            number_with_decimal = match.group(1)
            number_after_comma = match.group(3)
            result_str = f"{number_with_decimal},{number_after_comma}"

            with open(nombre_archivo_salida, 'a', encoding='utf-8') as file:
                file.write(result_str + '\n')  # Agrega un salto de línea

except KeyboardInterrupt:
    # Maneja una interrupción de teclado (Ctrl+C) para cerrar el puerto serie de manera segura
    port.close()

    

