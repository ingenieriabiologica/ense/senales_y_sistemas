#define PI 3.1415926535897932384626433832795
int sinValue = 0; // Variable que voy a usar para guardar el valor actual de la funcion seno
double fc = 2; // Frecuencia del seno en Hz
int R = 12;
int outputValue;
#define resolution pow(2,R) // Como mi resolucion es 12 bits, puedo representar 2^12 - 1 numeros

void setup() {
  Serial.begin(9600);
  analogWriteResolution(R); // la resolucion es 12 porque estamos trabajando con Arduino Due
}

void loop() {
  double t = millis();

  sinValue = (resolution/2)*(sin(2*PI*fc*1e-3*t)+1);
  outputValue = map(sinValue, 0, resolution-1, 0, 3300); // Lo pasamos a mV para sacarlo por el puerto serie

  analogWrite(DAC0, sinValue);   // Saco el seno por el DAC. Tiene que ser un valor entre 0 y 2^12 (12 es la resolucion). El conversor se encarga de pasarlo a voltaje

  Serial.print(t);
  Serial.print(',');
  Serial.println(outputValue);
  delay(10);
}
