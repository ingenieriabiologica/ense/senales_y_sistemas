#define FS 50.0 // Frecuencia de muestreo que va a usar Arduino
#define R 12 // Resolución del Arduino Due en bits

////////////////////////////////////////////////////////////////////////////////////////////////

#define T_delay (1000/FS) //ms
#define resolution pow(2,R) // Como mi resolucion es 12 bits, puedo representar 2^12 - 1 numeros

// to the pins used:
const int analogInPin = A0; // Analog input pin attached to the circuit output, measured as the capacitor voltage
int t = 0;
unsigned long b, a = 0;
int sensorValue1 = 0;        
int outputValue1 = 0;        

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
  analogReadResolution(R);
}

void loop() {
  a = millis();
  t = t + abs(a-b);
  b = a;

  sensorValue1 = analogRead(analogInPin);  // Valor leido por el puerto analogico. Esta en el rango 0- (resolution-1)
       
// Covierto a mV la salida. El rango de salida en mV es: 0-3300 mV. El map simplemente hace una regla de 3 entre este rango y 0-resolution.
  outputValue1 = map(sensorValue1, 0, resolution-1, 0, 3300);  
  
  // Imprimir los datos en el monitor serie:
  Serial.print(t);
  Serial.print(',');
  Serial.println(outputValue1);
  
  // Espero 1/Fs para tomar la siguiente muestra (esto define la frecuencia de muestreo)
  while ( millis()<(a + T_delay) ){
  }               
}
