# -*- coding: utf-8 -*-
"""
Created on Wed May 24 14:15:50 2023

@author: Ing. Biologica
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

wmax = 3*np.pi
h_w = 0.01
w = np.arange(-wmax, wmax, h_w)

#%% Ejemplo 5.1

a = 0.6
X1 = 1/(1-a*np.exp(-1j*w))

plt.figure()
plt.plot(w, np.abs(X1))
plt.xlabel('w')
plt.ylabel('$|H(e^{jw})|$')
plt.title('Ejemplo 5.1, modulo')

plt.figure()
plt.plot(w, np.angle(X1))
plt.xlabel('w')
plt.ylabel('Fase $H(e^{jw})$')
plt.title('Ejemplo 5.1, fase')

#%% Ejemplo 5.2

X2 = (1-a**2)/(1-2*a*np.cos(w)+a**2)

plt.figure()
plt.plot(w, np.abs(X2))
plt.xlabel('w')
plt.ylabel('$|H(e^{jw})|$')
plt.title('Ejemplo 5.2, modulo')

plt.figure()
plt.plot(w, np.angle(X2))
plt.xlabel('w')
plt.ylabel('Fase $H(e^{jw})$')
plt.title('Ejemplo 5.2, fase')

#%% Generacion de se;al
h = 0.001
t = np.arange(0, 5, h)
s1 = np.sin(2*np.pi*1*t)
s2 = 0.25*np.sin(2*np.pi*3*t)
s3 = 2*np.sin(2*np.pi*7*t)
s4 = 0.5*np.sin(2*np.pi*50*t)

r = 0.3*np.random.randn(len(t))

s = s1 + s2 + s3 + s4 + r # Se puede pensar como señal de clima

S = np.fft.fft(s)/len(s)   # FFT del seno, ventana rectangular, tf = 5
freq = np.fft.fftfreq(len(t), d = h) 

plt.figure()
plt.plot(freq, np.abs(S))

plt.figure()
plt.plot(t, s)
df = pd.DataFrame()
df['t'] = t
df['sig'] = s
df.to_csv('datos.csv', sep=',')













