//https://forum.arduino.cc/index.php?topic=154965.0

#define PI 3.1415926535897932384626433832795
int value=0;
int sinValue = 0;
double fc = 2; //Hz
void setup(){
  analogWriteResolution(12);
  Serial.begin(9600);
}

void loop(){
  double t = millis();
  analogWrite(DAC1, value );  //Rampa
   
  sinValue = (4096/2)*(sin(2*PI*fc*1e-3*t)+1);
  analogWrite(DAC0, sinValue );   //Seno
  
  int sensorValue = analogRead(A0);
  int sensorValue1 = analogRead(A1);
  Serial.print(sensorValue);
  Serial.print(',');
  Serial.println(sensorValue1);
  
  // Aumento el valor de rampa en +1
  value++;
  delay(10);
  if(value>4094)
    value=0;
}
