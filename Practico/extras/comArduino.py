# -*- coding: utf-8 -*-
"""
Created on Thu Mar 23 18:08:49 2023

@author: Ing. Biologica
"""
import serial
import time
import numpy as np
import matplotlib.pyplot as plt

tiempo_captura = 20     # Tiempo que se quiere registrar del puerto serie, expresado en segundos

datos = []

nombre_puerto = 'COM3'
velocidad_puerto = 9600

puerto = serial.Serial(nombre_puerto, velocidad_puerto, 8, 'N', 1, timeout=1)

#TODO: Añadir un tiempo de espera, para que no llegue información basura que altere el programa y pueda dar errores

tiempo_inicio = time.time()
tiempo_finalizacion = tiempo_inicio + tiempo_captura  # 20 segundos

while time.time() < tiempo_finalizacion:
    if puerto.in_waiting > 0:
        mensaje = puerto.readline().decode('utf-8').rstrip()
        datos.append(mensaje)
        print(mensaje)
        
puerto.close()

#%%
def extraer_vectores(datos, ini, sep, cols):
    datos = datos[ini:] 
    vectores = []
    for i in range(len(datos)):
        contF = 0
        contI = 0
        carac = 0
        aux = []
        for j in range(cols-1):
    
            while (datos[i][carac] != sep):
                contF += 1
                carac += 1
            aux.append(datos[i][contI:contF])
            contI = contF + 1
            contF = contI
            carac +=1
        
        aux.append(datos[i][contI:])
    
        vectores.append(aux)
    return vectores


def string_to_int(arr):
    salida = []
    for i in range(len(arr)):
        salida.append(int(arr[i]))
    salida = np.array(salida)    
    return salida

# TODO RECONOCER AUTOMATICAMENTE LA CABECERA, GUARDAR EL NOMBRE Y FIJAR DONDE CORTAR AUTOMATICAMENTE 
ini = 1     # Dejar afuera el nombre de las columnas, tienen que quedar solo numeros
# En caso que se quiera guardar los nombres de las columnas, se pueden guardar en una variable

sep = ' '
cols = 2

vectores = extraer_vectores(datos, ini, sep, cols)

vectores = np.array(vectores) # Aca tengo todos los vectores en formato string

vector0 = vectores[:, 0] # Extraccion de vector de la columna 0

salida0 = string_to_int(vector0) # Pasaje de float a int

vector1 = vectores[:, 1] # Extraccion de vector de la columna 0

salida1 = string_to_int(vector1) # Pasaje de float a int

#%%
plt.figure()
plt.plot(salida0)
plt.plot(salida1)




