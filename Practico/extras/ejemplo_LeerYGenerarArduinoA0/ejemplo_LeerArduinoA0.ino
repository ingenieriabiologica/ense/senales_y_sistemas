//Reads an analog input pin coming from an arbitrary waveform generator, the input is processed and written as an analog output

const int analogInPin = A0;    // Analog input pin 
int sensorValue1 = 0;        
int outputValue1 = 0;        
int sensorValue2 = 0;
int outputValue2 = 0;
int bits = 12;
int output = DAC1;
int count = 0;


void setup() {
	  // initialize serial communications at 9600 bps:
	  Serial.begin(9600); 
}

void loop() {

	  // set analog read resolution at 12 bits
	  analogReadResolution(bits);

	  // read the analog in value:
	  sensorValue1 = analogRead(analogInPin);     
       
	  // map it to the range of the analog out:
	  outputValue1 = map(sensorValue1, 0, pow(2, bits)-1, 0, 5000);  

	  // make digital processing of the input signal by changing its amplitude
	  float v = outputValue1;
	  float off = 3000; //offset is set at 3 Volts (considering the input signal's offset)
	  float vo = 2*(v-off)+off; 
	  outputValue2 = round(vo);, 
	  sensorValue2 = map(outputValue2, 0,5000, 0,  pow(2, bits)-1);  
	  analogWrite(output, sensorValue2);

	  // print the results to the serial monitor:
	 
	  if (count < 1000){          
	     count++;          
	  Serial.print(outputValue1);
	  Serial.print(" ");
	  Serial.println(outputValue2);
	  }
	   

	  // wait 1 milliseconds before the next loop
	  // for the analog-to-digital converter to settle
	  // after the last reading:
	  delay(1);
	  
	 // delayMicroseconds(1000); 

            
}
