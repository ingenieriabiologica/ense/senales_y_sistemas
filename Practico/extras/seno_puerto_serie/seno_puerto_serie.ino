#define PI 3.1415926535897932384626433832795
int sinValue = 0; // Variable que voy a usar para guardar el valor actual de la funcion seno
double fc = 2; // Frecuencia del seno en Hz

void setup() {
  Serial.begin(9600);
}

void loop() {
  double t = millis();
  sinValue = (4096/2)*(sin(2*PI*fc*1e-3*t)+1);
  Serial.print(t);
  Serial.print(',');
  Serial.println(sinValue);
  delay(10);
}
