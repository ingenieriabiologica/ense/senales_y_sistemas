
///////////////////////////////////////////////////////////////////////////////////////////////
//                             Sampling frequency (defined by user)                          //
///////////////////////////////////////////////////////////////////////////////////////////////

#define FS 50.0 //Hz
#define R 10 // Si usan Arduino 1 R=10, si usan Due R=12 (y descomentan analogReadResolution)

////////////////////////////////////////////////////////////////////////////////////////////////

#define T_delay (1000/FS) //ms
#define resolution pow(2,R)

// to the pins used:
const int analogInPin = A0; // Analog input pin attached to the circuit output, measured as the capacitor voltage
const int analogInPin2 = A1; //Analog input pin attached to the circuit input voltage
int t = 0;
unsigned long b, a = 0;
int sensorValue1 = 0;        
int outputValue1 = 0;        
int sensorValue2 = 0;
int outputValue2 = 0;

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
  //analogReadResolution(R);

  Serial.print("V_A0(mV)");
  Serial.print(" ");
  Serial.println("V_A1(mV)");
}

void loop() {
  a = millis();
  t = t + abs(a-b);
  b = a;
  // read the analog in value (circuit output):
  sensorValue1 = analogRead(analogInPin);  
  // read the analog in value (circuit input):
  sensorValue2 = analogRead(analogInPin2);    
       
  // map it to the range of the analog out:
  outputValue1 = map(sensorValue1, 0, resolution-1, 0, 5000);  
  // map it to the range of the analog out:
  outputValue2 = map(sensorValue2, 0, resolution-1, 0, 5000); 
  
  // print the results to the serial monitor:
  //Serial.print("sensor = " );
  Serial.print(outputValue1);
  Serial.print(" ");
  Serial.println(outputValue2);
  
  // wait 20 milliseconds before the next loop
  // for the analog-to-digital converter to settle
  // after the last reading:
  while ( millis()<(a + T_delay) ){
    
  }               
}
