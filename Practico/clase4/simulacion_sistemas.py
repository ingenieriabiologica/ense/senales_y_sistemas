# -*- coding: utf-8 -*-
"""
Created on Wed Mar 29 21:40:47 2023

@author: Ing. Biologica
"""

import numpy as np
import scipy.signal as sg
import matplotlib.pyplot as plt

h = 0.01
f = 1
cant_ciclos = 3
t = np.arange(0, cant_ciclos*(1/f), h)

ent = np.sin(2*np.pi*f*t)

plt.figure()
plt.plot(t, ent)
plt.title('Señal de entrada de los sistemas')
plt.ylabel('Amplitud')
plt.xlabel('Tiempo (s)')

#%% Generar y levantar datos
data = np.zeros((len(t), 2))
data[:, 0] = t
data[:, 1] = ent
np.savetxt('datos.csv', data, delimiter=',')

datos_archivo = np.loadtxt('datos.csv', delimiter=',')

t = datos_archivo[:, 0]
ent = datos_archivo[:, 1]

#%% Identidad
a = [1] # Coeficientes de los y
b = [1] # Coeficientes de los x

y_id = sg.lfilter(b, a, ent) #Primero los coef de x, luego los de y y por último la entrada.

plt.figure()
plt.plot(t, ent)
plt.plot(t, y_id, '.')
plt.legend(['Entrada', 'Salida'])
plt.title('Sistema identidad')
plt.ylabel('Amplitud')
plt.xlabel('Tiempo (s)')


# Con convolucion
sal_conv_id = np.convolve(ent, b, 'full')

plt.figure()
plt.plot(t, sal_conv_id)



#%% Proporcional
k = 2
a = [1]
b = [k]

y_prop = sg.lfilter(b, a, ent)

plt.figure()
plt.plot(t, ent)
plt.plot(t, y_prop)
plt.legend(['Entrada', 'Salida'])
plt.title('Sistema proporcional')
plt.ylabel('Amplitud')
plt.xlabel('Tiempo (s)')

# Con convolucion
sal_conv_prop = np.convolve(ent, b, 'full')

plt.figure()
plt.plot(t, sal_conv_prop)

#%% Retardo
k = 50
a = [1]

b = np.zeros(k)
b[k-1] = 1

y_ret = sg.lfilter(b, a, ent)

plt.figure()
plt.plot(t, ent)

plt.plot(t, y_ret)
plt.legend(['Entrada', 'Salida'])
plt.title('Sistema retardo')
plt.ylabel('Amplitud')
plt.xlabel('Tiempo (s)')

# Con convolucion
sal_conv_ret = np.convolve(ent, b, 'full')  # con same quedan del mismo largo

plt.figure()
plt.plot(sal_conv_ret)

# Notar que el lfilter me recorto la senal para que me quedara del mismo tamano que la de entrada

#%% Anadir ruido

n = (np.random.rand(len(t)) * 2 - 1)/5
entR = n + ent

#%% Generar y levantar datos
data = np.zeros((len(t), 2))
data[:, 0] = t
data[:, 1] = entR
np.savetxt('datos_ruidosos.csv', data, delimiter=',')

datos_archivo = np.loadtxt('datos_ruidosos.csv', delimiter=',')

t = datos_archivo[:, 0]
entR = datos_archivo[:, 1]

#%%
L = 20

coef = np.ones(L)
a = [L]

# Implemento una media móvil causal de largo L
salida = sg.lfilter(coef, a, entR)

plt.figure()
plt.plot(t, salida)
plt.plot(t, entR)
plt.legend(['Filtrada', 'No filtrada'])
plt.title('Aplicacion de media movil con L = ' + str(L))
plt.ylabel('Amplitud')
plt.xlabel('Tiempo (s)')

# Con convolucion
sal_conv_med = np.convolve(entR, coef/L, 'full')

plt.figure()
plt.plot(sal_conv_med)
