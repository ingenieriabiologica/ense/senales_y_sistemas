# -*- coding: utf-8 -*-
"""
Created on Thu Apr 25 12:56:07 2019

@author: lucia
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sg

#%% Genero un vector en tiempo discreto (paso = 1) que voy a usar como variable independiente
n=np.arange(0,20,1.0)
L=len(n)

# Funcion de entrada al sistema. Una recta de pendiente = 2 y que pasa por el origen.
x = 2*n

#%% Voy a implementar los sistemas derivador y acumulador con las ecuaciones de transformacion

# Sistema derivador
xd=np.diff(x)
xd=np.insert(xd,0,0)

# Sistema acumulador
xa=np.cumsum(x)

# Aplicacion de ambos sistemas en cascada. 
xad=np.diff(xa)
xad=np.insert(xad,0,0)

#%% Voy a implementar los sistemas usando sus ecuaciones en diferencias (mediante lfilter)
# Coeficientes del sistema derivador
a = [1]
b = [1, -1]
der = sg.lfilter(b, a, x) # Sistema que deriva implementado usando ecuacion en diferencias (mediante lfilter)

# Coeficientes del sistema integrador
a = [1, -1]
b = [1]
suma = sg.lfilter(b, a, x) # Sistema que acumula implementado usando ecuacion en diferencias (mediante lfilter)

#%% Implementacion de los sistemas "a mano"

# Derivador
salida_der = [0]    # Hago que el valor asociado a n=0 sea 0. No puedo calcular la salida aca porque no hay un valor anterior
salida_der_array = np.zeros(len(x))     # Es redundante a lo anterior. Se puede hacer de las dos formas.
for i in range(1, len(x)):
    salida_der.append(x[i] - x[i-1])
    salida_der_array[i] = x[i] - x[i-1]

# Acumulador
salida_acu = []
salida_acu_array = np.zeros(len(x))
for i in range(len(n)):
    aux = 0
    aux_array = 0
    for j in range(i):
        aux += x[j] 
        aux_array += x[j]
    aux += x[i]
    aux_array += x[i]
    salida_acu.append(aux)
    salida_acu_array[i] = aux_array
# Si se grafican se puede ver que da la misma salida.

#%% visualizacion de senal de entrada 
plt.figure(1)
plt.stem(n,x,'b', label='x')
plt.legend()
plt.xlim([n[0], n[len(n)-1]])
plt.ylim([0,50])
plt.title('original')

#%% visualizacion de salida del sistema derivador

plt.figure(2)
plt.stem(n,xd,'b',label='xd')
plt.legend()
plt.xlim([n[0], n[len(n)-1]])
plt.ylim([0,10])
plt.title('derivada')

#%% visualizacion de salida del sistema acumulador

plt.figure(3)
plt.stem(n,xa,'b', label='xa')
plt.legend()
plt.xlim([n[0], n[len(n)-1]])
plt.ylim([0,450])
plt.title('integrada')

#%% visualizacion de senal de salida al aplicar en cascada ambos sistemas

plt.figure(4)
plt.stem(n,xad,'b', label='xad')
plt.legend()
plt.xlim([n[0], n[len(n)-1]])
plt.ylim([0,50])
plt.title('integrada-derivada')

#%%visualizacion de senal de entrada, y salida de los sistemas en cascada. Deberian ser iguales.

plt.figure(5)
plt.stem(n,x,'r', markerfmt='ro', label='x')
plt.stem(n+0.1,xad,'b',markerfmt='bo', label='xad')
plt.legend()
plt.xlim([n[0], n[len(n)-1]])
plt.ylim([0,50])
plt.title('comparación: entrada vs salida sistemas en cascada')

#%% visualizacion de las salidas usando ecuaciones en diferencias y ec de transformacion

plt.figure(6)
plt.plot(n, xd, 'o')
plt.plot(n, der, '.')
plt.legend(['ec transformacion', 'ecuacion en diferencias (lfilter)'])
plt.title('Derivada de la funcion $x = 2n$')

plt.figure(7)
plt.plot(n, xa, 'o')
plt.plot(n, suma, '.')
plt.legend(['ec transformacion', 'ecuacion en diferencias (lfilter)'])
plt.title('Integral de la funcion $x = 2n$')


plt.show()