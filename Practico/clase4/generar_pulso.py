# -*- coding: utf-8 -*-
"""
Created on Wed Dec 27 16:16:31 2023

@author: Ing. Biologica
"""

import numpy as np
import matplotlib.pyplot as plt

h_t = 0.1
tf = 10
t = np.arange(-tf, tf, h_t)

a = np.zeros(2*int(tf/h_t))

ancho = int(2/h_t)  # 2s en muestras
mitad = int(len(t)/2)
a[mitad-ancho : mitad+ancho] = 1

np.save('pulso.npy', a)

plt.figure()
plt.plot(t, a)

