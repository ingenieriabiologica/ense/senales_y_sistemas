# -*- coding: utf-8 -*-
"""
Created on Mon Apr 17 17:54:12 2023

@author: Ing. Biologica
"""

import numpy as np
import matplotlib.pyplot as plt
import cmath
import math

N = 30
k = np.arange(-N, N)

T1 = 1
T = 4

# TODO: revisar esta cuenta, el módulo da diferente que lo análitico
# TODO: además hay que escribirlo más paramétrico (en función de T y T1)
# TODO: el (-1) se lo podés pasar al 1j y el código queda más limpio
# TODO: agregar el epsilon de la máquina
ak = 0.5*np.exp(1j*k*np.pi*0.5*(-1)) * np.sin(0.5*np.pi*k) * (1/(k*np.pi))
ak[N] = 0

ak_mod = np.abs(ak)

# 
# ak_fase = np.angle(np.sin(0.5*np.pi*k) * (1/(k*np.pi)))
ak_fase = np.angle(ak)
# ak_fase = np.angle(np.exp(1j*k*np.pi*0.5*(-1)))
# TODO: hasta no ver el módulo bien, no es fácil ver que está mal, pero el truco de *2 /2 ayuda a que funcione el unwrapp
# TODO: el -np.pi es una prueba trucha nomás para ver si lo hacía andar.
fase_unwrapped = np.unwrap(2*ak_fase)/2 - np.pi
plt.figure()
plt.stem(ak_fase)

plt.figure()
plt.stem(k, fase_unwrapped)

plt.figure()
plt.stem(k, ak_mod)

plt.show() # TODO: faltaba el plt.show()

# atan2 o angle2





