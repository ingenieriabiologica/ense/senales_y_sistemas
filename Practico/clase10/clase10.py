# -*- coding: utf-8 -*-
"""
Created on Tue May 23 13:10:50 2023

@author: Ing. Biologica
"""

import numpy as np
import matplotlib.pyplot as plt

wmax = 50  # w max para construir el vector w
paso_w = 0.1    # Paso del vector de frecuencia

w = np.arange(-wmax, wmax, paso_w)  # Creo un vector simetrico w

#%% Ejemplo 4.2
a = 0.6

H_4_2 = (2*a)/(a**2 + w**2)     # Como es real no tengo que hacer modulo y fase

plt.figure()
plt.plot(w, H_4_2)
plt.xlabel('$\omega$')
plt.ylabel('$|H(j\omega)|$')
plt.title('Ejercicio 4.2')
plt.grid()

'''Ejemplo 4.4'''
#%% La senal temporal, es un pulso que esta centrado en cero, y tiene ancho total 2T1
T1 = 1  # defino el ancho

# Me creo un vector de tiempo para graficar mi senal temporal
tmax = 5
h_t = 0.1
t = np.arange(-tmax, tmax, h_t)

x_T1 = np.zeros(len(t))
t_0_ind = np.argmin(np.abs(t))  # busco el indice del eje de tiempo donde esta el 0s
x_T1[t_0_ind - int(T1/h_t): t_0_ind + int(T1/h_t)] = 1  # int(T1/h_t) corresponde a la cantidad de muestras que valen 1 a la izquierda 0 derecha del 0s 

plt.figure()
plt.stem(t, x_T1)
plt.xlabel('t(s)')
plt.ylabel('Amplitud')
plt.title('Senal pulso, con $T_{1} = 1$')
plt.grid()

# Expresion de la transformada
H = (2 * np.sin(w*T1))/w    # Como es real, no tengo que hacer modulo y fase

plt.figure()
plt.plot(w, H)
plt.xlabel('$\omega$')
plt.ylabel('$|H(j\omega)|$')
plt.title('Ejercicio 4.4, con $T_{1} = 1s$')
plt.grid()

#%%
# Ahora hago lo mismo pero con un pulso del doble de ancho
T1_2 = 2

x_T1_2 = np.zeros(len(t))
t_0_ind = np.argmin(np.abs(t))  # busco el indice del eje de tiempo donde esta el 0s
x_T1_2[t_0_ind - int(T1_2/h_t): t_0_ind + int(T1_2/h_t)] = 1 

plt.figure()
plt.stem(t, x_T1_2)
plt.xlabel('t(s)')
plt.ylabel('Amplitud')
plt.title('Senal pulso, con $T_{1} = 1$')
plt.grid()

# Expresion de la transformada
H_2 = (2 * np.sin(w*T1_2))/w    # Como es real, no tengo que hacer modulo y fase

# Comparacion de las trasnformadas de los dos pulsos con distinto ancho T1
plt.figure()
plt.plot(w, H, color = 'red')
plt.plot(w, H_2, color = 'blue')
plt.xlabel('$\omega$')
plt.ylabel('$|H(j\omega)|$')
plt.title('Ejercicio 4.4, considerando dos pulsos de distintos anchos')
plt.legend(['$T_{1} = 1s$ ', '$T_{1} = 2s$'])    # son pulsos centrados en 0. Esos valores corresponden al ancho total
plt.grid()

'''Reconstruccion temporal a partir de X(jw)'''
#%% 
tmax = 5
h_t = 0.1

t = np.arange(-tmax, tmax, h_t)

# El 1/2pi de la ec de sintesis se lo multiplico al final
x = []
for i in range(len(t)):
    x.append((1/(2*np.pi))*np.sum(H*np.exp(1j*w*t[i]))*h_t)

plt.figure()
plt.plot(t, x, '.-')
plt.xlabel('t(s)')
plt.ylabel('Amplitud')
plt.title('Reconstruccion con la ecuacion de sintesis')
plt.grid()

plt.show()































