# -*- coding: utf-8 -*-
"""
Created on Fri Dec  8 16:16:19 2023

@author: Ing. Biologica
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.io import wavfile
from scipy.fft import fft, fftshift, ifftshift
import sounddevice as sd

reproducir_audio = True
#%%
# Leer señal de audio
fs, s = wavfile.read('voice2.wav')
L = len(s)
Ts = 1 / fs
t = np.arange(0, L) * Ts
t_max = L * Ts

# Graficar señal original en el tiempo
plt.figure()
plt.plot(t, s)
plt.title('Señal original en el tiempo')
plt.xlabel('Tiempo (s)')
plt.ylabel('Amplitud (V)')
print(f'Duración: {t_max:.2g} segs.')

#%%
# Reproducción de audio
if reproducir_audio:
    sd.play(s, fs)
    sd.wait()

#%%
# Cálculo de la transformada de Fourier
f = fs / 2 * np.linspace(-1, 1, L)
S = fft(s)
t_mod = fftshift(np.abs(S))

plt.figure()
plt.plot(f, t_mod / max(t_mod))
plt.title('Espectro de la señal original')
plt.ylabel('Amplitud')
plt.xlabel('Frecuencia (Hz)')
df = f[1] - f[0]

plt.show()

#%%
# Selección de un rango del espectro

# Frecuencia de corte de la ventana en frecuencia que me voy a tomar
f_c = 2000

ind = np.argmin(np.abs(f - f_c))
ind2 = np.argmin(np.abs(f + f_c))
t_mod_int = t_mod[ind:]
L_int = len(t_mod_int)
E_int = np.sum(t_mod_int ** 2) * df
E = np.sum(t_mod ** 2) * df
E_r = 2 * E_int / E * 100
print(f'Energía descartada: {E_r:.2f}%')

plt.figure()
plt.plot(f, t_mod / max(t_mod))
plt.axvline(x=f_c, color='r', linestyle='-', linewidth=2)
plt.axvline(x=-f_c, color='r', linestyle='-', linewidth=2)
plt.title(f'Filtro pasabajos con frecuencia de corte $f_c ={f_c}$')
plt.ylabel('Amplitud')
plt.xlabel('Frecuencia (Hz)')

#%%
# Filtro pasabajos ideal
mascara = np.zeros(L)
mascara[ind2:ind] = 1

plt.figure()
plt.plot(f, t_mod / max(t_mod), '-b')
plt.plot(f, mascara, '-r', linewidth=2)
plt.axis([min(f), max(f), 0, 1.05])
plt.title(f'Filtro pasabajos con frecuencia de corte $f_c ={f_c}$')
plt.ylabel('Amplitud')
plt.xlabel('Frecuencia (Hz)')

Sr = ifftshift(fftshift(S) * mascara)
sr = np.real(np.fft.ifft(Sr))

plt.figure()
Sr_mod_shift = np.abs(fftshift(Sr))
plt.plot(f, Sr_mod_shift / max(Sr_mod_shift))
plt.title(f'Espectro luego de aplicar filtro pasabajos con $f_c ={f_c}$')
plt.ylabel('Amplitud')
plt.xlabel('Frecuencia (Hz)')

# Normalizar la señal filtrada
sr_normalized = sr / np.max(np.abs(sr))
#%%
# Reproducción de la señal filtrada
if reproducir_audio:
    sd.play(sr_normalized, fs)
    sd.wait()

plt.figure()
plt.plot(t, sr)
title_str = f'Reconstrucción en el tiempo (descartando: {E_r:.2f}%) con $f_c ={f_c}$'
plt.title(title_str)
plt.xlabel('Tiempo (s)')
plt.ylabel('Amplitud (V)')

plt.show()

#%% Voy a construir una ventana en frecuencia que este centrada en el punto que yo quiera

# Selección de un rango del espectro
# Ahora me voy a tomar dos frecuencias de cortes para determinar la ventana rectangular

# Frecuencia de corte superior
f_c_sup = 2000

# Frecuencia de corte inferior
f_c_inf = 600

ind = np.argmin(np.abs(f - f_c_inf))
ind2 = np.argmin(np.abs(f - f_c_sup))
ind3 = np.argmin(np.abs(f + f_c_inf))
ind4 = np.argmin(np.abs(f + f_c_sup))

# Filtro pasabajos ideal
mascara = np.zeros(L)
mascara[ind4:ind3] = 1
mascara[ind:ind2] = 1

plt.figure()
plt.plot(f, t_mod / max(t_mod), '-b')
plt.plot(f, mascara, '-r', linewidth=2)
plt.axis([min(f), max(f), 0, 1.05])
plt.title(f'Filtros pasabanda a aplicar, con $f_i = {f_c_inf}$ y $f_s = {f_c_sup}$')
plt.ylabel('Amplitud')
plt.xlabel('Frecuencia (Hz)')

#%%
Sr = ifftshift(fftshift(S) * mascara)
sr = np.real(np.fft.ifft(Sr))

plt.figure()
Sr_mod_shift = np.abs(fftshift(Sr))
plt.plot(f, Sr_mod_shift/ max(Sr_mod_shift))
plt.title('Espectro luego de aplicar filtro pasabandas')
plt.ylabel('Amplitud')
plt.xlabel('Frecuencia (Hz)')


# Normalizar la señal filtrada
sr_normalized = sr / np.max(np.abs(sr))

plt.figure()
plt.plot(t, sr)
title_str = 'Reconstrucción en el tiempo luego de aplicar el filtro pasabandas'
# title_str = f'Reconstrucción en el tiempo luego de aplicar el filtro pasabandas (descartando: {E_r:.2f}%)'
# TODO calcular la energía descartada
plt.title(title_str)
plt.xlabel('Tiempo (s)')
plt.ylabel('Amplitud (V)')
# Reproducción de la señal filtrada
if reproducir_audio:
    sd.play(sr_normalized, fs)
    sd.wait()
plt.show()
# TODO hacer el calculo de la energia 
# t_mod_int = t_mod[ind:]
# L_int = len(t_mod_int)
# E_int = np.sum(t_mod_int ** 2) * df
# E = np.sum(t_mod ** 2) * df
# E_r = 2 * E_int / E * 100

#%%
# TODO fijarme en el video que se quiere hacer aca y arreglarlo 
# Aumentar la frecuencia de la señal
f = 10
Se = np.concatenate((S, np.zeros(f * L)))
fse = f * fs
Le = len(Se)
Se_mod = fftshift(np.abs(Se))
fe = fse / 2 * np.linspace(-1, 1, Le)

# plt.figure()
# plt.plot(fe, Se_mod / max(Se_mod))
se = np.fft.ifft(Se)
se = np.real(se)
se_normalized =  se / np.max(np.abs(se))

if 0:
    sd.play(se_normalized, fse)
    sd.wait()
    
# plt.figure()
# plt.plot(np.real(se))


plt.show()
