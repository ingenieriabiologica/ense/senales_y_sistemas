import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

def calcular_butter(frecuencia_corte, orden, fs):
    # Especificaciones del filtro
    
    
    # Calcular la frecuencia de corte normalizada
    frecuencia_corte_normalizada = frecuencia_corte / (0.5 * fs)
    
    # Diseñar el filtro Butterworth
    b, a = signal.butter(orden, frecuencia_corte_normalizada, btype='low', analog=False, output='ba')
    
    return b, a

def generar_suma_mezcla():
    # paso del eje de tiempo
    dt = 0.0001
    t = np.arange(0.0, 6.0, dt)
    # Defino las frecuencias en Hz de los senos que voy a usar
    f1 = 10
    f2 = 30
    f3 = 50
    
    #---------------COMPLETAR CODIGO--------------------------------------------
    # Defino los senos de diferentes frecuencias que voy a usar
    s1 = np.sin(2 * np.pi * f1 * t)
    s2 = np.sin(2 * np.pi * f2 * t)
    s3 = np.sin(2 * np.pi * f3 * t)
    
    # Calculo la suma de los tres senos
    suma_senos = s1+s2+s3
    
    # Defino los pulsos prendidos en diferentes tiempos que voy a usar
    nmax = len(t)
    n1 = np.int(np.floor(len(t) / 3.0))
    n2 = np.int(np.ceil(len(t) * 2.0 / 3))
    p1 = np.concatenate([np.ones(n1), np.zeros(n2)])  # pulso 1
    p2 = np.concatenate([np.zeros(n1), np.ones(n1), np.zeros(nmax-2*n1)])  # pulso 2
    p3 = np.concatenate([np.zeros(n2), np.ones(n1)])  # pulso 3
    
    # Calculo la mezcla de los tres senos (usar los pulsos y las señales)
    mezcla_senos = s1*p1+s2*p2+s3*p3
    
    return suma_senos, mezcla_senos