# -*- coding: utf-8 -*-
"""
Created on Wed Jun  7 15:18:56 2023

@author: Ing. Biologica
"""

import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

fs = 50000
h = 1/fs

#%% Dominio del tiempo
f = 400
t = np.arange(0, 1, h)

s = 2 * np.sin(2*np.pi*f*t) # senal de voz

fp = 5000   # Frecuencia portadora
p = np.sin(2*np.pi*fp*t)    # senal portadora



# ventana = np.hamming(len(t))

# penv = p*ventana

y = s * p # voz + portadora

plt.figure()
plt.plot(t, s)
plt.plot(t, p)
plt.plot(t, y)
plt.legend(['Voz', 'Portadora', 'Voz + portadora'])

#%% Dominio de la frecuencia

freq = np.fft.fftfreq(len(t), d = h)    # Contruccion del eje de frecuencia para todas las senales (el paso es el mismo)

S = np.fft.fft(s)/len(s)   # FFT de la voz
P = np.fft.fft(p)/len(p)   # FFT de la portadora
Y = np.fft.fft(y)/len(y)   # FFT suma

Sabs = np.abs(S)
Pabs = np.abs(P)
Yabs = np.abs(Y)

plt.figure()
plt.stem(freq, Sabs)
plt.title('FFT voz')
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('Amplitud')

plt.figure()
plt.stem(freq, Pabs)
plt.title('FFT portadora')
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('Amplitud')

plt.figure()
plt.stem(freq, Yabs)
plt.title('FFT voz + portadora')
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('Amplitud')

#%% Ahora quiero demodular

# Vuelvo a usar p

d = y * p

D = np.fft.fft(d)/len(d)   # FFT de la senal demodulada
Dabs = np.abs(D)
plt.figure()
plt.stem(freq, Dabs)
plt.title('FFT demodulada')
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('Amplitud')

#%% Ahora aplico un pasabajos para recuperar mi señal
def calcular_butter(frecuencia_corte, orden, fs):
    # Especificaciones del filtro
    
    
    # Calcular la frecuencia de corte normalizada
    
    frecuencia_corte_normalizada = frecuencia_corte / (0.5 * fs)
    
    # Diseñar el filtro Butterworth
    b, a = signal.butter(orden, frecuencia_corte_normalizada, btype='low', analog=False, output='ba')
    
    return b, a

frecuencia_corte = 600  # Hz
orden = 4  # Orden del filtro Butterworth

b, a = calcular_butter(frecuencia_corte, orden, fs)
# Graficar respuesta en frecuencia del filtro 
w, h = signal.freqz(b, a)

plt.figure()
plt.plot(w*(fs/2), (np.abs(h)))
plt.stem([400], [1])
plt.stem([9600], [0.5], linefmt='red')
plt.axis([-500, 10500, -0.1, 1.1])
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('Amplitud')
plt.title('Filtro bueno')

#%%
#TODO pasar a función este bloque
# Aplicar el filtro a la señal
d_filt = signal.lfilter(b, a, d)

D_filt = np.fft.fft(d_filt)/len(d_filt)  # FFT de la senal demodulada
D_filt_abs = np.abs(D_filt)

# Graficar la señal original y la señal filtrada
plt.figure()
plt.stem(freq, D_filt_abs)
plt.title('FFT demodulada y filtrada con filtro bueno')
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('Amplitud')

señal_reconstruida = np.fft.ifft(D_filt)*len(t)

plt.figure()
plt.plot(t, señal_reconstruida.real)
plt.axis([0.5, 0.525, -1, 1])
plt.title('Senal reconstruida con filtro bueno')
plt.xlabel('Tiempo (s)')
plt.ylabel('Amplitud')


#%% reconstruyo con un filtro peor

frecuencia_corte = 300  # Hz
orden = 1  # Orden del filtro Butterworth

b, a = calcular_butter(frecuencia_corte, orden, fs)
# Graficar respuesta en frecuencia del filtro 
w, h = signal.freqz(b, a)

plt.figure()
plt.plot(w*(fs/2), (np.abs(h)))
plt.stem([400], [1])
plt.stem([9600], [0.5], linefmt='red')
plt.axis([-500, 10500, -0.1, 1.1])
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('Amplitud')
plt.title('Filtro malo')


#%%

# Aplicar el filtro a la señal
d_filt = signal.lfilter(b, a, d)

D_filt = np.fft.fft(d_filt)/len(d_filt)  # FFT de la senal demodulada
D_filt_abs = np.abs(D_filt)

# Graficar la señal original y la señal filtrada
plt.figure()
plt.stem(freq, D_filt_abs)
plt.title('FFT demodulada y filtrada, con filtro malo')
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('Amplitud')

señal_reconstruida1 = np.fft.ifft(D_filt)*len(t)

plt.figure()
plt.plot(t, señal_reconstruida1.real)
plt.axis([0.5, 0.525, -1, 1])
plt.title('Senal reconstruida con filtro malo')
plt.xlabel('Tiempo (s)')
plt.ylabel('Amplitud')

plt.figure()
plt.plot(t, señal_reconstruida.real, color = 'green')
plt.plot(t, señal_reconstruida1.real, color = 'red')
plt.axis([0.5, 0.525, -1, 1])
plt.title('Senal reconstruida con filtro bueno y malo')
plt.xlabel('Tiempo (s)')
plt.ylabel('Amplitud')
plt.legend(['Filtro bueno', 'Filtro malo'])

plt.figure()
plt.plot(t, 2*señal_reconstruida.real, color = 'green')
plt.plot(t, 2*señal_reconstruida1.real, color = 'red')
plt.plot(t, s)
plt.axis([0.5, 0.525, -2.5, 2.5])
plt.title('Senal reconstruida con filtro bueno y malo')
plt.xlabel('Tiempo (s)')
plt.ylabel('Amplitud')
plt.legend(['Filtro bueno', 'Filtro malo', 'senal original'])

#TODO calcular retardo de grupo --> luego de aplicar butter. Se puede crear un objeto tipo sistema con LTI, al que le puede pedir el retardo de grupo



