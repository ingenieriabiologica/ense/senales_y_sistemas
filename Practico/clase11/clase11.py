# -*- coding: utf-8 -*-
"""
Created on Sat May 20 16:12:28 2023

@author: Ing. Biologica
"""

import numpy as np
import matplotlib.pyplot as plt

# Creo un seno de 2 Hz, que comience y termine en el mismo punto, para no generar problema en la FFT

f = 2
tf = 5    # Me defino el tiempo final del vector en 5s, el ti = 0 lo dejo fijo en 0s

h = 0.001    # Paso del vector de tiempo
t_comp = np.arange(0, tf, h)

s_comp = np.sin(2*np.pi*f*t_comp)

plt.figure()
plt.plot(t_comp, s_comp)
plt.xlabel('Tiempo (s)')
plt.ylabel('Amplitud')
plt.title('Seno con tf = ' + str(tf))

S_comp = np.fft.fft(s_comp)/len(s_comp)   # FFT del seno, ventana rectangular, tf = 5
freq = np.fft.fftfreq(len(t_comp), d = h)    # Notar que el eje de frecuencia depende solo de t, no de la se;al que estoy transformando

plt.figure()
plt.stem(freq, np.abs(S_comp))
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('Modulo de H(jw)')

# Se puede ver que la FFT arroja el resultado esperado

# TODO Duda: si siempre estoy convolucionando con un senoc, por que aca no afecta en nada? es porque la se;al 
# comienza y termina exactamente en el mismo punto?

'''Ahora voy a cambiar el tiempo final para que me quede el ultimo ciclo incompleto,
es decir que el ti sea distinto a tf'''
#%%
tf = 5.125

t = np.arange(0, tf, h)
s = np.sin(2*np.pi*f*t)     # Ahora el ultimo ciclo del seno quedo cortado

plt.figure()
plt.plot(t, s)
plt.xlabel('Tiempo (s)')
plt.ylabel('Amplitud')
plt.title('Seno con tf = ' + str(tf))

S = np.fft.fft(s)/len(s)   # FFT del seno, ventana rectangular, tf = 5
freq = np.fft.fftfreq(len(t), d = h)    # Mientras mantenga el t puedo seguir usando el mismo eje de frecuencias
#TODO que el eje de frecuencia sea un linspace, y el shift se haga sobre el eje y

plt.figure()
plt.stem(freq, np.abs(S))
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('Modulo de H(jw)')

# Se puede ver claramente la aparicion de ruido sobre las componentes de la frecuencia fundamental del seno

''' Ventanas '''
#%% Tiempo

# Hamming
hamming = np.hamming(len(t))

# Hann
hann = np.hanning(len(t))

# Blackman
blackman = np.blackman(len(t))

plt.figure()
plt.plot(t, hamming)
plt.plot(t, hann)
plt.plot(t, blackman)
plt.xlabel('Tiempo (s)')
plt.ylabel('Amplitud')
plt.legend(['Hamming', 'Hann', 'Blackman'])
plt.title('Ventanas en el tiempo')

#%% Frecuencia

HM = np.fft.fft(hamming)/len(hamming)   # FFT de la ventana de Hamming

HAN = np.fft.fft(hann)/len(hann)   # FFT de la ventana de Hann

BL = np.fft.fft(blackman)/len(blackman)   # FFT de la ventana de Blackman

plt.figure()
plt.semilogy(freq, np.abs(HM),'.-')
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('$log(|H(j\omega)|)$')
plt.title('FFT Hamming en eje logaritmico')
plt.grid()

plt.figure()
plt.semilogy(freq, np.abs(HAN), '.-')
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('$log(|H(j\omega)|)$')
plt.title('FFT Hann en eje logaritmico')
plt.grid()

plt.figure()
plt.semilogy(freq, np.abs(BL), '.-')
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('$log(|H(j\omega)|)$')
plt.title('FFT Blackman en eje logaritmico')
plt.grid()

# Grafico conjunto en ejes logaritmicos
plt.figure()
plt.semilogy(freq, np.abs(HM))
plt.semilogy(freq, np.abs(HAN))
plt.semilogy(freq, np.abs(BL))
plt.legend(['Hamming', 'Hann', 'Blackman'])
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('$log(|H(j\omega)|)$')
plt.title('FFT Hamming, Hann, Blackman en eje logaritmico')
plt.grid()

# Grafico conjunto en ejes normales
plt.figure()
plt.plot(freq, np.abs(HM), '.-')
plt.plot(freq, np.abs(HAN), '.-')
plt.plot(freq, np.abs(BL), '.-')
plt.legend(['Hamming', 'Hann', 'Blackman'])
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('$|H(j\omega)|$')
plt.title('FFT Hamming, Hann, Blackman')
plt.grid()
plt.axis([-2, 2, 0, 0.6])

#%%
rect = np.ones(len(t))
R = np.fft.fft(rect)/len(rect)

plt.figure()
plt.plot(freq, np.abs(R))
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('$log(|H(j\omega)|)$')



'''Ahora aplico las ventanas'''
#%% Tiempo

s_ham = s*hamming
s_hann = s*hann
s_bl = s*blackman

plt.figure()
plt.plot(t, s)
plt.plot(t, s_ham)
plt.plot(t, s_hann)
plt.plot(t, s_bl)
plt.xlabel('Tiempo (s)')
plt.ylabel('Amplitud')
plt.legend(['Rectangular','Hamming', 'Hann', 'Blackman'])
plt.title('Seno enventanado con distintas ventanas')

#TODO Hacer otra grafica con mas periodos para mostrar que el enventanado afecta principalmente los periodos del principio y final
#%% Frecuencia

S_ham = np.fft.fft(s_ham)/len(s_ham)   # FFT de la se;al enventanada con Hamming
S_hann = np.fft.fft(s_hann)/len(s_hann)   # FFT de la se;al enventanada con Hamming
S_bl = np.fft.fft(s_bl)/len(s_bl)   # FFT de la se;al enventanada con Hamming


plt.figure()
plt.stem(freq, np.abs(S))
plt.stem(freq, np.abs(S_ham), linefmt='red', markerfmt='ro')
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('Modulo de H(jw)')
plt.title('FFT del seno env con Hamming (en rojo)')
plt.axis([-5, 5, 0, 0.6])

plt.figure()
plt.stem(freq, np.abs(S))
plt.stem(freq, np.abs(S_hann), linefmt='red', markerfmt='ro')
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('Modulo de H(jw)')
plt.title('FFT del seno env con Hann (en rojo)')
plt.axis([-5, 5, 0, 0.6])

plt.figure()
plt.stem(freq, np.abs(S))
plt.stem(freq, np.abs(S_bl), linefmt='red', markerfmt='ro')
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('Modulo de H(jw)')
plt.title('FFT del seno env con Blackman (en rojo)')
plt.axis([-5, 5, 0, 0.6])























