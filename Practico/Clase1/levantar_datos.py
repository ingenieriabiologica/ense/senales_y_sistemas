# -*- coding: utf-8 -*-
"""
Created on Sun Mar  5 14:37:50 2023

@author: Usuario
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import csv

# Convierte un arreglo o lista 1D de string a float.
def string_to_float(arr):
    salida = []
    for i in range(len(arr)):
        salida.append(float(arr[i]))
    salida = np.array(salida)    
    return salida

'''Defino una variable que indica si quiero usar la libreria pandas para levantar los datos,
o la libreria csv. Si usar_pandas es True se usa pandas, y si no csv
'''
usar_pandas = True

if usar_pandas:
    data = pd.read_csv('seno_prueba.csv', sep = ',')    # Por ahora se está usando tipo caja negra
    
    t = np.array(data['Time (s)'])
    sig = np.array(data['Channel 1 (V)'])

#%% Alternativa con csv

if not usar_pandas:
    datos = []
    with open('seno_prueba.csv', 'r') as archivo:
        lector_csv = csv.reader(archivo)
        for fila in lector_csv:     # Recorro el archivo de datos fila por fila y voy guardando los datos en mi lista
            datos.append(fila)
            
    datos = np.array(datos)
    t = datos[1:, 0]
    sig = datos[1:, 1]
    
    # Se llegan a los mismos vectores que con pandas
    t = string_to_float(t)
    sig = string_to_float(sig)

#%%

plt.figure()
plt.plot(t, sig)
plt.xlabel('Tiempo (s)')
plt.ylabel('Voltaje (V)')
plt.title('Sig experimental en el tiempo completa')
plt.show()

sen_teor = np.sin(2*np.pi*t)    # Agarré el t experimental para que sea comparable

error_abs = np.abs(sen_teor - sig)

plt.figure()
plt.plot(t, error_abs)
plt.xlabel('Tiempo (s)')
plt.ylabel('Voltaje (V)')
plt.title('Error absoluto- senales desfasadas')

fig, ax = plt.subplots(2, sharex='col', sharey='row')
ax[0].plot(t, sig, label = 'Señal experimental')
ax[0].plot(t, sen_teor, label = 'Señal teórica')

ax[0].set(xlabel='Tiempo (s)', ylabel='Voltaje (V)', title='Señales')

ax[1].plot(t, error_abs)
ax[1].set(xlabel='Tiempo (s)', ylabel='Voltaje (V)',
          title='Error absoluto')

# %% Alineando comienzos. Esta hecho para estos datos particulares.

sen_teor = sen_teor[147:]   # Se alinean usando el cruce por cero
sig = sig[166:]
t = t[166:]

sen_teor = sen_teor[:-(len(sen_teor)- len(sig))] # Tengo que cortar esta senal para que quede del mismo largo que las otras dos. 
# Recorto del final porque el principio es lo que tengo alineado, y no lo quiero perder. 

error_abs = np.abs(sen_teor - sig)

plt.figure()
plt.plot(t, error_abs)
plt.xlabel('Tiempo (s)')
plt.ylabel('Voltaje (V)')
plt.title('Error absoluto- senales alineadas')

fig, ax = plt.subplots(2, sharex='col', sharey='row')
ax[0].plot(t, sig, label = 'Señal experimental')
ax[0].plot(t, sen_teor, label = 'Señal teórica')

ax[0].set(xlabel='Tiempo (s)', ylabel='Voltaje (V)',title='Señales')

ax[1].plot(t, error_abs)
ax[1].set(xlabel='Tiempo (s)', ylabel='Voltaje (V)',
          title='Error absoluto luego de alinear')


'''Si restamos dos senos de la misma frecuencia pero 
desfasados se obtiene un seno de la misma frecuencia,
 y la amplitud depende de qué tan desfasados estén los senos.'''
 


prom_teor = np.mean(sen_teor)
prom_exp = np.mean(sig)

print('Promedio sig teórica:' + str(prom_teor) + ', promedio sig experimental: ' + str(prom_exp))

# Responder la siguiente pregunta: el promedio de la teorica da mas cerca de 0?

plt.show()