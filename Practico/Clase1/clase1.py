# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 14:02:30 2023

@author: Usuario
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#%% Ejemplo 2- diapositivas
h = 0.1    # Elegí paso 0.1
N = 1+h    # Porque x = [0,1]
x = np.arange(0, N, h)

sol2 = [1]

for i in range(len(x)-1):
    sol_nuevo = sol2[i] + h*(x[i]*sol2[i]**2 + sol2[i])
    sol2.append(sol_nuevo)
    
plt.figure()
plt.plot(x, sol2, '.-')
plt.title('Ejemplo 2- diapositivas')

#%% Ejemplo 1- diapositivas- exponencial


sol1 = [1]

for i in range(len(x)-1):   # Forma recursiva
    sol1.append(sol1[i] + h*sol1[i])

factor_ref = 10     # Cuanto quiero refinar el eje de la solucion teorica. Cuántos puntos de la teórica meto en cada paso de la aproximada
deltaT = h/factor_ref

t = np.arange(0, N-h+deltaT, deltaT)
sol_teo = np.exp(t)

plt.figure()
plt.plot(x, sol1, '.-')
plt.plot(t, sol_teo)
plt.legend(['Aprox', 'Teorica'])
plt.title('Ejemplo 1- diapositivas- recursión')

sol_imp = [1]
for i in range(1, len(x)):   # Forma general
    sol_imp.append((1+h)**i)

plt.figure()
plt.plot(x, sol_imp, '.-')
plt.plot(t, sol_teo)
plt.legend(['Aprox', 'Teorica'])
plt.title('Ejemplo 1- diapositivas- forma general')

#%% Calculo de errores
ind_coinc = np.arange(0, len(t), factor_ref)

error_abs = []
error_rel = []

for i in range(len(ind_coinc)):
    error_abs.append(np.abs(sol_teo[ind_coinc[i]] - sol1[i]))
    error_rel.append(error_abs[i]/sol_teo[ind_coinc[i]])
    
fig, ax1 = plt.subplots()

ax2 = ax1.twinx()

ax1.plot(x, sol1, 'g-', label='Solucion aprox')
ax1.plot(t, sol_teo, 'b-', label='Solucion teorica')

ax2.plot(x, error_abs, 'r-', label='Error absoluto')

ax1.legend(loc='upper left')
ax2.legend(loc='upper right')

ax1.set_ylabel('Soluciones aprox y teor', color='g')
ax2.set_ylabel('Error absuluto', color='r')
plt.title('Solucion teorica y aprox con error absoluto')
plt.grid()

fig, ax1 = plt.subplots()

ax2 = ax1.twinx()

ax1.plot(x, sol1, 'g-', label='Solucion aprox')
ax1.plot(t, sol_teo, 'b-', label='Solucion teorica')

ax2.plot(x, error_rel, 'r-', label='Error relativo')

ax1.legend(loc='upper left')
ax2.legend(loc='upper right')

ax1.set_ylabel('Soluciones aprox y teor', color='g')
ax2.set_ylabel('Error relativo', color='r')
plt.title('Solucion teorica y aprox con error relativo')
plt.grid()

plt.show()


