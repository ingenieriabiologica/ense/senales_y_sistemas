# -*- coding: utf-8 -*-
"""
Created on Sun Dec 24 16:29:44 2023

@author: Ing. Biologica
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
import scipy.signal as sg
from scipy.interpolate import interp1d
def calcular_butter(frecuencia_corte, orden, fs):
    # Especificaciones del filtro
    
    
    # Calcular la frecuencia de corte normalizada
    
    frecuencia_corte_normalizada = frecuencia_corte / (0.5 * fs)
    
    # Diseñar el filtro Butterworth
    b, a = sg.butter(orden, frecuencia_corte_normalizada, btype='low', analog=False, output='ba')
    
    return b, a
def interpolacion(x, L, fs, orden):
    h_inter = 1/(L)
    xnew = np.arange(0, len(x)-1, h_inter)/fs
    
    k = np.arange(0, len(x))/fs
    # #%%
    f = interp1d(k, x, kind=orden)
    
    ynew = f(xnew)   # use interpolation function returned by `interp1d`
    
    return xnew, ynew

def crear_tiempo(Tc = 0.005, ti = 0, tf = 8):
    t = np.arange(ti, tf, Tc)
    return t

def crear_xc(w_cos, amp_cos, t):
    s = amp_cos[0]*np.cos(w_cos[0]*t) + amp_cos[1]*np.cos(w_cos[1]*t)+ amp_cos[2]*np.cos(w_cos[2]*t)+ amp_cos[3]*np.cos(w_cos[3]*t)+ amp_cos[4]*np.cos(w_cos[4]*t)
    return s

def fft_xc(t, Tc, s):
    freq = np.fft.fftfreq(len(t), d=Tc)
    transf_s = np.fft.fft(s, norm='forward')
    return freq, transf_s

def generar_filtro(transf_xs, fsc, orden = 10):
    # Frecuencia de corte
    frecuencia_corte2 = 5.0  # Frecuencia de corte en Hz
    
    # Es como freq
    freq_lin = np.linspace(-0.5, 0.5, len(transf_xs))*fsc
    
    # Diseña el filtro Butterworth pasa bajos en tiempo continuo
    b, a = signal.butter(orden, frecuencia_corte2, btype='low', analog=True)
    
    # Visualizar la respuesta en frecuencia del filtro
    frecuencias, respuesta = signal.freqs(b, a, freq_lin)
    return frecuencias, respuesta

def generar_seno_continuo():
    T = 0.005
    t = np.arange(0, 8, T)
    s = np.sin(2*np.pi*t)
    return t, s

def generar_senos_discretos():
    # Defino los periodos de muestreo
    T_1 = 0.1
    T_32 = 0.32
    T_42 = 0.42
    T_6 = 0.6
    
    # defino ejes de tiempo discreto segun cada T_x
    t_1 = np.arange(0, 8, T_1)
    t_32 = np.arange(0, 8, T_32)
    t_42 = np.arange(0, 8, T_42)
    t_6 = np.arange(0, 8, T_6)
    
    # defino las señales discretas de frecuencia 1
    s_1 = np.sin(2*np.pi*t_1)
    s_32 = np.sin(2*np.pi*t_32)
    s_42 = np.sin(2*np.pi*t_42)
    s_6 = np.sin(2*np.pi*t_6)
    
    return t_1, t_32, t_42, t_6, s_1, s_32, s_42, s_6

# t_1, t_32, t_42, t_6, s_1, s_32, s_42, s_6 = generar_senos_discretos()

def crear_graficas_interpoladores():
    T = 0.32
    w = np.arange(-100, 100, 0.01)
    
    H0 = (np.sin(0.5*T*w))/(0.5*T*w)
    
    plt.figure()
    plt.plot(w/(2*np.pi), np.abs(H0))
    plt.xlabel('Frecuencia (Hz)')
    plt.ylabel('Amplitud')
    plt.grid()
    plt.title('Respuesta en frecuencia de mantenedor de orden 0')
    
    H1 = (np.sin(0.5*T*w)**2)/((0.5*T*w)**2)
    
    plt.figure()
    plt.plot(w/(2*np.pi), np.abs(H1))
    plt.xlabel('Frecuencia (Hz)')
    plt.ylabel('Amplitud')
    plt.grid()
    plt.title('Respuesta en frecuencia de interpolación lineal')
    
    plt.figure()
    plt.plot(w/(2*np.pi), np.abs(H0))
    plt.plot(w/(2*np.pi), np.abs(H1))
    plt.legend(['Mantenedor orden 0', 'Interpolación lineal'])
    plt.xlabel('Frecuencia (Hz)')
    plt.ylabel('Amplitud')
    plt.grid()
    plt.title('Comparación entre reconstructores')
    

#crear_graficas_interpoladores()
# plt.figure()
# plt.plot(t_1, s_1, 'o-')

# plt.figure()
# plt.plot(t_32, s_32, 'o-')

# plt.figure()
# plt.plot(t_42, s_42, 'o-')

# plt.figure()
# plt.plot(t_6, s_6, 'o-')