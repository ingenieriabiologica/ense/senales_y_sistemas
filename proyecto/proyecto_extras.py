# -*- coding: utf-8 -*-
"""
Created on Fri Dec  1 19:47:53 2023

@author: Ing. Biologica
"""
'''
Resumen del codigo.
Tanto para el filtro pasabajos como para el pasaaltos, se hace lo siguiente:
    - Definir valores de los elementos elegidos: R y C.
    - Filtrado analogico experimental
        - Importar datos obtenidos.
        - Calcular frecuencia de muestreo usada por el osciloscopio del AD2.
    - Filtrado analogico simulado. Comparacion: experimental vs simulado.
    - Filtrado digital.
        - Diseñar filtro digital a partir de la frecuencia de muestreo y tau.
        - Importar datos experimentales (obtenidos con Arduino Due).
        - Comparar salida experimental y simulada con lfilter
        - Pasar el filtro de TC a TD usando una funcion de python (aún no funciona completamente)
'''

import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sg
import pandas as pd
import ejercicio13_diseno_filtros as func


'''Filtro pasabajos'''
#%% Datos de mi experimento
# Elementos usados para el filtro pasabajos analogico
R_pb = 10000
C_pb = 0.000001
tau_pb = R_pb * C_pb   

#%% Filtrado analogico experimental
data = pd.read_csv('s1_filt.csv', sep = ',')    #Sample rate: 704.99Hz

t = data['Time (s)']
sal = data['Channel 1 (V)']     # salida luego del filtrado analógico
ent = data['Channel 2 (V)']     # entrada medida con el osciloscopio, conectando directo al wavegen

t = np.array(t)                 # vector de tiempo exportado
h_t = t[1] - t[0]               # calculo el paso del tiempo, en consecuencia la frecuencia de muestreo

# Frecuencia de muestreo usada por el osciloscopio para adquirir la salida del filtro analogico pasabajos
fs_pb = 1/h_t

ent = np.array(ent)
sal = np.array(sal)

#%% Filtrado analogico simulado. comparacion: experimental vs simulado.
'''El filtro analógico tiene la siguiente forma: H(s) = 1/(1+tau*s)'''

b = [1]
a = [tau_pb, 1]

# Analisis temporal
s = sg.lti(b, a)

t = t - np.min(t)
t, y, _ = sg.lsim(s, ent, t)  # Filtrar la señal de entrada 'x'

w_bode, mag, phase = s.bode()

plt.figure()
plt.semilogx(w_bode, mag)   # Está bien, el wc = 100
plt.xlabel('w (rad/s)')
plt.ylabel('Magnitud (dB)')
plt.title('Bode hecho con la simulacion del sistema')

plt.figure()
plt.plot(t, ent)
plt.plot(t, sal)
plt.plot(t, y, '.-')
plt.legend(['entrada','filtrada experimental', 'filtrada simulacion'])
plt.title('Filtro pasabajos analogico. Comparacion entre salida experimental y simulada')

# Analisis en frecuencia
freq = np.fft.fftfreq(len(t), d = h_t)
ENT = np.fft.fft(ent)   # fft de la entrada experimental
SAL = np.fft.fft(sal)   # fft de la salida experimental
Y = np.fft.fft(y)   # fft de la salida simulada

plt.figure()
plt.plot(freq, np.abs(ENT)) 
plt.plot(freq, np.abs(SAL))
plt.plot(freq, np.abs(Y), '.')
plt.legend(['entrada', 'salida experimental', 'salida simulada'])
plt.ylabel('$|H(e^{jw}|$')
plt.xlabel('Frecuencia (Hz)')
plt.title('Filtro pasabajos analogico. Comparacion salida experimental y simulada')
# La salida simulada se comporta de forma muy similar a la experimental. La diferencia principal se da en 50 Hz, y puede ser porque la experimental esta expuesta a ruido de linea




#%% Filtrado digital. Determino coeficientes como hice en ejercicio13_diseno_filtros.py
# Rango en que voy a buscar el parametros del filtro pasabajos
c = np.arange(0, 0.999, 0.001)

# Coeficientes del filtro pasabajos
a = [1, -c]
b = [1-c]

# Calculo frecuencia angular en la que quiero fijar el polo (regla de 3, pasaje TC-TD)
wx_pb = 1/(tau_pb*fs_pb)

# Funcion que encuentra el c que determina el filtro con la frecuencia angular de corte deseada
c_opt_pb = func.design_disrete_filter(c, b, a, wx_pb)

# Defino mi filtro con el paramentro c hallado
a_pb = [1, -c_opt_pb]
b_pb = [1-c_opt_pb]

#%% Levanto los datos obtenidos con este filtro digital

data = pd.read_csv('s1_filt_digital.csv', sep = ',')    #Sample rate: 1495 Hz (osciloscopio de AD2)

t = data['Time (s)']
ent = data['Channel 1 (V)']     # salida luego del filtrado analógico
sal = data['Channel 2 (V)']     # entrada medida con el osciloscopio, conectando directo al wavegen

t = np.array(t)                 # vector de tiempo exportado
h_t = t[1] - t[0]               # calculo el paso del tiempo, en consecuencia la frecuencia de muestreo

ent = np.array(ent)
sal = np.array(sal)

#%% Filtro digital. Comparacion entre salida experimental y simulada con lfilter

# Analisis en el tiempo
filtrada = sg.lfilter(b_pb, a_pb, ent)

plt.figure()
plt.plot(t, ent)
plt.plot(t, sal)
plt.plot(t, filtrada)
plt.legend(['entrada', 'salida experimental', 'salida simulada'])
plt.title('Filtrado digital, comparacion entre salida experimental y simulada')
plt.ylabel('Amplitud (V)')
plt.xlabel('Tiempo (s)')

# Se ven claramente los ajustes de ganancia y offset que se les tuvo que hacer a la senal para poder usar Arduino para filtrar

# Analisis en frecuencia
freq = np.fft.fftfreq(len(t), d = h_t)
ENT = np.fft.fft(ent)   # fft de la entrada experimental
SAL = np.fft.fft(sal)   # fft de la salida experimental
FILTRADA = np.fft.fft(filtrada)   # fft de la salida simulada

plt.figure()
plt.plot(freq, np.abs(ENT)) 
plt.plot(freq, np.abs(SAL))
plt.plot(freq, np.abs(FILTRADA), '.')
plt.legend(['entrada', 'salida experimental', 'salida simulada'])
plt.ylabel('$|H(e^{jw}|$')
plt.xlabel('Frecuencia (Hz)')
plt.title('Filtro pasabajos digital. Comparacion salida experimental y simulada')

# La salida simulada se comporta de forma relativamente similar que la experimental

#%% Intento. pasar el filtro pasabajos de TC a TD usando una funcion de python

# Frecuencia de muestreo deseada
# fs = 705  # en realidad puse frecuencia de muestreo sobre 2 para que anduviera. con este valor anda perfecto.

# Convertir a tiempo discreto
dt = 1 / fs_pb     # puedo usar h_t también
system_dt = s.to_discrete(dt, method='gbt', alpha=0.5)

b_d = system_dt.num
a_d = system_dt.den

w_d, h_d = sg.freqz(b_d, a_d)

w, h = sg.freqz(b_pb, a_pb)

plt.figure()
plt.plot(w_d, np.abs(h_d))
plt.plot(w, np.abs(h)) # TODO esto no me está dando bien
plt.legend(['con funcion', 'calculado a mano'])
plt.title('Intento de pasar sistema pasabajos de TC a TD')




'''Filtro pasaaltos'''
#%% Datos de mi experimento
# Elementos usados para el filtro pasaaltos analogico
R_pa = 150000
C_pa = 0.000001
tau_pa = R_pa * C_pa

#%% Filtrado analogico experimental
data = pd.read_csv('s2_filt.csv', sep = ',')    #Sample rate: 704.99Hz

t = data['Time (s)']
sal = data['Channel 1 (V)']     # salida luego del filtrado analógico
ent = data['Channel 2 (V)']     # entrada medida con el osciloscopio, conectando directo al wavegen

t = np.array(t)                 # vector de tiempo exportado
h_t = t[1] - t[0]               # calculo el paso del tiempo, en consecuencia la frecuencia de muestreo

# Frecuencia de muestreo usada por el osciloscopio para adquirir la salida del filtro analogico pasabajos
fs_pa = 1/h_t

ent = np.array(ent)
sal = np.array(sal)

#%% Filtrado analogico simulado. comparacion: experimental vs simulado.
'''El filtro analógico tiene la siguiente forma: H(s) = tau*s/(1+tau*s)'''


b = [tau_pa, 0]
a = [tau_pa, 1]

# Analisis temporal
s = sg.lti(b, a)

t = t - np.min(t)
t, y, _ = sg.lsim(s, ent, t)  # Filtrar la señal de entrada 'x'

w_bode, mag, phase = s.bode()

plt.figure()
plt.semilogx(w_bode, mag)   # Está bien, el wc = 6.66
plt.xlabel('w (rad/s)')
plt.ylabel('Magnitud (dB)')
plt.title('Sistema pasaaltos, Bode hecho con la simulacion del sistema')

plt.figure()
plt.plot(t, ent)
plt.plot(t, sal)
plt.plot(t, y, '.-')
plt.legend(['entrada','filtrada experimental', 'filtrada simulacion'])
plt.title('Filtro pasaaltos analogico. Comparacion entre salida experimental y simulada')

# Analisis en frecuencia
freq = np.fft.fftfreq(len(t), d = h_t)
ENT = np.fft.fft(ent)   # fft de la entrada experimental
SAL = np.fft.fft(sal)   # fft de la salida experimental
Y = np.fft.fft(y)   # fft de la salida simulada

plt.figure()
plt.plot(freq, np.abs(ENT)) 
plt.plot(freq, np.abs(SAL))
plt.plot(freq, np.abs(Y), '.')
plt.legend(['entrada', 'salida experimental', 'salida simulada'])
plt.ylabel('$|H(e^{jw}|$')
plt.xlabel('Frecuencia (Hz)')
plt.title('Filtro pasaaltos analogico. Comparacion salida experimental y simulada')

# La simulacion se comporta de forma similar que la salida experimental

#%% Filtrado digital. Determino coeficientes como hice en ejercicio13_diseno_filtros.py
# Rango en que voy a buscar el parametros del filtro pasabajos
c = np.arange(0, 0.999, 0.001)

# Coeficientes del filtro pasaaltos
a = [1, -c]
b = [c, -c]

# Calculo frecuencia angular en la que quiero fijar el polo (regla de 3, pasaje TC-TD)
wx_pa = 1/(tau_pa*fs_pa)

# Funcion que encuentra el c que determina el filtro con la frecuencia angular de corte deseada
c_opt_pa = func.design_disrete_filter(c, b, a, wx_pa)

# Defino mi filtro con el paramentro c hallado
a_pa = [1, -c_opt_pa]
b_pa = [c_opt_pa, -c_opt_pa]

#%% Levanto los datos obtenidos con este filtro digital

data = pd.read_csv('s2_filt_digital.csv', sep = ',')    ##Sample rate: 800Hz (osciloscopio de AD2)

t = data['Time (s)']
ent = data['Channel 1 (V)']     # salida luego del filtrado analógico
sal = data['Channel 2 (V)']     # entrada medida con el osciloscopio, conectando directo al wavegen

t = np.array(t)                 # vector de tiempo exportado
h_t = t[1] - t[0]               # calculo el paso del tiempo, en consecuencia la frecuencia de muestreo

ent = np.array(ent)
sal = np.array(sal)

#%% Filtro digital. Comparacion entre salida experimental y simulada con lfilter

# Analisis en el tiempo
filtrada = sg.lfilter(b_pa, a_pa, ent)
plt.figure()
plt.plot(t, ent)
plt.plot(t, sal)
plt.plot(t, filtrada)
plt.legend(['entrada', 'salida experimental', 'salida simulada'])
plt.title('Filtrado digital, comparacion entre salida experimental y simulada')
plt.ylabel('Amplitud (V)')
plt.xlabel('Tiempo (s)')

# Al igual que para el pasabajos, se ven claramente los ajustes de ganancia y offset que se les tuvo que hacer a la senal para poder usar Arduino para filtrar

# Analisis en frecuencia
freq = np.fft.fftfreq(len(t), d = h_t)
ENT = np.fft.fft(ent)   # fft de la entrada experimental
SAL = np.fft.fft(sal)   # fft de la salida experimental
FILTRADA = np.fft.fft(filtrada)   # fft de la salida simulada

plt.figure()
plt.plot(freq, np.abs(ENT)) 
plt.plot(freq, np.abs(SAL))
plt.plot(freq, np.abs(FILTRADA), '.')
plt.legend(['entrada', 'salida experimental', 'salida simulada'])
plt.ylabel('$|H(e^{jw}|$')
plt.xlabel('Frecuencia (Hz)')
plt.title('Filtro pasaaltos digital. Comparacion salida experimental y simulada')

# La salida experimental y simulada son bastantes parecidas

#%% Intento. pasar el filtro pasaaltos de TC a TD usando una funcion de python

# Frecuencia de muestreo deseada
fs = 157.5  # en realidad puse frecuencia de muestreo sobre 2 para que anduviera. con este valor anda perfecto.

# Convertir a tiempo discreto
dt = 1 / fs
system_dt = s.to_discrete(dt, method='gbt', alpha=0.5)

# Generar señal de entrada para simular
t_d = np.arange(0, 1, int(fs))  # Tiempo de muestra

# Simular respuesta del sistema en tiempo discreto

# plt.figure()
# plt.plot(t_output, output_signal)

# Obtener la respuesta en frecuencia
b_d = system_dt.num
a_d = system_dt.den

w, h = sg.freqz(b_pa, a_pa)
w_d, h_d = sg.freqz(system_dt.num,system_dt.den)

plt.figure()
plt.plot(w_d, np.abs(h_d))
plt.plot(w, np.abs(h)) # Parece que esta bien
plt.legend(['con funcion', 'calculado a mano'])
plt.title('Intento de pasar sistema de TC a TD')

plt.show()
