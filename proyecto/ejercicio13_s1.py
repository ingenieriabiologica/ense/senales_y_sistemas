# -*- coding: utf-8 -*-
"""
Created on Tue Jul 11 20:59:48 2023

@author: Ing. Biologica
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sg
import pandas as pd

# Fs de Arduino: 705 Hz (la que se uso para diseñar el filtro pasabajos)

data = pd.read_csv('s1_filt_digital.csv', sep = ',')    #Sample rate: 1495 Hz (osciloscopio de AD2)

t = data['Time (s)']
ent = data['Channel 1 (V)']     # salida luego del filtrado analógico
sal = data['Channel 2 (V)']     # entrada medida con el osciloscopio, conectando directo al wavegen

t = np.array(t)                 # vector de tiempo exportado
h_t = t[1] - t[0]               # calculo el paso del tiempo, en consecuencia la frecuencia de muestreo

ent = np.array(ent)
sal = np.array(sal)

#%% Grafico en el tiempo

plt.figure()
plt.plot(t, ent)
plt.plot(t, sal)
plt.legend(['entrada', 'salida'])
plt.title('Filtrado digital de la señal 1')
plt.ylabel('Amplitud (V)')
plt.xlabel('Tiempo (s)')

#%% Frecuencia

# fft de los datos medidos, entrada y salida en el filtrado analógico
ENT = np.fft.fft(ent)
SAL = np.fft.fft(sal)

# construcción del eje de frecuencias
freq = np.fft.fftfreq(len(t), d = h_t)

# Se podria normalizar para comparar mejor, ya que el DAC atenua la senal
# La otra opcion es sacar la entrada por el DAC tambien, para que sus efectos se apliquen a ambas

plt.figure()
plt.plot(freq, np.abs(ENT)) 
plt.plot(freq, np.abs(SAL))
plt.title('fft de la entrada y salida en el filtrado digital')
plt.ylabel('$|H(e^{jw}|$')
plt.xlabel('Frecuencia (Hz)')

plt.show()



