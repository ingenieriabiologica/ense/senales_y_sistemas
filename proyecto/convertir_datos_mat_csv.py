# -*- coding: utf-8 -*-
"""
Created on Thu Jun 22 12:24:13 2023

@author: Ing. Biologica
"""
import numpy as np
import math
import scipy.io
import pandas as pd
import matplotlib.pyplot as plt


mat = scipy.io.loadmat('100c.mat')

datos=mat['signal']

frec_muestreo=360
tiempo=np.arange(0,datos.shape[1]/frec_muestreo,1/frec_muestreo)
datos=datos.reshape(datos.shape[1])
plt.figure()
plt.plot(tiempo,datos)

# Ruta y nombre de archivo CSV de salida
df = pd.DataFrame(datos)
archivo_csv = "s1.txt"

# Guardar el DataFrame como archivo CSV
df.to_csv(archivo_csv, index=False, header=False)

#%%
mat = scipy.io.loadmat('124.mat')

datos=mat['s1']

frec_muestreo=360
tiempo=np.arange(0,datos.shape[0]/frec_muestreo,1/frec_muestreo)
datos=datos.reshape(datos.shape[0])
plt.plot(tiempo,datos)

# Ruta y nombre de archivo CSV de salida
df = pd.DataFrame(datos)
archivo_csv = "s2.txt"

# Guardar el DataFrame como archivo CSV
df.to_csv(archivo_csv, index=False, header=False)





