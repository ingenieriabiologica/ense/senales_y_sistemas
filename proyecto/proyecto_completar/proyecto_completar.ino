
///////////////////////////////////////////////////////////////////////////////////////////////
//                             Sampling frequency (defined by user)                          //
///////////////////////////////////////////////////////////////////////////////////////////////

// Comentar/descomentar dependiendo el filtro que estén usando
#define FS ... // Frecuencia de muestreo usada para determinar el filtro pasabajos
//#define FS ... //Frecuencia de muestreo usada para determinar el filtro pasaaltos

#define R 12 // Si usan Arduino 1 R=10, si usan Due R=12 (y descomentan analogReadResolution)

////////////////////////////////////////////////////////////////////////////////////////////////

#define T_delay (1000/FS) //ms
#define resolution pow(2,R)

// to the pins used:
const int analogInPin = A0; // Definimos el puerto por donde va a ingresar la señal de entrada

unsigned long t_act = 0;
double ent_act, ent_ant, sal_act, sal_ant = 0;        
float sal_act_calc = 0;

///*

// Pasabajos
float b0 = ...;  // Numerador 0.08
float a0 =  ...;    // Denominador, e^0
float a1 = ...; // Denominador, e^-jw
float b1 = ...; // Numerador, e^-jw

//*/


/*

// Pasaaltos
float b0 = ...;  // Numerador, e^0
float b1 = ...;  // Numerador, e^-jw
float a0 = ...;    // Denominador, e^0
float a1 = ...; // Denominador, e^-jw

*/

void setup() {
  Serial.begin(9600);
  analogReadResolution(R);
  analogWriteResolution(R); // Le seteo al DAC su resolución 
}

void loop() {
  t_act = millis(); // Dejo guardado en a el valor del instante actual de tiempo

  ent_act = analogRead(analogInPin);  // Leo el valor que ingresa por el puerto analógico, con una resoluci+ón R = 12

  // Aplicar el filtro digital/ calcular la salida

  // Filtro (aplicar ecuación en diferencias)
  sal_act_calc = ...;
  //sal_act_calc = sal_act_calc / a0; no sirve para nada porque a0 = 1 en ambos casos, en caso contrario habría que hacerlo
  sal_act = double(sal_act_calc); // el DAC saca valores enteros, y sal_act_calc va a ser un float

  
  analogWrite(DAC0, sal_act);   // PASABAJOS. Sacar una la señal por el DAC con resolución 12. 
  //analogWrite(DAC0, sal_act + 2043);   // PASAALTOS Sacar una la señal por el DAC con resolución 12. 
  analogWrite(DAC1, ent_act);   // Sacamos la señal de entrada sin haberla modificado

  //Serial.print(ent_act);
  //Serial.print(", ");
  //Serial.println(sal_act_calc);
  while ( millis()<(t_act + T_delay) ){
    
  }               
}
