# -*- coding: utf-8 -*-
"""
Created on Wed Nov 29 14:42:07 2023

@author: Ing. Biologica
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sg
import pandas as pd

R = 150000
C = 0.000001
tau = R*C

data = pd.read_csv('s2_filt.csv', sep=',')    #Sample rate: 704.99Hz

t = data['Time (s)']
sal = data['Channel 1 (V)']
ent = data['Channel 2 (V)']

t = np.array(t)
h_t = t[1] - t[0]

ent = np.array(ent)
sal = np.array(sal)

#%%
plt.figure()
plt.plot(t, ent)
plt.plot(t, sal)
plt.ylabel('Amplitud (V)')
plt.xlabel('Tiempo (s)')
plt.legend(['entrada', 'salida'])
plt.title('Grafico de los datos medidos correspondientes a la señal 2')

#%%
ENT = np.fft.fft(ent)
SAL = np.fft.fft(sal)

freq = np.fft.fftfreq(len(t), d = h_t)

plt.figure()
plt.plot(freq, np.abs(ENT)) 
plt.plot(freq, np.abs(SAL))
plt.ylabel('$|H(e^{jw}|$')
plt.xlabel('Frecuencia (Hz)')
plt.title('fft de la entrada y salida en el filtrado analógico')


#%% Simulacion del filtrado analógico

'''El filtro analógico tiene la siguiente forma: H(s) = s/(1+tau*s)'''

b = [tau, 0]
a = [tau, 1]

s = sg.lti(b, a)

t = t - np.min(t)
t, y, _ = sg.lsim(s, ent, t)  # Filtrar la señal de entrada 'x'

plt.figure()
plt.plot(t, y)
plt.plot(t, ent)
plt.plot(t, sal)
plt.legend(['salida_lti', 'entrada', 'salida'])
plt.title('Comparacion entre salida experimental y la obtenida con funciones de Python')

w_bode, mag, phase = s.bode()

plt.figure()
plt.semilogx(w_bode, mag)   # Está bien, el wc = 6.67
plt.xlabel('w (rad/s)')
plt.ylabel('Magnitud (dB)')
plt.title('Diagrama de Bode del sistema simulado en Python')
plt.show()