# -*- coding: utf-8 -*-
"""
Created on Sat Jul  8 20:57:42 2023

@author: Manu
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sg
import pandas as pd

#%% Señal 1 en el tiempo
data = pd.read_csv('s1.csv', sep=',', header=None)    # Sample rate: 360
data.columns = ['s1']
s1 = data['s1']
h_t = 1/360
t = np.arange(0, len(data))*h_t

plt.figure()
plt.plot(t, s1)
plt.ylabel('Amplitud (V)')
plt.xlabel('Tiempo (s)')
plt.title('Señal 2 en tiempo')

#%% Señal 2 en frecuencia
data = pd.read_csv('s2.csv', sep=',', header=None)    # Sample rate: 360
data.columns = ['s2']
s2 = data['s2']
h_t = 1/360
t = np.arange(0, len(data))*h_t

plt.figure()
plt.plot(t, s2)
plt.title('Señal 2 en tiempo')
plt.ylabel('Amplitud (V)')
plt.xlabel('Tiempo (s)')

#%% Señal 1 en frecuencia

S1 = np.fft.fft(s1)
freq = np.fft.fftfreq(len(t), d = h_t)

plt.figure()
plt.plot(freq, np.abs(S1))
plt.ylabel('$|H(e^{jw}|$')
plt.xlabel('Frecuencia (Hz)')
plt.title('Señal 1 en frecuencia')

'''Ruido en 50 Hz. Va a ser necesario filtar con un pasabajos.'''

#%% Señal 2 en frecuencia

S2 = np.fft.fft(s2)
freq = np.fft.fftfreq(len(t), d = h_t)

plt.figure()
plt.plot(freq, np.abs(S2))
plt.ylabel('$|H(e^{jw}|$')
plt.xlabel('Frecuencia (Hz)')
plt.title('Señal 2 en frecuencia')

'''Ruido en baja frecuencia. Va a ser necesario filtar con un pasaaltos.'''


