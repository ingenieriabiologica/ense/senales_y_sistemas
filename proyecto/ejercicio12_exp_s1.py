# -*- coding: utf-8 -*-
"""
Created on Sat Jul  8 20:25:46 2023

@author: Manu
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sg
import pandas as pd

R = 10000
C = 0.000001
tau = R*C

data = pd.read_csv('s1_filt.csv', sep=',')    # Sample rate: 704.99Hz

t = data['Time (s)']
sal = data['Channel 1 (V)']     # salida luego del filtrado analógico
ent = data['Channel 2 (V)']     # entrada medida con el osciloscopio, conectando directo al wavegen

t = np.array(t)                 # vector de tiempo exportado
h_t = t[1] - t[0]               # calculo el paso del tiempo, en consecuencia la frecuencia de muestreo

ent = np.array(ent)
sal = np.array(sal)

#%% Grafico en el tiempo

plt.figure()
plt.plot(t, ent)
plt.plot(t, sal)
plt.legend(['entrada', 'salida'])
plt.title('Gráfico de los datos medidos correspondientes a la señal 1')
plt.ylabel('Amplitud (V)')
plt.xlabel('Tiempo (s)')

#%% Frecuencia

# fft de los datos medidos, entrada y salida en el filtrado analógico
ENT = np.fft.fft(ent)
SAL = np.fft.fft(sal)

# construcción del eje de frecuencias
freq = np.fft.fftfreq(len(t), d = h_t)

plt.figure()
plt.plot(freq, np.abs(ENT)) 
plt.plot(freq, np.abs(SAL))
plt.title('fft de la entrada y salida en el filtrado analógico')
plt.ylabel('$|H(jw)|$')
plt.xlabel('Frecuencia (Hz)')


#%% Simulacion del filtrado analógico

'''El filtro analógico tiene la siguiente forma: H(s) = 1/(1+tau*s)'''

b = [1]
a = [tau, 1]

s = sg.lti(b, a)

t = t - np.min(t)
t, y, _ = sg.lsim(s, ent, t)  # Filtrar la señal de entrada 'x'

plt.figure()
plt.plot(t, ent)
plt.plot(t, sal)
plt.plot(t, y, '.-')
plt.legend(['entrada', 'salida', 'salida_lti'])
plt.title('Comparacion entre salida experimental y la obtenida con funciones de Python')

w_bode, mag, phase = s.bode()

plt.figure()
plt.semilogx(w_bode, mag)   # Está bien, el wc = 100
plt.xlabel('w (rad/s)')
plt.ylabel('Magnitud (dB)')
plt.title('Diagrama de Bode del sistema simulado en Python')
plt.show()

