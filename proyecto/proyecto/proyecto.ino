
///////////////////////////////////////////////////////////////////////////////////////////////
//                             Sampling frequency (defined by user)                          //
///////////////////////////////////////////////////////////////////////////////////////////////

//#define FS 705 //Hz pasabajos
#define FS 315 //Hz pasaaltos

#define R 12 // Si usan Arduino 1 R=10, si usan Due R=12 (y descomentan analogReadResolution)

////////////////////////////////////////////////////////////////////////////////////////////////

#define T_delay (1000/FS) //ms
#define resolution pow(2,R)

// to the pins used:
const int analogInPin = A0; // Voy a usar A0 para leer la se;al de entrada, que sale del osciloscopio del AD2

unsigned long t_act = 0;
double ent_act, ent_ant, sal_act, sal_ant = 0;        
float sal_act_calc = 0;

/*

// Pasabajos
float b0 = 0.08;  // Numerador 0.08
float a0 =  1;    // Denominador, e^0
float a1 = -0.92; // Denominador, e^-jw
float b1 = 0;
*/


///*

// Pasaaltos
float b0 = 1.02;  // Numerador, e^0
float b1 = -1.02;  // Numerador, e^-jw
float a0 =  1;    // Denominador, e^0
float a1 = -0.96; // Denominador, e^-jw

//*/

void setup() {
  Serial.begin(9600);
  analogReadResolution(R);
  analogWriteResolution(R); // Le seteo al DAC su resolución 
}

void loop() {
  t_act = millis(); // Dejo guardado en a el valor del instante actual de tiempo

  ent_act = analogRead(analogInPin);  // Leo el valor que ingresa por el puerto analógico, con una resoluci+ón R = 12

  // Aplicar el filtro digital/ calcular la salida

  // Filtro
  sal_act_calc = b0*ent_act - a1*sal_ant + b1*ent_ant;
  //sal_act_calc = sal_act_calc / a0; no sirve para nada porque a0 = 1 en ambos casos
  sal_act = double(sal_act_calc);

  
  //analogWrite(DAC0, sal_act);   // Sacar una la señal por el DAC con resolución 12
  analogWrite(DAC0, sal_act + 2043);   // Sacar una la señal por el DAC con resolución 12
  analogWrite(DAC1, ent_act);
  
  ent_ant = ent_act;
  sal_ant = sal_act;

  //Serial.print(ent_act);
  //Serial.print(", ");
  //Serial.println(sal_act_calc);
  while ( millis()<(t_act + T_delay) ){
    
  }               
}
