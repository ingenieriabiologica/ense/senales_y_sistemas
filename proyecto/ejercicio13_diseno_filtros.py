# -*- coding: utf-8 -*-
"""
Created on Tue Nov 28 17:22:18 2023

@author: Ing. Biologica
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sg

def design_disrete_filter(c, b, a ,wx):
# Entradas:
    # c: arreglo que contiene todos los posibles valores del parametro que se quiere determinar
    # b: coeficientes del numerador con c dentro. ejemplo b = [c, -c]
    # a: coeficientes del denominador con c dentro. ejemplo a = [1, -c]
    # wx: frecuencia angular de corte del filtro
# Salida: c_w siendo este el valor de c para el cual se tiene wx como frec de corte

    hs = []     # Lista donde voy a guardar los valores de ganancia para cada valor de c
    
    for i in range(len(c)):
        # print('nueva iteracion: ' + str(i))
        # Construyo los a y b para el valor de c de esta iteracion c
        #print(i)
        a_aux = []
        b_aux = []
        for j in range(len(a)):
            if (type(a[j]) is np.ndarray):
                a_aux.append(a[j][i])
            else:
                a_aux.append(a[j])
                
        for j in range(len(b)):
            if (type(b[j]) is np.ndarray):
                b_aux.append(b[j][i])
            else:
                b_aux.append(b[j])

        w, h = sg.freqz(b_aux, a_aux)
        hs.append(np.abs(h))    # los w no los guardo porque siempre son iguales
        
    # Busco en w en que lugar esta wx (o el valor mas cercano)
    wx_ind = np.argmin(np.abs(w - wx))

    # Para hallar el c óptimo, voy a buscar las ganancias solo para esta frecuencia angular,
    # para todos los valores de c.
    # La que sea mas cercana a 0.5, correspondera a mi c optimo
    hs = np.array(hs)
    h_wx = hs[:, wx_ind]

    # Busco donde se da la ganancia = 0.5
    c_opt_ind = np.argmin(np.abs(h_wx - 0.5))
    c_opt = c[c_opt_ind]

    return c_opt


def show_filter_freq_response(b, a):
    w, h = sg.freqz(b, a)
    plt.figure()
    plt.plot(w, np.abs(h))
    
#%% Filtro pasabajos

# Elementos usados para el filtro pasabajos analogico
R_pb = 10000
C_pb = 0.000001
tau_pb = R_pb * C_pb   

# Frecuencia de muestreo usada por el osciloscopio para adquirir la salida del filtro analogico pasabajos
fs_pb = 705

# Rango en que voy a buscar el parametros del filtro pasabajos
c = np.arange(0, 0.999, 0.001)

# Coeficientes del filtro pasabajos
a = [1, -c]
b = [1-c]

# Calculo frecuencia angular en la que quiero fijar el polo (regla de 3, pasaje TC-TD)
wx_pb = 1/(tau_pb*fs_pb)

# Funcion que encuentra el c que determina el filtro con la frecuencia angular de corte deseada
c_opt_pb = design_disrete_filter(c, b, a, wx_pb)

# Defino mi filtro con el paramentro c hallado
a_pb = [1, -c_opt_pb]
b_pb = [1-c_opt_pb]

# Grafico la respuesta en frecuencia del filtro
show_filter_freq_response(b_pb, a_pb)
plt.title("Respuesta al impulso (pasa-bajos)")
#%% Filtro pasaaltos

# Elementos usados para el filtro pasaaltos analogico
R_pa = 150000
C_pa = 0.000001
tau_pa = R_pa * C_pa   

# Frecuencia de muestreo usada por el osciloscopio para adquirir la salida del filtro analogico pasaaltos
fs_pa = 315

# Rango en que voy a buscar el parametros del filtro pasaaltos
c = np.arange(0, 0.999, 0.001)

# Coeficientes del filtro pasaaltos
a = [1, -c]
b = [c, -c]

# Calculo frecuencia angular en la que quiero fijar el polo (regla de 3, pasaje TC-TD)
wx_pa = 1/(tau_pa*fs_pa)

# Funcion que encuentra el c que determina el filtro con la frecuencia angular de corte deseada
c_opt_pa = design_disrete_filter(c, b, a, wx_pa)

# Defino mi filtro con el paramentro c hallado
a_pa = [1, -c_opt_pa]
b_pa = [c_opt_pa, -c_opt_pa]

# Grafico la respuesta en frecuencia del filtro
show_filter_freq_response(b_pa, a_pa)
plt.title("Respuesta al impulso (pasa-altos)")
plt.show()
