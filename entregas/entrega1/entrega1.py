# -*- coding: utf-8 -*-
"""
Created on Tue Feb 27 18:40:21 2024

@author: Manu
"""

import numpy as np
import matplotlib.pyplot as plt

# Tanto en el estudio teórico, simulación y gráfica experimental, se usaron los valores medidos, para favorecer la comparación
V_i = 4.563   # voltaje inicial del capacitor medido
C = 8.31e-6     # valor medido
R = 96.55e3     # valor medido
tau = R*C       # por comodidad defino la constante tau

#%% Ejercicio 1- estudio teórico

# Creo un vector de tiempo para graficar vc(t), la cual tiene la expresión resultante de resolver la ecuación difernecial del sistema
h_t = 0.1   # paso del vector de tiempo
t_max = 4   # tiempo máximo del vector en segundos

t = np.arange(0, t_max, h_t)

v_c = V_i*np.exp(-t/tau)

ind_tau = np.argmin(np.abs(t - tau))

plt.figure()
plt.plot(t, v_c, '.-')
plt.plot(t[ind_tau], v_c[ind_tau], 'o', color = 'red')
plt.xlabel('Tiempo (s)')
plt.ylabel('Voltaje (V)')
plt.title('Ejercicio 1: comportamiento teórico de la descarga del capacitor, $h = ' + str(h_t) + '$')
plt.grid()
plt.legend(['$v_c(t)$', r'$\tau$'])
for x, y in zip([t[ind_tau]], [v_c[ind_tau]]):
    plt.plot([x, x], [0, y], linestyle='--', color='gray')  # línea punteada vertical
    plt.plot([0, x], [y, y], linestyle='--', color='gray')  # línea punteada horizontal
    plt.text(x, 0, f't={x:.2f}', ha='right', va='bottom')  # texto para el valor de x
    plt.text(0, y, f'$v_c(t)$={y:.2f}', ha='right', va='top')     # texto para el valor de y

#%% Ejercicio 2- simulación
# Voy a usar Euler hacia adelante y hacia atrás
h_s = 0.1   # paso elegido para la simulación
t_sim = np.arange(0, t_max, h_s)    # vector de tiempo de la simulación
v_sim = []
v_sim.append(V_i)   # esta es la condición incial de Euler

for i in range(1, int(t_max/h_s)):
    aux = v_sim[i-1] - h_s*v_sim[i-1]/tau   # esta fórmula por recurrencia sale de plantear Euler hacia adelante
    v_sim.append(aux)

# Elegí el mismo paso para la simulación
plt.figure()
plt.plot(t, v_c, '.-')
plt.plot(t_sim, v_sim, '.-')
plt.xlabel('Tiempo (s)')
plt.ylabel('Voltaje (V)')
plt.title('Ejercicio 2: simulación, $h = ' + str(h_s) + '$')
plt.grid()
plt.legend(['$v_c(t)$', 'simulación'])

# Graficos de error
# Crear la figura y los subgráficos
fig, axs = plt.subplots(2, 1)  # (filas, columnas)
fig.suptitle('Gráficos de error para Euler hacia adelante')

# Graficar en el primer subgráfico
axs[0].plot(t_sim, np.abs(v_c - v_sim))
axs[0].set_title('Gráfico de error absoluto')
axs[0].set_xlabel('Tiempo (s)')
axs[0].set_ylabel('Voltaje (V)')
axs[0].grid(True)
 
# Graficar en el segundo subgráfico
axs[1].plot(t_sim, 100*np.abs(v_c - v_sim)/np.abs(v_c))
axs[1].set_title('Gráfico de error relativo')
axs[1].set_xlabel('Tiempo (s)')
axs[1].set_ylabel('Porcentaje de error')
axs[1].grid(True)

# Mostrar la figura
plt.tight_layout()  # Ajusta automáticamente los espacios entre subgráficos


# Cambio el paso del vector de tiempo de vc(t)
h_t = 0.01   # paso del vector de tiempo
t_max = 4   # tiempo máximo del vector en segundos
t = np.arange(0, t_max, h_t)
v_c = V_i*np.exp(-t/tau)

plt.figure()
plt.plot(t, v_c, '.-')
plt.plot(t_sim, v_sim, '.-')
plt.xlabel('Tiempo (s)')
plt.ylabel('Voltaje (V)')
plt.title('Ejercicio 2: simulación Euler adelante, $h_s = ' + str(h_s) + '$ y $h_t = ' + str(h_t) + '$')
plt.grid()
plt.legend(['$v_c(t)$', 'Simulación Euler adelante'])

# Ahora con Euler hacia atrás, uso mismo h_t y t_sim
i = np.arange(0, len(t_sim))
v_sim_atras = V_i/((1 + h_s/tau)**i)

# Vuelvo al vc(t) de antes
h_t = 0.1   # paso del vector de tiempo
t_max = 4   # tiempo máximo del vector en segundos
t = np.arange(0, t_max, h_t)
v_c = V_i*np.exp(-t/tau)

# Comparacion entre simulación con Euler hacia atrás y resultado teórico
plt.figure()
plt.plot(t, v_c, '.-')
plt.plot(t_sim, v_sim_atras, '.-')
plt.xlabel('Tiempo (s)')
plt.ylabel('Voltaje (V)')
plt.title('Ejercicio 2: simulación Euler atrás, $h = ' + str(h_s) + '$')
plt.grid()
plt.legend(['$v_c(t)$', 'Simulación Euler atrás'])

# Graficos de error
# Crear la figura y los subgráficos
fig, axs = plt.subplots(2, 1)  # (filas, columnas)
fig.suptitle('Gráficos de error para Euler hacia atrás')

# Graficar en el primer subgráfico
axs[0].plot(t_sim, np.abs(v_c - v_sim_atras))
axs[0].set_title('Gráfico de error absoluto')
axs[0].set_xlabel('Tiempo (s)')
axs[0].set_ylabel('Voltaje (V)')
axs[0].grid(True)

# Graficar en el segundo subgráfico
axs[1].plot(t_sim, 100*np.abs(v_c - v_sim_atras)/np.abs(v_c))
axs[1].set_title('Gráfico de error relativo')
axs[1].set_xlabel('Tiempo (s)')
axs[1].set_ylabel('Porcentaje de error')
axs[1].grid(True)

# Mostrar la figura
plt.tight_layout()  # Ajusta automáticamente los espacios entre subgráficos

# Comparacion entre Euler hacia adelante y Euler hacia atrás
plt.figure()
plt.plot(t_sim, v_sim, '.-')
plt.plot(t_sim, v_sim_atras, '.-')
plt.xlabel('Tiempo (s)')
plt.ylabel('Voltaje (V)')
plt.title('Ejercicio 2: comparación entre Euler hacia adelante y atrás')
plt.grid()
plt.legend(['Simulación Euler adelante', 'Simulación Euler atrás'])

#%% Ejercicio 3- estudio experimental
# Levantar el archivo de datos
data = np.loadtxt('datos.csv', delimiter = ',')

t_exp = data[:, 0]
v_exp = data[:, 1]

# El vector de tiempo comienza en cero


# Corto a mano para quedarme solo con la parte de la descarga
t_exp = t_exp[895:]
v_exp = v_exp[895:]

t_exp = t_exp - np.min(t_exp)

plt.figure()
plt.plot(t_exp, v_exp)
plt.xlabel('Tiempo (s)')
plt.ylabel('Voltaje (V)')
plt.title('Señal de descarga de capacitor experimental')
plt.grid()

# Comparacion con la expresión teórica vc(t)
v_c_t_exp = V_i*np.exp(-t_exp/tau)  # Calculo el voltaje teórico con el eje de tiempo experimental para poder calcular errores

plt.figure()
plt.plot(t_exp, v_c_t_exp, '.-')
plt.plot(t_exp, v_exp, '.-')
plt.xlabel('Tiempo (s)')
plt.ylabel('Voltaje (V)')
plt.title('Ejercicio 3: descarga del capacitor experimental')
plt.grid()
plt.legend(['$v_c(t)$', 'Señal experimental'])

# Graficos de error
# Crear la figura y los subgráficos
fig, axs = plt.subplots(2, 1)  # (filas, columnas)
fig.suptitle('Gráficos de error de señal experimental')

# Graficar en el primer subgráfico
axs[0].plot(t_exp, np.abs(v_c_t_exp - v_exp))
axs[0].set_title('Gráfico de error absoluto')
axs[0].set_xlabel('Tiempo (s)')
axs[0].set_ylabel('Voltaje (V)')
axs[0].grid(True)

# Graficar en el segundo subgráfico
axs[1].plot(t_exp, 100*np.abs(v_c_t_exp - v_exp)/np.abs(v_c_t_exp))
axs[1].set_title('Gráfico de error relativo')
axs[1].set_xlabel('Tiempo (s)')
axs[1].set_ylabel('Porcentaje de error')
axs[1].grid(True)

# Mostrar la figura
plt.tight_layout()  # Ajusta automáticamente los espacios entre subgráficos
plt.show()

# Como el error relativo se va muy para arriba cuando la señal teórica tiende a 0, voy a cortar el eje de tiempo
t_exp = t_exp[:3200]
v_exp = v_exp[:3200]
v_c_t_exp = v_c_t_exp[:3200]

plt.figure()
plt.plot(t_exp, 100*np.abs(v_c_t_exp - v_exp)/np.abs(v_c_t_exp))
plt.xlabel('Tiempo (s)')
plt.ylabel('Porcentaje de error')
plt.title('Error relativo de señal experimental')
plt.grid()

plt.show()




