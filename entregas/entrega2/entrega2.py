# -*- coding: utf-8 -*-
"""
Created on Wed Feb 28 22:45:21 2024

@author: Manu
"""

import numpy as np
import matplotlib.pyplot as plt

R=220000    # Valor relevado de la resistencia
C=0.00001   # Valor relevado del capacitor
tau = R*C   # Constante de tiempo del sistema

#%% Ejercicio 4- Respuesta al impulso y escalón teórica

# Voy a usar en principio un eje de tiempo con tiempo negativo
paso_t = 0.01
t_min = -2
t_max = 10
t = np.arange(t_min, t_max, paso_t)

u_t = np.zeros(len(t))
u_t[int(np.abs(t_min)/paso_t):] = 1 

h_t = (u_t/tau)*np.exp(-t/tau)  # Respuesta al impulso

ind_tau = np.argmin(np.abs(t - tau))

# Visualización
plt.figure()
plt.plot(t, h_t)
plt.xlabel('Tiempo (s)')
plt.ylabel('Voltaje (V)')
plt.title('Ejercicio 4: respuesta al impulso teórica, $h = ' + str(paso_t) + '$')
plt.grid()
for x, y in zip([t[ind_tau]], [h_t[ind_tau]]):
    plt.plot([x, x], [0, y], linestyle='--', color='gray')  # línea punteada vertical
    plt.plot([0, x], [y, y], linestyle='--', color='gray')  # línea punteada horizontal
    plt.text(x, 0, f't={x:.2f}', ha='right', va='bottom')  # texto para el valor de x
    plt.text(0, y, f'$v_c(t)$={y:.2f}', ha='right', va='top')     # texto para el valor de y
    
s_t = (1 - np.exp(-t/tau))*u_t/tau  # respuesta al escalón- 
# TODO fijarme de donde sale ese tau que divide (tuve que dividir entre tau para que quedara bien)

# Visualización
plt.figure()
plt.plot(t, s_t)
plt.xlabel('Tiempo (s)')
plt.ylabel('Voltaje (V)')
plt.title('Ejercicio 4: respuesta al escalón teórica, $h = ' + str(paso_t) + '$')
plt.grid()
for x, y in zip([t[ind_tau]], [s_t[ind_tau]]):
    plt.plot([x, x], [0, y], linestyle='--', color='gray')  # línea punteada vertical
    plt.plot([0, x], [y, y], linestyle='--', color='gray')  # línea punteada horizontal
    plt.text(x, 0, f't={x:.2f}', ha='right', va='bottom')  # texto para el valor de x
    plt.text(0, y, f'$v_c(t)$={y:.2f}', ha='right', va='top')     # texto para el valor de y

#%% Ejercicio 5- simulaciones de respuesta al impulso y escalón
# Impulso
# Euler hacia adelante
h_s = 0.1   # paso elegido para la simulación
t_sim = np.arange(0, t_max, h_s)    # vector de tiempo de la simulación

x0 = 1/h_s  # esta es la condición incial de Euler, el valor de la delta en 0

n = np.arange(0, len(t_sim))

v_sim_adelante = (x0*h_s/tau)*(1 - h_s/tau)**(n)     # simulación con Euler hacia adelante
# TODO: en la cuenta me dio elevado a la n-1 pero funciona mejor con n. Ver próximo TODO.

ind_tau = np.argmin(np.abs(t_sim - tau))


# Visualización
plt.figure()
plt.plot(t_sim, v_sim_adelante, 'o-')
plt.plot(t, h_t)
plt.xlabel('Tiempo (s)')
plt.ylabel('Voltaje (V)')
plt.title('Ejercicio 5: simulación con Euler hacia adelante y señal teórica)')
plt.legend(['Simulación', 'Teórica'])
plt.grid()

for x, y in zip([t_sim[ind_tau]], [v_sim_adelante[ind_tau]]):
    plt.plot([x, x], [0, y], linestyle='--', color='gray')  # línea punteada vertical
    plt.plot([0, x], [y, y], linestyle='--', color='gray')  # línea punteada horizontal
    plt.text(x, 0, f't={x:.2f}', ha='right', va='bottom')  # texto para el valor de x
    plt.text(0, y, f'$v_c(t)$={y:.2f}', ha='right', va='top')     # texto para el valor de y

# Euler hacia atrás
# Primero tengo que definir el valor de la delta en 0, voy a usar el mismo que antes
x0 = 1/h_s

v_sim_atras = x0 * (h_s/(tau+h_s)) * (tau/(tau+h_s))**(n-1)     # simulación con Euler hacia atrás

ind_tau = np.argmin(np.abs(t_sim - tau))

# Visualización
plt.figure()
plt.plot(t_sim, v_sim_atras, 'o-')
plt.plot(t, h_t)
plt.xlabel('Tiempo (s)')
plt.ylabel('Voltaje (V)')
plt.title('Ejercicio 5: simulación con Euler hacia atrás y señal teórica)')
plt.legend(['Simulación', 'Teórica'])
plt.grid()

for x, y in zip([t_sim[ind_tau]], [v_sim_atras[ind_tau]]):
    plt.plot([x, x], [0, y], linestyle='--', color='gray')  # línea punteada vertical
    plt.plot([0, x], [y, y], linestyle='--', color='gray')  # línea punteada horizontal
    plt.text(x, 0, f't={x:.2f}', ha='right', va='bottom')  # texto para el valor de x
    plt.text(0, y, f'$v_c(t)$={y:.2f}', ha='right', va='top')     # texto para el valor de y

#%%
''' Comparacion entre simulaciones y teórica
1) Cualitativo: al aumentar el tau el sistema demora más tiempo el descargarse (simulación), aunque no coincida con el tau teórico
2) Constante de tiempo tau: la constante de tiempo en la simulación es similar a la teórica
3) La amplitud de la señal de la simulación es similar a la teórica
    
Veamos todos los puntos para Euler hacia adelante y hacia atrás
'''

# Según las últimas gráficas, parecen tener comportamientos muy similares. Variemos el tau
tau = 1.5
h_t = (u_t/tau)*np.exp(-t/tau)  # Respuesta al impulso
ind_tau = np.argmin(np.abs(t - tau))    # índice de tau

# Hago Euler hacia adelante con el nuevo tau
x0 = 1/h_s  # valor de la entrada en tiempo 0
v_sim_adelante = x0 * (h_s/(tau+h_s)) * (tau/(tau+h_s))**(n-1)     # simulación con Euler hacia atrás
# TODO: acá funciona mejor con elevarlo a la n-1, es raro

ind_tau = np.argmin(np.abs(t_sim - tau))

# Hago Euler hacia atrás con el nuevo tau
x0 = 1/h_s  # valor de la entrada en tiempo 0
v_sim_atras = x0 * (h_s/(tau+h_s)) * (tau/(tau+h_s))**(n-1)     # simulación con Euler hacia atrás
ind_tau = np.argmin(np.abs(t_sim - tau))

# Visualización Euler hacia adelante
plt.figure()
plt.plot(t_sim, v_sim_adelante, 'o-')
plt.plot(t, h_t)
plt.xlabel('Tiempo (s)')
plt.ylabel('Voltaje (V)')
plt.title('Ejercicio 5: simulación con Euler hacia adelante y señal teórica, $x_0 = ' + str(x0) + '$')
plt.legend(['Simulación', 'Teórica'])
plt.grid()

for x, y in zip([t_sim[ind_tau]], [v_sim_adelante[ind_tau]]):
    plt.plot([x, x], [0, y], linestyle='--', color='gray')  # línea punteada vertical
    plt.plot([0, x], [y, y], linestyle='--', color='gray')  # línea punteada horizontal
    plt.text(x, 0, f't={x:.2f}', ha='right', va='bottom')  # texto para el valor de x
    plt.text(0, y, f'$v_c(t)$={y:.2f}', ha='right', va='top')     # texto para el valor de y

# Visualización Euler hacia atrás
plt.figure()
plt.plot(t_sim, v_sim_atras, 'o-')
plt.plot(t, h_t)
plt.xlabel('Tiempo (s)')
plt.ylabel('Voltaje (V)')
plt.title('Ejercicio 5: simulación con Euler hacia atrás y señal teórica, $x_0 = ' + str(x0) + '$')
plt.legend(['Simulación', 'Teórica'])
plt.grid()

for x, y in zip([t_sim[ind_tau]], [v_sim_atras[ind_tau]]):
    plt.plot([x, x], [0, y], linestyle='--', color='gray')  # línea punteada vertical
    plt.plot([0, x], [y, y], linestyle='--', color='gray')  # línea punteada horizontal
    plt.text(x, 0, f't={x:.2f}', ha='right', va='bottom')  # texto para el valor de x
    plt.text(0, y, f'$v_c(t)$={y:.2f}', ha='right', va='top')     # texto para el valor de y

# Parece todo funcionar correctamente, los 3 puntos. 
# Ahora voy a cambiar x0
x0 = 1
v_sim_atras = x0 * (h_s/(tau+h_s)) * (tau/(tau+h_s))**(n-1)     # simulación con Euler hacia atrás
ind_tau = np.argmin(np.abs(t_sim - tau))

# Visualización
plt.figure()
plt.plot(t_sim, v_sim_atras, 'o-')
# plt.plot(t, h_t)
plt.xlabel('Tiempo (s)')
plt.ylabel('Voltaje (V)')
plt.title('Ejercicio 5: simulación con Euler hacia atrás y señal teórica, $x_0 = ' + str(x0) + '$')
# plt.legend(['Simulación', 'Teórica'])
plt.grid()

for x, y in zip([t_sim[ind_tau]], [v_sim_atras[ind_tau]]):
    plt.plot([x, x], [0, y], linestyle='--', color='gray')  # línea punteada vertical
    plt.plot([0, x], [y, y], linestyle='--', color='gray')  # línea punteada horizontal
    plt.text(x, 0, f't={x:.2f}', ha='right', va='bottom')  # texto para el valor de x
    plt.text(0, y, f'$v_c(t)$={y:.2f}', ha='right', va='top')     # texto para el valor de y

# Lo único que cambia es el punto 3, ya que queda escalado por el término h_s
# Para Euler hacia adelante es lo mismo, ya que también está multiplicado x0 a todo

#%% Respuestas al escalón
# Euler hacia adelante, lo voy a hacer por recurrencia

# Defino el primer valor en 0, mi condición incial
esc_adelante = [0]
for i in range(1, len(t_sim)+1):
    esc_adelante.append(esc_adelante[i-1] + h_s*((1-esc_adelante[i-1])/tau))    # el 1 es por la altura del escalón

plt.figure()
plt.plot(t_sim, esc_adelante[:-1], '.-')
plt.xlabel('Tiempo (s)')
plt.ylabel('Voltaje (V)')
plt.title('Simulación de la respuesta al escalón, Euler hacia adelante recursivo')
plt.grid()

for x, y in zip([t_sim[ind_tau]], [esc_adelante[ind_tau]]):
    plt.plot([x, x], [0, y], linestyle='--', color='gray')  # línea punteada vertical
    plt.plot([0, x], [y, y], linestyle='--', color='gray')  # línea punteada horizontal
    plt.text(x, 0, f't={x:.2f}', ha='right', va='bottom')  # texto para el valor de x
    plt.text(0, y, f'$v_c(t)$={y:.2f}', ha='right', va='top')     # texto para el valor de y

# El comportamiento de tau parece ser correcto

# Comparación con respuesta al escalón teórica
s_t = (1 - np.exp(-t/tau))*u_t  # respuesta al escalón, le saque el tau dividiendo
# TODO ver esto del tau dividiendo

# Visualización
plt.figure()
plt.plot(t, s_t)
plt.plot(t_sim, esc_adelante[:-1], '.-')
plt.xlabel('Tiempo (s)')
plt.ylabel('Voltaje (V)')
plt.title('Simulación de la respuesta al escalón, Euler hacia adelante recursivo')
plt.grid()
plt.legend(['teórica', 'simulación'])

for x, y in zip([t_sim[ind_tau]], [esc_adelante[ind_tau]]):
    plt.plot([x, x], [0, y], linestyle='--', color='gray')  # línea punteada vertical
    plt.plot([0, x], [y, y], linestyle='--', color='gray')  # línea punteada horizontal
    plt.text(x, 0, f't={x:.2f}', ha='right', va='bottom')  # texto para el valor de x
    plt.text(0, y, f'$v_c(t)$={y:.2f}', ha='right', va='top')     # texto para el valor de y

plt.show()
