# -*- coding: utf-8 -*-
"""
Created on Sun Mar  3 19:29:49 2024

@author: Manu
"""

import numpy as np
import matplotlib.pyplot as plt

# TODO puede ser util hacer algunas funciones para que el codigo quede mas corto y organizado.
# Incluso podria hacer un notebook 
#%% Ejercicio 7

# PARTE A
# Me defino un eje w para graficar H(jw)
w = np.arange(-5*np.pi, 5*np.pi, 0.1)
H_jw = 1/(1 + 1j*w) # se hace en la pag. 229 del Opp

# TODO Calculo modulo a fase a mano, luego hay que ver cómo queremos que lo hagan los estudiantes
# Modulo
mod_H_jw = 1/(1+w**2)**(0.5)

# Fase
fase_H_jw = -np.arctan(w)   # arg(1) = 0. Y al ser un cociente es la resta de los argumentos

# Ahora lo hago con funciones para comparar
# Modulo
fmod_H_jw = np.abs(H_jw)

# Fase
ffase_H_jw = np.angle(H_jw)

# Visualización de módulo y fase, calculada teóricamente y con función de Python
plt.figure()
plt.plot(w, mod_H_jw, '.-')
plt.plot(w, fmod_H_jw)
plt.xlabel('\\omega (rad/s)')
plt.ylabel('$|H(jw)|$')
plt.grid()
plt.legend(['A mano', 'Con función de Python'])
plt.title('Módulo de $H(j \\omega)$')

plt.figure()
plt.plot(w, fase_H_jw, '.-')
plt.plot(w, ffase_H_jw)
plt.xlabel('\\omega (rad/s)')
plt.ylabel('$ \\angle H(j \\omega)$')
plt.grid()
plt.legend(['A mano', 'Con función de Python'])
plt.title('Fase de $H(j \\omega)$')

#%% PARTE B
# Usamos la señal vista en el ejemplo 3.5, pág. 193 Opp
# Los coeficientes de la señal a_k tiene la siguiente forma ak​=sin(kω0​T1​)/kπ.
# Traigo el código desde el práctico 5

# Primero tengo que elegir un período T para trabajar, luego voy a analizar el efecto que tiene esto
T = 16

# Elijo T1, ancho del pulso
T1 = 4

# En la Serie de Fourier incide la relación entre T1 y T así que es útil definir esta variable
rel_T = T1/T

# Construimos un eje k, donde los limites superiores e inferior se expresar como una variable para poder ajustarla en caso que sea necesario
# epsilon de la máquina (para evitar errores numéricos)
eps = np.finfo(float).eps
print("eps: ", eps)

# Fijamos el valor maximo del eje de k que queremos
k_max = 20

# Creamos un eje k simetrico
k = np.arange(-k_max, k_max)

# Ahora que tenemos el k, calculamos los coeficientes ak
ak = np.sin(2*np.pi*rel_T*k)/(k*np.pi+eps)

# Ahora falta calcular el valor en k = 0
# En que posicion del arreglo k esta k = 0? En k[k_max], porque es simetrico con respecto a 0
ak[k_max] = 2*rel_T

# Visualizacion en k
plt.figure()
plt.stem(k, ak)
plt.grid()
plt.xlabel('$k$')
plt.ylabel('$a_{k}$')
plt.title('Coeficientes $a_{k}$')

# Visualizacion en w
w0 = 2*np.pi/T

plt.figure()
plt.stem(k*w0, ak)
plt.grid()
plt.xlabel('$\\omega (rad/s)$')
plt.ylabel('$a_{k}$')
plt.title('Coeficientes $a_{k}$')

'''Para calcular la salida vamos a realizar un producto en frecuencia, es decir multiplicar
los coeficientes. Para ello primero tenemos que calcular hk (coeficientes del sistema).
Como se vio en el practico 9, calculamos hk = H(jkw0), es decir evaluar H(jw) en w = k.w0
'''

# Por lo tanto mi nuevo w va a ser w = k.w0
w_disc = k*w0

# Calculo los coeficientes del sistema hk
hk = 1/(1 + 1j*w_disc)

# Al igual que más arriba, calculo modulo y fase
# Modulo
mod_hk = 1/(1+w_disc**2)**(0.5)

# Fase
fase_hk = -np.arctan(w_disc)   # arg(1) = 0. Y al ser un cociente es la resta de los argumentos

# Visualizacion de coeficientes del sistema junto con expresion original
plt.figure()
plt.plot(w, mod_H_jw)
plt.plot(w_disc, mod_hk, '.-')
plt.xlabel('$\\omega (rad/s)$')
plt.ylabel('Amplitud')
plt.grid()
plt.legend(['$H(j \\omega)$', '$H(jk \\omega_{0})$'])
plt.title('Módulo de $H(j \\omega )$')

# Al igual que en el práctico, se puede ver que hk es un muestreo de H(jw), donde el paso es w0

#%% Ahora procedo a calcular la salida del sistema

# Calculo los coeficientes de Fourier de la salida
mod_bk = mod_hk*np.abs(ak)  # TODO Aca usé valor absoluto, pero la salida era real, solo que tenía signo. Nuevamente, hay que ver si vamos a introducir np.abs antes de esta entrega
bk = hk*ak
# Visualizacion junto a las otras señales
plt.figure()
plt.plot(w_disc, np.abs(ak), '.-')
plt.plot(w_disc, mod_hk, '.-')
plt.plot(w_disc, mod_bk, '.-')

plt.xlabel('$\\omega (rad/s)$')
plt.ylabel('Amplitud')
plt.grid()
plt.legend(['$a_{k}$: entrada', '$h_{k}$: sistema', '$b_{k}$: salida'])
plt.title('Coeficientes de la entrada, sistema y salida')

#%% Reconstruccion temporal

# Me defino un vector de tiempo donde va a vivir mi senal reconstruida
t_max = 2*T         # Quiero ver dos periodos
h_t = 0.1           # Elijo el paso del vector de tiempo
t_rec = np.arange(0, t_max, h_t)

# Construyo xr
xr = np.zeros_like(t_rec)        # creo xr con la misma estrutura de t_rec e inicializo todos los valores en 0

# Aplico la ecuacion de reconstruccion
for i in range(len(t_rec)):      # recorro el eje de tiempo. en cada iteracion calculo xr(t_i)
    t_i = t_rec[i]
    # Una vez definido el instante en el que voy a reconstruir la senal, procedo a hacer la sumatoria en k
    aux = 0
    for j in range(len(k)):
        aux += bk[j] * np.exp(1j*k[j]*2*np.pi*(1/T)*t_i)
    xr[i] = aux

# Visualizacion de reconstruccion de salida en el tiempo, k_max = 20
plt.figure()
plt.plot(t_rec, xr)
plt.xlabel('Tiempo (s)')
plt.ylabel('Amplitud')
plt.grid()
plt.title('Reconstrucción de la señal temporal a partir de ' + str(len(k)) + ' coeficientes')

# Ahora aumento la cantidad de coeficientes k, recordar que en la ecuacion de sintesis se usan infinitos k
# Fijamos el valor maximo del eje de k que queremos
k_max = 200

k = np.arange(-k_max, k_max)
ak = np.sin(2*np.pi*rel_T*k)/(k*np.pi+eps)
ak[k_max] = 2*rel_T
w_disc = k*w0
hk = 1/(1 + 1j*w_disc)

# Calculo nuevo bk
bk = ak*hk

# Nueva reconstruccion
xr1 = np.zeros_like(t_rec)        # creo xr con la misma estrutura de t_rec e inicializo todos los valores en 0
for i in range(len(t_rec)):      # recorro el eje de tiempo. en cada iteracion calculo xr(t_i)
    t_i = t_rec[i]
    # Una vez definido el instante en el que voy a reconstruir la senal, procedo a hacer la sumatoria en k
    aux = 0
    for j in range(len(k)):
        aux += bk[j] * np.exp(1j*k[j]*2*np.pi*(1/T)*t_i)
    xr1[i] = aux

# Visualizacion de reconstruccion de salida en el tiempo, k_max = 200
plt.figure()
plt.plot(t_rec, xr1)
plt.xlabel('Tiempo (s)')
plt.ylabel('Amplitud')
plt.grid()
plt.title('Reconstrucción de la señal temporal a partir de '+ str(len(k)) + ' coeficientes')

# Comparacion
plt.figure()
plt.plot(t_rec, xr)
plt.plot(t_rec, xr1)
plt.xlabel('Tiempo (s)')
plt.ylabel('Amplitud')
plt.grid()
plt.legend(['40 coeficientes', '400 coeficientes'])
plt.title('Reconstrucción de la señal temporal a partir de 40 y 400 coeficientes')

#%% Ejercicio 8
'''
Para esta parte voy a usar la identidad de Parseval para calcular la energia. Voy a aumentar los coeficientes
hasta que practicamente deje de aumentar la energia, y me voy a considerar esa energia como mi 100%. Luego voy
a calcular el 95% y me voy a fijar a cuantos coeficientes corresponden en la reconstruccion
'''
# Calculo a mano modulo de bk porque lo necesito para aplicar Parseval
mod_hk = 1/(1+w_disc**2)**(0.5)
mod_bk = mod_hk*np.abs(ak)

# Calculo la energia para 400 coeficientes
e_400 = np.sum((mod_bk)**2)
print('Energía con 400 coeficientes: '+ str(e_400))

# Ahora para 600 coeficientes
# Fijamos el valor maximo del eje de k que queremos
k_max_300 = 300

k_300 = np.arange(-k_max_300, k_max_300)
ak_300 = np.sin(2*np.pi*rel_T*k_300)/(k_300*np.pi+eps)
ak_300[k_max] = 2*rel_T
w_disc_300 = k_300*w0
mod_hk_300 = 1/(1+w_disc_300**2)**(0.5)
mod_bk_300 = mod_hk_300*np.abs(ak_300)

# Calculo la energia para 600 coeficientes
e_600 = np.sum((mod_bk_300)**2)
print('Energía con 600 coeficientes: ' + str(e_600))

#%%
'''Se puede ver que el aumento de energía al tomarse 600 coeficientes es despreciable
con respecto a considerar 400 coeficientes, por lo tanto voy a considerar que el 100% de
la energía está tomándome 400 coeficientes.
'''
# Calculo el 95% de mi energia
e_95 = 0.95*e_400

# Ahora me fijo la cantidad de coeficientes que esta mas cerca de esta energia
# Se puede hacer con codigo, pero es mucho mas facil probar con un par de valores

# Prueba tomando solo el termino en k = 0
k_95 = [k_max]
e_k0 = np.sum(mod_bk[k_95]**2)
print(e_k0)

# Todavia podemos agregar energía, así que vamos a agregar coeficientes
# Prueba tomando de -1 a 1
k_95 = [k_max-1, k_max, k_max+1]
e_k1 = np.sum(mod_bk[k_95]**2)
print(e_k1)
# Con este agregado de energia, ya me pase del 95%, asi que me quedo con esta cantidad de energia
# Porcentaje de energia de estos tres coeficientes
print('% Energía utilizada para la reconstrucción aproximada usando 3 coeficientes: '+ str((e_k1/e_400)*100) + '%')

#%% Reconstruccion aproximada
bk_aprox = np.array(bk[k_95])
mod_bk_aprox = np.array(mod_bk[k_95])
k_aprox = np.array(k_95)-k_max    # necesito un k que valga -1, 0 y 1. El k_95 indicaba donde estaban esos valores en el vector k original

xr_aprox = np.zeros_like(t_rec)        # creo xr con la misma estrutura de t_rec e inicializo todos los valores en 0
for i in range(len(t_rec)):      # recorro el eje de tiempo. en cada iteracion calculo xr(t_i)
    t_i = t_rec[i]
    # Una vez definido el instante en el que voy a reconstruir la senal, procedo a hacer la sumatoria en k
    aux = 0
    for j in range(len(k_95)):
        aux += bk_aprox[j] * np.exp(1j*k_aprox[j]*2*np.pi*(1/T)*t_i)
    xr_aprox[i] = aux

plt.figure()
plt.plot(t_rec, xr_aprox)
plt.xlabel('Tiempo (s)')
plt.ylabel('Amplitud')
plt.grid()
plt.title('Reconstrucción de la señal temporal aproximada con 3 coeficientes')

# Comparacion 400 vs 3 coeficientes
plt.figure()
plt.plot(t_rec, xr_aprox)
plt.plot(t_rec, xr)
plt.xlabel('Tiempo (s)')
plt.ylabel('Amplitud')
plt.grid()
plt.legend(['3 coeficientes', '400 coeficientes'])
plt.title('Comparación: reconstrucción 100% energía, y aproximación con 3 coeficientes')

#TODO podria hacer graficos de error

#%%
'''
Si bien las señales no son tan distintas, están muy lejos de ser similares. Ese menos de 3%
de energia de diferencia, provoca esos cambios.
Ahora voy a ir agregando coeficientes para ir observando los cambios.
'''
# Tomando  -3, -2, -1, 0, 1, 2, 3. Saltee de -2 a 2 porque estos coeficientes son 0
k_95 = [k_max-3, k_max-2, k_max-1, k_max, k_max+1, k_max+2, k_max+3]
e_k3 = np.sum(mod_bk[k_95]**2)
print(e_k3)
print('% Energía utilizada para la reconstrucción aproximada usando 7 coeficientes: '+ str((e_k3/e_400)*100) + '%')

# Reconstruccion
bk_aprox = np.array(bk[k_95])
mod_bk_aprox = np.array(mod_bk[k_95])
k_aprox = np.array(k_95) - k_max

xr_aprox_7 = np.zeros_like(t_rec)        # creo xr con la misma estrutura de t_rec e inicializo todos los valores en 0
for i in range(len(t_rec)):      # recorro el eje de tiempo. en cada iteracion calculo xr(t_i)
    t_i = t_rec[i]
    # Una vez definido el instante en el que voy a reconstruir la senal, procedo a hacer la sumatoria en k
    aux = 0
    for j in range(len(k_aprox)):
        aux += bk_aprox[j] * np.exp(1j*k_aprox[j]*2*np.pi*(1/T)*t_i)
    xr_aprox_7[i] = aux

# Comparacion 400 vs 7 coeficientes
plt.figure()
plt.plot(t_rec, xr_aprox_7)
plt.plot(t_rec, xr)
plt.xlabel('Tiempo (s)')
plt.ylabel('Amplitud')
plt.grid()
plt.legend(['7 coeficientes', '400 coeficientes'])
plt.title('Comparación: reconstrucción 100% energía, y aproximación con 7 coeficientes')
# Se puede ver que se ajusta mucho mejor


# Por ultimo voy a usar 11 coeficientes, ya que los que se agregan si se usan 9 son 0
# Tomando  11 coeficientes
k_95 = [k_max-5, k_max-4, k_max-3, k_max-2, k_max-1, k_max, k_max+1, k_max+2, k_max+3, k_max+4, k_max+5]
e_k5 = np.sum(mod_bk[k_95]**2)
print(e_k5)
print('% Energía utilizada para la reconstrucción aproximada usando 11 coeficientes: '+ str((e_k5/e_400)*100) + '%')

# Reconstruccion
bk_aprox = np.array(bk[k_95])
mod_bk_aprox = np.array(mod_bk[k_95])
k_aprox = np.array(k_95) - k_max

xr_aprox_11 = np.zeros_like(t_rec)        # creo xr con la misma estrutura de t_rec e inicializo todos los valores en 0
for i in range(len(t_rec)):      # recorro el eje de tiempo. en cada iteracion calculo xr(t_i)
    t_i = t_rec[i]
    # Una vez definido el instante en el que voy a reconstruir la senal, procedo a hacer la sumatoria en k
    aux = 0
    for j in range(len(k_aprox)):
        aux += bk_aprox[j] * np.exp(1j*k_aprox[j]*2*np.pi*(1/T)*t_i)
    xr_aprox_11[i] = aux

# Comparacion 400 vs 11 coeficientes
plt.figure()
plt.plot(t_rec, xr_aprox_11)
plt.plot(t_rec, xr)
plt.xlabel('Tiempo (s)')
plt.ylabel('Amplitud')
plt.grid()
plt.legend(['11 coeficientes', '400 coeficientes'])
plt.title('Comparación: reconstrucción 100% energía, y aproximación con 11 coeficientes')
# Mejora un poco con respecto a tomar 7 coeficientes

# Visualicemos todas las reconstrucciones juntas
plt.figure()
plt.plot(t_rec, xr)
plt.plot(t_rec, xr_aprox)
plt.plot(t_rec, xr_aprox_7)
plt.plot(t_rec, xr_aprox_11)
plt.plot(t_rec, xr1, '.-')
plt.xlabel('Tiempo (s)')
plt.ylabel('Amplitud')
plt.grid()
plt.legend(['400', '3', '7', '11', '600'])
plt.title('Comparación entre reconstrucciones con distinta cantidad de coeficientes')

#TODO falta la parte experimental pero no tiene mucha cosa interesante que mostrar
