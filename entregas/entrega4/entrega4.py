# -*- coding: utf-8 -*-
"""
Created on Sun Mar 17 19:13:31 2024

@author: Manu
"""
import numpy as np
import matplotlib.pyplot as plt

tau = 1         # Constante del circuito = RC
w_max = 200      # Cuánto quiero ver del eje de frecuencia de la representación de la función continua de w
h_cont = 0.1  # dw le llamé h continuo porque es muy chico para simular que estoy mostrando una función continua, aunque no lo sea en la PC
f_h_cont = 1/h_cont
w = np.arange(-w_max, w_max, h_cont)     # eje de frecuencias "continuo" (elegí un paso chico para construir w)

'''Ejercicio 10'''
#%% Respuesta en frecuencia del sistema

H_jw = 1/(1j*w*tau+1)   # Respuesta en frecuencia del sistema "continua"

# TODO al igual que en la entrega 3, hay que decidir si calcular a mano modulo y fase o con funciones

# A mano
# Modulo
mod_H_jw = 1/(1+w**2)**(0.5)
# Fase
fase_H_jw = -np.arctan(w)   # arg(1) = 0. Y al ser un cociente es la resta de los argumentos

# Con funciones
# Modulo
fmod_H_jw = np.abs(H_jw)

# Fase
ffase_H_jw = np.angle(H_jw)

plt.figure()
plt.plot(w/(2*np.pi), mod_H_jw, '.-')
plt.plot(w/(2*np.pi), fmod_H_jw)
plt.title('Respuesta en frecuencia "continua" del sistema')
plt.xlabel('w')     # Si lo quiero en frecuencia tengo que dividir entre 2.pi
plt.ylabel('Amplitud')
plt.legend(['A mano', 'Con función de Python'])

plt.figure()
plt.plot(w/(2*np.pi), fase_H_jw, '.-')
plt.plot(w/(2*np.pi), ffase_H_jw)
plt.title('Fase "continua" del sistema')
plt.xlabel('w')     # Si lo quiero en frecuencia tengo que dividir entre 2.pi
plt.ylabel('Ángulo (rad/s)')

#%% Transformada de un escalón unitario

# Construyo eje para graficar U_jw

hu_cont =  0.1    # Elegí un paso irracional para que no evualara cerca 0, además tome un paso "grande". De esta manera me evité una asíntota
f_hu_cont = 1/hu_cont

wu = np.arange(-w_max, w_max, hu_cont)
U_jwu = 1/(1j*wu)     

U_jwu[int(f_hu_cont*w_max)] = np.pi  # f_hu_cont*w_max = indice

# U_jwu = U_jwu/np.max(U_jwu) #     Normalizacion

plt.figure()
plt.plot(wu, np.abs(U_jwu), '.-')
plt.title('Respuesta en frecuencia "continua" del escalón')
plt.xlabel('w')
plt.ylabel('Amplitud')

#%% Calculo de la salida Y(jw)

H_jwu = 1/(1j*wu*tau+1) 

Y_jwu = U_jwu * H_jwu

# Y_jwu = 1/(1j*wu) * (1/(1j*wu*tau+1))

plt.figure()
plt.plot(wu/(2*np.pi), np.abs(H_jwu))
plt.title('Respuesta en frecuencia "continua" del sistema')
plt.xlabel('wu')     # Si lo quiero en frecuencia tengo que dividir entre 2.pi
plt.ylabel('Amplitud')

plt.figure()
plt.plot(wu/(2*np.pi), np.abs(U_jwu))
plt.plot(wu/(2*np.pi), np.abs(Y_jwu))
plt.legend(['Entrada', 'Salida'])
plt.title('Respuesta Y_jwu')

plt.figure()
plt.semilogy(wu/(2*np.pi), np.abs(U_jwu))
plt.semilogy(wu/(2*np.pi), np.abs(Y_jwu))
plt.legend(['Entrada', 'Salida'])
plt.title('Respuesta Y_jwu, eje logaritmico')    # Considerar graficar solo la parte derecha, o con eje log

# TODO graficar fase?

#%% Ejercicio 11
tau = 1
l = 1000
h = 0.1
t = np.arange(-l, l, h)

u = np.zeros(len(t))

u[int(l/h):] = 1

plt.figure()
plt.plot(t, u)

freq = np.fft.fftfreq(len(t), d = h)    # Contruccion del eje de frecuencia para todas las senales (el paso es el mismo)

U = np.fft.fft(u)  # FFT escalon

plt.figure()
plt.stem(freq, np.abs(U))