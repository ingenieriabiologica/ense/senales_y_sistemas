#!/usr/bin/env python3
import csv
import numpy as np
import matplotlib.pyplot as plt

t_list=[]


filename='Andres.csv'

with open(filename, 'rt') as csvfile:
	spamreader = csv.reader(csvfile, delimiter='\t', quotechar='|')
	i=0
	for row in spamreader:
		if i>=9:
			if len(row[1])>0:
				t_list.append(float(row[1]))
		i=i+1
t=np.array(t_list)
plt.figure(1)
plt.plot(t)
plt.show()
## %%
my_dpi=300
plt.gca().set_position([0.1, 0.1, 0.9, 0.9])
fname="oculograma_global.png"
plt.savefig(fname,dpi=my_dpi)
## %%
L=int(1e4)
N1=2*L
r=np.arange(N1,N1+L,1)
plt.figure(2)
plt.plot(t[r])
plt.show()

# %%
plt.gca().set_position([0.1, 0.1, 0.9, 0.9])
fname="oculograma_ev_1.png"
plt.savefig(fname,dpi=my_dpi)


# %%
plt.plot([0,L],[0.1,0.1],'-g')

# %%
plt.gca().set_position([0.1, 0.1, 0.9, 0.9])
fname="oculograma_ev_1_th_1.png"
plt.savefig(fname,dpi=my_dpi)

plt.plot([0,L],[-0.1,-0.1],'-r')

plt.gca().set_position([0.1, 0.1, 0.9, 0.9])
fname="oculograma_ev_1_th_2.png"
plt.savefig(fname,dpi=my_dpi)
plt.show()

Fs = 1000

NFFT = 1024       # the length of the windowing segments
Fs = 1000
ax1 = plt.subplot(211)
plt.plot(t)
plt.subplot(212, sharex=ax1)
Pxx, freqs, bins, im = plt.specgram(t, NFFT=NFFT, Fs=Fs, noverlap=900,
                                cmap=plt.cm.gist_heat)
plt.colorbar(im)
plt.show()
