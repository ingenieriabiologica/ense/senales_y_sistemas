#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 26 12:31:37 2017

@author: juan
"""

from scipy import signal
import matplotlib.pyplot as plt
import numpy as np

N=128

sig = np.repeat([0., 1., 1., 0., 1., 0., 0., 1.], N)
sig_noise = sig + np.random.randn(len(sig))
match=np.zeros(N)
mid=int(np.floor(N/2))
L=int(np.floor(N/4))
match[mid-L:mid+L]=np.ones(mid)
corr = signal.correlate(sig_noise,match, mode='same') / N
clock = np.arange(64, N, N)

ax_orig = plt.figure(1).gca()
ax_noise= plt.figure(2).gca()
ax_match = plt.figure(3).gca()
ax_corr= plt.figure(4).gca()

ax_orig.plot(sig)
ax_orig.plot(clock, sig[clock], 'ro')
ax_orig.set_title('Original signal')

ax_noise.plot(sig_noise)
ax_noise.set_title('Signal with noise')

ax_match.plot(match)
ax_match.set_ylim([0,1.1])
ax_match.set_title('Matching filter')

ax_corr.plot(corr)
ax_corr.plot(clock, corr[clock], 'ro')
ax_corr.axhline(0.4, ls=':',color='green')
ax_corr.set_title('Cross-correlated with rectangular pulse')
ax_orig.margins(0, 0.1)
plt.show()