#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  3 09:14:51 2019

@author: lucia
"""

import numpy as np
import matplotlib.pyplot as plt
import os
import sys

sys.path.append("../serie_fourier/")
from plot_freq import ss_plot_frec

F_count = 1

# %% Ejemplo IIR (p 244)

w_range = 3 * np.array([-np.pi, np.pi])

N = 20
w0 = np.pi / N
M = 3 * np.pi / w0
ks = np.arange(-M, M + 1, 1)
wd = ks * w0

w = np.arange(w_range[0], w_range[1] + 0.01, 0.01)
a = 0.9

# Transformada de Fourier de h[n]
H = (1 - a * np.exp(-1j * w)) ** (-1)
Hd = (1 - a * np.exp(-1j * wd)) ** (-1)
hmin = min(np.abs(H))
hmax = max(np.abs(H))
Nh = 10
dh = (hmax - hmin) / Nh
ticks = np.arange(hmin, hmax + dh, dh)

f1 = F_count
F_count += 1
f2 = F_count
F_count += 1
ss_plot_frec(w, H, f1, f2, 0)

plt.figure(f1)
plt.plot([-np.pi, -np.pi], np.array([0, 1.05]) * hmax, 'b', linewidth=2)
plt.plot([np.pi, np.pi], np.array([0, 1.05]) * hmax, 'b', linewidth=2)

plt.figure(f2)
plt.plot([-np.pi, -np.pi], [min(np.angle(H)), max(np.angle(H))], 'b', linewidth=2)
plt.plot([np.pi, np.pi], [min(np.angle(H)), max(np.angle(H))], 'b', linewidth=2)

# %% Ejemplo transformada discreta (ej 5.1 p 362)

plt.figure(F_count)
F_count += 1
# col_cont = np.array([0.3, 0.3, 0.3])
col_cont = np.array([1, 0, 0])
col_ticks = np.array([0.7, 0.7, 0.7])
plt.plot(w, np.abs(H), color=col_cont, label='Transformada')
# plt.stem(wd,Hd,'ks-',MarkerFaceColor='k', MarkerSize=3)
markerLines, stemLines, baseLines = plt.stem(wd, np.abs(Hd), 'bs-', label='Serie')
plt.setp(markerLines, color='b', markersize=4)
plt.plot(w_range, [0, 0], 'k')
for i in range(0, len(ticks)):
    plt.plot(w_range, [ticks[i], ticks[i]], color=col_ticks)

# plt.axis(w_range[0], w_range[1], hmin, hmax)
# %%
ax = plt.gca()
for item in ([ax.xaxis.label, ax.yaxis.label]):  # set axes fontsize
    item.set_fontsize(8)
ax.set_yticks(ticks)
ax.set_yticklabels([], fontsize=8)

wd_label = [[] for i in range(0, len(wd))]
for i in range(0, len(wd)):
    wd_label[i] = "%3.2f" % (wd[i])
ax.set_xticks(wd)
ax.set_xticklabels(wd_label, fontsize=8)
plt.title(u"Transformada de Fourier\n")

ax.set_xlim([-np.pi / 2, np.pi / 2])
ax.set_ylim([hmin, hmax])

plt.legend()
#
ax2 = ax.twiny()
ax2.set_xticks(ks)
ax2.set_xlim(np.array([-np.pi / 2, np.pi / 2]) / w0)

plt.show()
