%%
clear all
close all
home
%%
n=[0:20];
L=length(n);

%%
x=2*n;

xd=[0 diff(x)];
xa=cumsum(x);
xad=[0 diff(xa)];
%%
figure(1)
stem(n,x,'bs-','MarkerFaceColor','b','MarkerSize',4);
legend('x')
axis([n(1) n(end) 0 50])
title('original')
%%
figure(2)
stem(n,xd,'bs-','MarkerFaceColor','b','MarkerSize',4);
legend('xd')
axis([n(1) n(end) 0 10])
title('derivada')
%%
figure(3)
stem(n,xa,'bs-','MarkerFaceColor','b','MarkerSize',4);
legend('xa')
axis([n(1) n(end) 0 450])
title('original')
%%
figure(4)
stem(n,xad,'bs-','MarkerFaceColor','b','MarkerSize',4);
legend('xad')
axis([n(1) n(end) 0 50])
title('integrada-derivada')
%% desfasamos el eje x para poder ver las diferencias
figure(5)
stem(n,x,'rs-','MarkerFaceColor','r','MarkerSize',4);
hold on
stem(n+0.1,xad,'bs-','MarkerFaceColor','b','MarkerSize',4);
hold off
axis([n(1) n(end) 0 50])
legend('x','xad')
title('comparaci?n')