# -*- coding: utf-8 -*-
"""
Created on Fri Apr 26 15:34:08 2019

@author: lucia
"""
#ls /dev/ttyUSB*
#ls /dev/ttyACM*

import serial
import time
import pandas as pd
from pandas import DataFrame

arduino = serial.Serial('COM4', 9600)
time.sleep(2)

fs = 50 # Hz
s = 5   # segundos de señal
v = []

for i in range(0,fs*s):
    c = arduino.read()
    rawString = arduino.readline()
    v.append(rawString)

dt = pd.DataFrame(v)
dt.to_csv(r'Arduino.csv')
arduino.close()