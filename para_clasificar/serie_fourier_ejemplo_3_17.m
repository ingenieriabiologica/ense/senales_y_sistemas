%%
close all
clear all
home
%%
dw=0.01;
w=[-100:dw:100];
N=100;
f0=1/N;
wo=2*pi*f0
wk=[-N/2:N/2]*wo
a=0.5;
%%
H=1./sqrt((1-a*cos(wk)).^2+a^2*sin(wk).^2);

ss_plot_frec_disc(wk,H)

Hm=1/sqrt(1+a)
Hhi=1/abs(1+a)
%%
L=10;
n=0:L-1;
u=ones(1,L);
h=a.^n;
s=(1-a.^(n+1))/(1-a);

figure(3)
plot(n,u,'-b')
hold on
plot(n,h,'-r')
hold off
axis([0 L-1 0 1.05])
title('h[n]')

figure(4)
plot(n,2*u,'-b')
hold on
plot(n,s,'-g')
hold off
axis([0 L-1 0 2.05])
title('s[n]')

n0=ceil(log(0.1)/log(a))

s0=(1-a^(n0+1))/(1-a)
