#!/usr/bin/env python3
import math
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from scipy import signal
 
N=5000
t = np.linspace(0,20,N) # create a time signal
dt=t[2]-t[1]
s=1;
ti=2;
w = 0.5
x1 =  signal.sawtooth(2 * np.pi * w * t)


plt.plot(t,x1,'k-')
plt.ylim([0,1.1])
plt.title('sawtooth signal')
plt.show()



y=signal.correlate(x1,x1, mode='same')/N
plt.plot(t,y,'b-')
ymax=np.max(y)
idx_max=np.argmax(y)
tmax=t[idx_max];
plt.ylim([0,1.1*ymax])
plt.plot(tmax,ymax,'ro')
plt.title('auto correlation')
plt.show()

print("detection: ",tmax)


tau=1
idx_tau=math.floor(1/dt+N/2)
r=range(idx_tau,N-1)
ymax_2=np.max(y[r])
idx_max_2=np.argmax(y[r])+idx_tau
t_tau=t[idx_tau]
tmax_2=t[idx_max_2];
plt.plot(t,y,'b-')
plt.ylim([0,1.1*ymax])
plt.plot([t_tau,t_tau],[0,1.1*ymax],'b--')
plt.plot(tmax,ymax,'ro')
plt.plot(tmax_2,ymax_2,'go')
plt.title('max detection corrected')
plt.show()

f_est=1/(tmax_2)
print("estimated frequency: ",f_est)