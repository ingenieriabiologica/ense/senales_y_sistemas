import numpy as np
import matplotlib.pyplot as plt


w = 3
w = np.arange(-w, w, 0.01)

W1=1
W2=2
t=np.linspace(-10, 10, len(w))

N = len(w) # sample count
P1 = 1  # period
P2 = 2
D = 5   # width of pulse
X1 = np.arange(N) % P1 < D
X2 = np.arange(N) % P2 < D


#X1=rectpuls(w,W1);
#X2=rectpuls(w,W2);

plt.figure(1)
plt.plot(w,X1,'-b');
plt.plot(w,X2,'-r');
#plt.legend('X1','X2')
#axis([w(1) w(end) 0 1.1])

x1=W1/np.pi*np.sinc(W1*t/np.pi);
x2=W2/np.pi*np.sinc(W2*t/np.pi);

plt.figure(2)
plt.plot(t,x1,'-b')
plt.plot(t,x2,'-r')
#plt.legend('x1','x2')
plt.show()