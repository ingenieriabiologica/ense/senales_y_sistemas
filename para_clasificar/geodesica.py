#!/usr/bin/env python3
import math
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from scipy import signal
 
vs=10/3.6
vw=3/3.6

d1s=2*10*np.sqrt(2)
d1w=3*10*np.sqrt(2)
d1=d1s+d1w
t1=d1s/vs+d1w/vw

d2s=10*np.sqrt(5**2+2**2)
d2w=3*10
d2=d2s+d2w
t2=d2s/vs+d2w/vw
