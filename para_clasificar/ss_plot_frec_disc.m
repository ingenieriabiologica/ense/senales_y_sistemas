function ss_plot_frec_disc(w,H)

M=max(abs(H));
m=min(abs(H));
umbral=0.5*(M-m)*ones(size(w))+m;

figure(1)
plot(w,abs(H),'-g')
hold on
plot(w,abs(H),'.')
plot(w,umbral,'--r')
hold off
figure(2)
plot(w,angle(H))
