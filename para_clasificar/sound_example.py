#!/usr/bin/env python3

import pygame, math
import numpy as np
import matplotlib.pyplot as plt
import time

fs=44100
bits=16
pygame.mixer.init(frequency=fs,size=-bits,channels=1)

#b=np.vstack((a,a))
#sound = pygame.sndarray.make_sound(a)
sound = pygame.mixer.Sound("parte1.wav")
print("length: "+str(sound.get_length()))
sound.play()

time.sleep(10)
sound.stop()

A=2**(bits-1)-1
f = 400
t = np.arange(0,5,1.0/fs)
a = A*np.sin(2*math.pi*f*t)
a = a.astype(np.int16)
sound2 = pygame.sndarray.make_sound(a)
r=np.arange(0,1000)
sound2.play()
plt.plot(t[r],a[r])
plt.show()

#time.sleep(10)