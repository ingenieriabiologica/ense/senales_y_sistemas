#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from scipy import signal,io

Nr=50
N1=8
N=24;
k=np.arange(-N,N)
f=N1/N
ak=1/math.pi/k*np.sin(2*math.pi*N1/N*k)
ak[N]=2*f
w=k*2*math.pi/N

x=np.arange(-N,N,0.1)
xp=np.arange(1,N,0.1)
ax=1/math.pi/x*np.sin(2*math.pi*f*x)
envelope=1/math.pi/xp
ax[N*10]=2*f

my_dpi=300
plt.plot(k,ak,'.r',markersize=10)
plt.plot(x,ax,'-g')
plt.plot(xp,envelope,'-b')
plt.plot(-xp,envelope,'-b')
plt.ylim([-0.13,0.52])
plt.xlim([-Nr,Nr])
#plt.show()
plt.gca().set_position([0.1, 0.1, 0.8, 0.9])
fname="test_"+str(round(N,3))+"_"+str(round(N1,3))+".png"
plt.savefig(fname,dpi=my_dpi)
plt.figure()
plt.plot(w,ak*N,'.r',markersize=10)
plt.gca().set_position([0.1, 0.1, 0.8, 0.9])
fname="w_"+str(round(N,3))+"_"+str(round(N1,3))+".png"
plt.savefig(fname,dpi=my_dpi)