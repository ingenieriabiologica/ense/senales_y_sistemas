# -*- coding: utf-8 -*-
"""
Created on Thu Apr 25 12:56:07 2019

@author: lucia
"""

import numpy as np
import matplotlib.pyplot as plt

#%%
n=np.arange(0,20,1)
L=len(n)

#%%
x = 2*n

xd=np.diff(x)
xd=np.insert(xd,0,0)
xa=np.cumsum(x)
xad=np.diff(xa)
xad=np.insert(xad,0,0)

#%%
plt.figure(1)
plt.stem(n,x,'b', label='x')
plt.legend()
plt.xlim([n[0], n[len(n)-1]])
plt.ylim([0,50])
plt.title('original')

#%%
plt.figure(2)
plt.stem(n,xd,'b',label='xd')
plt.legend()
plt.xlim([n[0], n[len(n)-1]])
plt.ylim([0,10])
plt.title('derivada')

#%%
plt.figure(3)
plt.stem(n,xa,'b', label='xa')
plt.legend()
plt.xlim([n[0], n[len(n)-1]])
plt.ylim([0,450])
plt.title('integrada')

#%%
plt.figure(4)
plt.stem(n,xad,'b', label='xad')
plt.legend()
plt.xlim([n[0], n[len(n)-1]])
plt.ylim([0,50])
plt.title('integrada-derivada')

#%%
plt.figure(5)
plt.stem(n,x,'r', markerfmt='ro', label='x')
plt.stem(n+0.1,xad,'b',markerfmt='bo', label='xad')
plt.legend()
plt.xlim([n[0], n[len(n)-1]])
plt.ylim([0,50])
plt.title('comparación')

plt.show()