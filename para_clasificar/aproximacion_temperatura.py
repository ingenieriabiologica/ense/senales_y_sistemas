import math

t=math.pi/2+0.01
s = [math.cos(t), math.sin(t), t]
Th=s[0]*s[0]+s[1]*s[1]+s[2]*s[2]
t=math.pi/2
s = [math.cos(t), math.sin(t), t]
T=s[0]*s[0]+s[1]*s[1]+s[2]*s[2]
e = (Th-T)/T*100
print(f"T: {T} Th: {Th} e: {e}")