%%
close all
clear all
delete(instrfindall);
%s=serial('/dev/ttyACM0','Terminator','CR');
s=serial('COM9','Terminator','CR');
%%
fopen(s);
%%
tf=10;
ts=5;
lv=tf*60/ts;
fscale=1e-3;
v=zeros(1,lv);
w=zeros(1,lv);
figure(1)
i=0;
x=[1:lv];
regimen=0;
while 1

    str=fscanf(s);
    yo=sscanf(str,'%d %d');
    y=yo*fscale;
    fprintf('leido %d convertido %g\n',yo,y);
    v=[v(2:end) y(1)];
    w=[w(2:end) y(2)];
  
    if(i>=lv)
        regimen=1;
    end
    if ~regimen
        v_end=i+1;
 
        plot([1:v_end],v(end-i:end),'r*');
      hold on
        plot([1:v_end],w(end-i:end),'g*');
        hold off
    else
        plot(x,v,'*r');
        hold on
        plot(x,w,'*g');
        hold off
        v_end=lv;
    end
    axis([0 v_end -0.5 5.5])
    drawnow
    pause(0.05)
    i=i+1;
end

fclose(s)