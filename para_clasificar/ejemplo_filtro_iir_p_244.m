    %%
close all
clear all
home
%% Ejemplo IIR (p 244)
w_range=3*[-pi pi];

N=20;
w0=pi/N;
M=3*pi/w0;
ks=[-M:M];
wd=ks*w0;

w=[w_range(1):0.01:w_range(2)];
a=0.9;
H=1./(1-a*exp(-1i*w));
Hd=1./(1-a*exp(-1i*wd));
hmin=min(abs(H));
hmax=max(abs(H));
Nh=10;
dh=(hmax-hmin)/Nh;
ticks = hmin:dh:hmax;
[f1,f2]=ss_plot_frec(w,H);
figure(f1)
hold on
plot([-pi -pi],[0 1.05]*hmax,'g','linewidth',2)
plot([pi pi],[0 1.05]*hmax,'g','linewidth',2)
hold off
figure(f2)
hold on
plot([-pi -pi],[min(angle(H)) max(angle(H))],'g','linewidth',2)
plot([pi pi],[min(angle(H)) max(angle(H))],'g','linewidth',2)
hold off
%% Ejemplo transformada discreta (ej 5.1 p 362)
figure(3)
%col_cont=[0.3 0.3 0.3];
col_cont=[1 0 0];
col_ticks=[0.7 0.7 0.7];
plot(w,abs(H),'Color',col_cont);
hold on
%stem(wd,Hd,'ks-','MarkerFaceColor','k','MarkerSize',3);
stem(wd,abs(Hd),'bs-','MarkerFaceColor','b','MarkerSize',4);
plot(w_range,[0 0],'k')
plot(repmat([w_range(1);w_range(2)],1,length(ticks)),[ticks;ticks],'Color',col_ticks)
%axis([w_range(1) w_range(2) hmin hmax])
axis([-pi/2 pi/2 hmin hmax])
set(gca,'FontSize',8,'YTick',ticks,'YTickLabel',[]);
wd_label=cell(1,length(wd))
for i=1:length(wd)
	wd_label{i}=sprintf('%3.2f',wd(i));
end
set(gca,'XTick',wd,'XTickLabel',wd_label);
xlabel('w')
ylabel('X(jw)')
title('Transformada de Fourier')
legend('Transformada','Serie')

if 1
ax1=gca;
ax2 = axes('Position', get(ax1, 'Position'),'Color', 'none');
set(ax2, 'XAxisLocation', 'top','YAxisLocation','Right');
% set the same Limits and Ticks on ax2 as on ax1;
set(ax2, 'XLim', get(ax1, 'XLim'),'YLim', get(ax1, 'YLim'));
set(ax2, 'XTick', get(ax1, 'XTick'), 'YTick', get(ax1, 'YTick'));

% Set the x-tick and y-tick  labels for the second axes
set(ax2, 'XTickLabel', ks);
axes(ax1)
end

print()

