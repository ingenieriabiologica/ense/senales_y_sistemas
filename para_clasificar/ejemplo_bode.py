#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from scipy import signal, io

# defino parámetros del sistema
tau = 1
# creo un sistema y hago el bode
b = [1]
a = [1, 1 / tau]

# %%
# calculo casero de polos
wc = 1 / tau
polos = np.roots(a)

# %%
# calculo usando sistemas
s1 = signal.lti(b, a)
w, mag, phase = signal.bode(s1)

# asintotas
B = (w > wc) * (-20 * np.log(w) / math.log(10))
# dibujado
plt.figure()
plt.semilogx(w, B, '-b')
plt.semilogx(w, mag, '-r')  # Bode magnitude plot
plt.title('Módulo')
plt.legend(['Asintótico', 'Real'])

plt.figure()
plt.semilogx(w, phase)  # Bode phase plot
plt.title('Fase')
plt.show()

# %%
# calculo de polos usando sistemas
res, poles, k = signal.residue(b, a)
