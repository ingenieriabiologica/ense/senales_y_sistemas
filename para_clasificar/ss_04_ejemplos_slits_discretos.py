#!/usr/bin/env python3
import numpy as np
import scipy.signal as sp
import matplotlib.pyplot as plt
 
N=200
t = np.linspace(0,10,N) # create a time signal
dt=t[2]-t[1]
x1 = np.sin(t) # create a simple sine wave
x2 = x1 + 1.0*(np.random.rand(N) -0.5) # add noise to the signal

L=11
b1=np.ones(L)/L;
#b1=[1/3,1/3,1/3]

a=1;
y1_fir = sp.filtfilt(b1, a, x1)
y2_fir = sp.filtfilt(b1, a, x2)

b2=[0,0,0,0,0,1]
y1_delay = sp.lfilter(b2, a, x1)

b3=[1]
y1_delta = sp.lfilter(b3, a, x1)
#%%
plt.plot(t,x1,'k-')	
plt.title('Original signal')
plt.xlabel('time')
plt.show()
#%%
plt.plot(t,x1,'k-')
plt.plot(t,y1_delta,'r-')
plt.title('identity')
plt.show()
#%%
plt.plot(t,x1,'k-')
plt.plot(t,y1_delay,'r-')
plt.title('delay')
plt.show()
#%%
plt.plot(t,x1,'k-')	
plt.plot(t,x2,'r-')
plt.title('Uniform noise')
plt.xlabel('time')
plt.show()
#%%
plt.plot(t,x1,'k-')
plt.plot(t,y2_fir,'r-')
plt.title('media móvil (sobre señal con ruido)')
plt.show()
#%%
plt.plot(t,x1,'k-')
plt.plot(t,y1_fir,'r-')
plt.title('media móvil (sobre señal sin ruido)')
plt.show()