%%
clear all
close all
home
t=[0:0.01:2];
%%
figure(1)

col='rgb';
K=3;
for k=1:K
	phi(k,:)=cos(2*pi*k*t);
	subplot(K,1,k)
	plot(t,phi(k,:),col(k))
	ylabel(['\Phi_' num2str(k)])
end
subplot(3,1,1)
title('bases');
%%
a0=1;
a=[1/4 1/2 1/3];
figure(2)
x=a0;
for k=1:K
	x=x+a(k)*phi(k,:);
end
plot(t,x)
title('se?al original')
%%
figure(3)
b0=1;
y=b0;
theta0=0;
for k=1:K
	b(k)=1./sqrt(1+4*pi^2*k^2);
	theta(k)=-atan(2*pi*k)
	phi_y(k,:)=b(k)*cos(2*pi*k*t+theta(k));
	t_max_phi_y(k)=(2*pi-theta(k))/2/pi/k;
	y_max_phi_y(k)=b(k);
	y=y+phi_y(k,:);
	%phi_y(k,:)=cos(2*pi*k*t+theta(k));
	subplot(K,1,k)
	plot(t,phi(k,:),col(k))
	hold on
	plot(t,phi_y(k,:),[col(k) ':'])
	plot(t_max_phi_y(k),y_max_phi_y(k),'ok')
	title(['\Phi_' num2str(k)])
	hold off
end

%%
figure(4)
stem([0:3],[b0 b],'rs-','MarkerFaceColor','r','MarkerSize',10);
axis([-0.1 3.1 0 1.1])
%%
figure(5)
stem([0:3],[theta0 theta],'rs-','MarkerFaceColor','r','MarkerSize',10);
axis([-0.1 3.1 -2  2])
%%
figure(6)
plot(t,x,'-r')
hold on
plot(t,y,'-b')
hold off
legend('entrada','salida')