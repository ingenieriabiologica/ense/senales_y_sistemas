#!/usr/bin/env python3
import math
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from scipy import signal, io

# file = "/home/juan/ownCloud/ingenieria_biologica/ense/cursos/3_tercero/procesamiento_digital_de_seniales/proyecto/ecg/100corto.mat"
file = "/Users/338422/NextCloud/ingenieria_biologica/ense/cursos/3_tercero/procesamiento_digital_de_seniales/proyecto/ecg/100corto.mat"

s = io.loadmat(file)
y0 = s["s"]
y = y0[0, :]
N = y.shape[0]
Ts = 1 / 360
tf = N * Ts
t = np.linspace(0, tf, N)  # create a time signal
s = 1
ti = 2
w = 0.5

idx_max = np.argmax(y)
tmax = t[idx_max]
ymax = np.max(y)
ymin = np.min(y)
plt.plot(t, y, 'b-')
plt.ylim([ymin, 1.1 * ymax])
plt.title('ECG signal')
plt.show()

L = 21
b = np.ones(L) / L
a = 1
# x0 = np.absolute(y-np.mean(y))
x0 = y - np.mean(y)
x1 = sp.signal.filtfilt(b, a, x0)

ymax = np.max(x1)
ymin = np.min(x1)
plt.plot(t, x1, 'b-')
plt.ylim([ymin, 1.1 * ymax])
plt.title('gaussian pulse')
plt.show()

y = signal.correlate(x1, x1, mode='same') / N
plt.plot(t, y, 'b-')
ymax = np.max(y)
idx_max = np.argmax(y)
tmax = t[idx_max]
plt.ylim([0, 1.1 * ymax])
plt.plot(tmax, ymax, 'ro')
plt.title('cross correlation')
plt.show()

tau = 1
idx_rel_tau = math.floor(tau / Ts)
idx_abs_tau = idx_rel_tau + math.floor(N / 2)
r = range(idx_abs_tau, N - 1)
ymax_2 = np.max(y[r])
idx_est = np.argmax(y[r]) + idx_rel_tau
t_est = t[idx_est]
idx_max_2 = np.argmax(y[r]) + idx_abs_tau
t_tau = t[idx_abs_tau]
tmax_2 = t[idx_max_2]
plt.plot(t, y, 'b-')
plt.ylim([0, 1.1 * ymax])
plt.plot([t_tau, t_tau], [0, 1.1 * ymax], 'k--')
plt.plot(tmax, ymax, 'ro')
plt.plot(tmax_2, ymax_2, 'go')
plt.title('max detection corrected')
plt.show()

t_est = tmax_2 - tmax
f_est = 1 / (t_est)
T_est = 60 * f_est
print("estimated period (s): ", t_est)
print("estimated frequency (Hz): ", f_est)
print("estimated period (BPM): ", T_est)
