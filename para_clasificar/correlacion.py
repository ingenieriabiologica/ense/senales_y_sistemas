#!/usr/bin/env python3
import math
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from scipy import signal
import time

N = 2000
t = np.linspace(-10, 10, N)  # create a time signal
dt = t[2] - t[1]
s = 1
ti = 0	# tiempo buscado
x1 = np.exp(-np.power(t - ti, 2) / s ** 2)

x2 = np.zeros(N)
w = math.floor(0.5 / dt)
t2s = 2
idx2 = math.floor(t2s / dt + N / 2)
x2[range(idx2 - w, idx2 + w)] = 1
plt.plot(t, x1, 'k-')
plt.plot(t, x2, 'r-')
plt.ylim([0, 1.1])
plt.title('original')
plt.show()

plt.figure()
t2i = -2
for i in range(1, 10):
    x2i = np.zeros(N)
    w = math.floor(0.5 / dt)
    t2 = t2i + 0.5 * i
    idx2 = math.floor(t2 / dt + N / 2)
    x2i[range(idx2 - w, idx2 + w)] = 1
    plt.plot(t, x1, 'k-')
    plt.plot(t, x2i)
    plt.ylim([0, 1.1])
    plt.title('gaussian pulse')
    plt.show()

    c = np.correlate(x1, x2i)
    print("correlation coef: ", c)

plt.figure()
y = signal.correlate(x1, x2, mode='full') / N
plt.plot(y, 'b-')
ymax = np.max(y)
idx_max = np.argmax(y)
tmax = idx_max * dt - N * dt;
plt.ylim([0, 1.1 * ymax])
plt.plot(idx_max, ymax, 'ro')
plt.title('cross correlation')
plt.show()

print("detection: ")
print("t_offset: ", tmax)
print("t_abs: ", t2s+tmax)
