function [f1,f2]=ss_plot_frec(w,H)

M=max(abs(H));
m=min(abs(H));
umbral=0.5*(M-m)*ones(size(w))+m;

f1=figure()
plot(w,abs(H))
hold on
plot(w,umbral,'--r')
hold off
f2=figure()
plot(w,angle(H))
grid on