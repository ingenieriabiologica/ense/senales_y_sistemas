#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 13 09:24:32 2020

@author: lucia
"""

import matplotlib.pyplot as plt
import numpy as np

# Seno de dos variables sobre x
x, y = np.mgrid[0:100:1,0:100:1]
sx = np.sin(x)
sy = np.sin(y)


plt.figure()
plt.imshow(sx, cmap=plt.get_cmap('gray'))

plt.figure()
plt.imshow(sy, cmap=plt.get_cmap('gray'))

#Transformada de Fourier
Sx = np.fft.fft(sx)

plt.figure()
plt.imshow(np.abs(Sx), cmap=plt.get_cmap('gray'))

plt.figure()
plt.imshow(np.angle(Sx), cmap=plt.get_cmap('gray'))

#Transformada de Fourier
Sy = np.fft.fft(sy)

plt.figure()
plt.imshow(np.abs(Sy), cmap=plt.get_cmap('gray'))

plt.figure()
plt.imshow(np.angle(Sy), cmap=plt.get_cmap('gray'))


plt.show()