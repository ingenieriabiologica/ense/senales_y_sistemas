#!/usr/bin/env python3
# %%
import math
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from numpy.fft import fft, fftshift

N = 1000
t_max = 1
t = np.linspace(-t_max, t_max, N)
dt = t[2] - t[1]
fs = 1 / dt
f = 0.5
duty = 0.1 # para mover N hay que tocar aca
w = np.linspace(-math.pi, math.pi, N)

wc = 1
Hd = 0.5 * (1 - signal.square(2 * math.pi * 0.2 * (w - wc)))
plt.figure()
plt.plot(w, Hd)
plt.title('|Hd|')
plt.xlabel('w')

# %% 
# creo una ventana cuadrada
# hay que revisar esta creación, es mucho más angosta que la ventana de hamming
# y eso lo hace dificil de comparar
t_aux = 2 * np.pi * f * (t - duty)
window1 = 0.5 * (1 - signal.square(t_aux))
M_aux = N * duty / 2.0
M = round(M_aux)
print("M_aux: ", M_aux)
print("M: ", M)
# %%
H1 = np.sin(w * (M + 1) / 2.0) / np.sin(w / 2.0)
H1_fase = np.exp(-1j * w * M / 2) * H1
plt.figure()
plt.plot(t, window1)
plt.ylim(-0.1, 1.1)
plt.axhline(0, color='yellow')
plt.axvline(0, color='yellow')
plt.title('w1[n]')

plt.figure()
plt.plot(w, np.abs(H1) / np.max(np.abs(H1)))
plt.title('|W1|')

Hr1 = signal.convolve(Hd, H1, 'same') / N

plt.figure()
plt.plot(w, np.abs(Hr1), '-r')
plt.plot(w, Hd, '-b')
plt.legend(['Hr1', 'Hd'])
plt.title('|Hr1| Respuesta en frecuencia resultante')

# %%
width = int(N * f)
wh1 = np.hamming(width)
wh2 = wh1 - np.min(wh1)
wh2 = wh2 / np.max(wh2)
width_p = int((N - width) / 2)
wz = np.zeros(width_p)
window = np.concatenate((wz, wh2, wz))
# %%
plt.figure()
plt.title("Hamming window")
plt.ylabel("Amplitude")
plt.xlabel("t")
plt.axhline(0, color='yellow')
plt.axvline(0, color='yellow')
plt.plot(t, window)
# %%
plt.figure()
plt.plot(t, window1, '-r')
plt.plot(t, window, '-g')
plt.title('Window comparison (time domain)')
# %%
plt.figure()
Wh = fft(window, N)
mag = np.abs(fftshift(Wh))
freq = np.linspace(-0.5, 0.5, len(Wh))
plt.plot(freq, mag)
plt.title('Frequency response of Hamming window (computed with FFT)')
# %%
plt.figure()
response = 20 * np.log10(mag)
response = np.clip(response, -100, 100)
plt.plot(freq, response)
plt.title("Frequency response of Hamming window")
plt.ylabel("Magnitude [dB]")
plt.xlabel("Normalized frequency [cycles per sample]")
plt.axis('tight')
plt.xlim([-0.1, 0.1])

# %%
plt.figure()
Hw1 = signal.convolve(Hd, Wh, 'same') / N
Hw1_mod = abs(fftshift(Hw1))
plt.plot(w, Hw1_mod / max(Hw1_mod), '-r')
plt.plot(w, Hd, '-b')
plt.legend(['Hw1', 'Hd'])
plt.title('Hw: filtro resultante con ventana de Hamming')
#plt.show()
# %%
dw = 0.1*math.pi
Mopt = 4*math.pi/dw -1
print(f"M_opt:{Mopt}")
M = 39
Hmax = 1/np.sin(np.pi/2/(M+1))
Hmax_approx = 2*(M+1)/np.pi

Hmax_db = 20*np.log(Hmax)/np.log(10)
Hmax_approx_db = 20*np.log(Hmax_approx)/np.log(10)
print("Overshoot in dB: ", np.round(Hmax_db,3))
print("Overshoot (approx) in dB: ", np.round(Hmax_approx_db,3))


# %%
plt.show()