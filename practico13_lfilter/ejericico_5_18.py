#!/usr/bin/env python3
import math
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from scipy import signal


ver_filtrado = True
ver_modulo_y_fase = True

if ver_modulo_y_fase:

	fs = 100
	w1 = np.linspace(-np.pi, np.pi, fs)
	H = 1/(1-0.5*np.exp(-1j*w1))

	a = [1, -0.5]
	b = [1]

	w, h = signal.freqz(b, a)

	fig2, ax3 = plt.subplots()
	ax3.set_title('respuesta en frecuencia calculo manual')
	ax3.plot(w1, abs(H), 'b')
	ax3.set_ylabel('Amplitud', color='b')
	ax3.set_xlabel('frecuencia')
	ax4 = ax3.twinx()
	ax4.plot(w1, np.angle(H), 'g')
	ax4.set_ylabel('Angulo', color='g')
	ax4.grid()


	fig, ax1 = plt.subplots()
	ax1.set_title('respuesta en frecuencia con freqz')
	ax1.plot(w, abs(h), 'b')
	ax1.set_ylabel('Amplitud', color='b')
	ax1.set_xlabel('frecuencia')
	ax2 = ax1.twinx()
	ax2.plot(w, np.angle(h), 'g')
	ax2.set_ylabel('Angulo', color='g')
	ax2.grid()

	plt.show()

if ver_filtrado:

	N=1000
	r =  0.1*np.random.randn(N)


	fs = 1000
	f1 = 5
	t = np.arange(0, 1, 1.0/fs)
	x = np.sin(2*math.pi*f1*t)


	xr = x + r

	b = [1, -0.5]
	a = [1]
	zi = signal.lfilter_zi(b, a)
	z, _ = signal.lfilter(b, a, xr, zi=zi*xr[0])

	b2, a2 = signal.butter(3, 0.05) # usa la funcion butter para filtrar
	zi2 = signal.lfilter_zi(b2, a2)
	z2, _ = signal.lfilter(b2, a2, xr, zi=zi2*xr[0])


	plt.figure(1)
	plt.plot(t, xr, 'g', label='sin+noisy')
	plt.plot(t, z, 'r', label='filt_sistema')
	plt.plot(t, z2, 'b', label='filt_butter')
	plt.title('Lfilter aplication')
	plt.legend()
	plt.grid()
	plt.show()
